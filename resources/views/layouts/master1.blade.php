<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>@yield('title')</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/style1.css')}}">
    <link rel="stylesheet" href="{{asset('css/font_styling.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
    <link rel="stylesheet" href="{{asset('css/media_query.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <script type="text/javascript" src="{{asset('js/animation-decksys.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="js/app.js"></script>




    @yield('styles')

</head>
<body>
@if(\Request::is("/"))
    @include('partials.header')
@elseif(\Request::is("Aboutus"))
    @include('partials.Aboutus_header')
@elseif(\Request::is("SME_Hosting"))
    @include('partials.SME_header')
@elseif(\Request::is("dedicated_server"))
    @include('partials.Dedicated_header')
@elseif(\Request::is("Cloud_Hosting"))
    @include('partials.Cloud_header')
@elseif(\Request::is("Managed_wordpress"))
    @include('partials.Wordpress_header')
@elseif(\Request::is("Contact"))
    @include('partials.Contact_header')
@elseif(\Request::is("Ecommerce_Hosting"))
    @include('partials.ecommerce_header')
@elseif(\Request::is("EnterpriseServer_Hosting"))
    @include('partials.enterprise_header')
@elseif(\Request::is("Resellers"))
    @include('partials.Reseller_header')
@elseif(\Request::is("Terms_Service"))
    @include('partials.Terms_header')
@elseif(\Request::is("Privacy_Policy"))
    @include('partials.Privacy_header')
@elseif(\Request::is("Refund_Policy"))
    @include('partials.Refund_header')
@elseif(\Request::is("Freelancers"))
    @include('partials.freelancer_header')
@elseif(\Request::is("cart"))
    @include('partials.cart_header')
@elseif(\Request::is("payWithRazorpay"))
    @include('partials.Razorpay_header')
@elseif(\Request::is("Product"))
    @include('partials.Product_header')
@elseif(\Request::is("Domain"))
    @include('partials.domain_header')
@elseif(\Request::is("payment_success"))
    @include('partials.domain_header')
@endif


@yield('content')
@include('partials.footer')
@yield('scripts')
<!-- Accordian Script Start-->
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
</script>
<!-- Accordian script End -->



<script  type="text/javascript">
    $( document ).ready(function() {
        $(".with-us").hide();
        $(".with-other").hide();
        $(".with-all").hide();
        $('textarea[name="shopDescription"]').hide();
        $("#work-option :radio").change(function () {
            var workType = $('input[name=work]:checked').val();
            if (workType == "wc") {
                $(".with-other").hide();
                $(".with-all").hide();
                $(".with-us").show();
            } else if (workType == "woc") {
                $(".with-us").hide();
                $(".with-all").hide();
                $(".with-other").show();
            }else if (workType =="oc"){
                $(".with-us").hide();
                $(".with-other").hide();
                $(".with-all").show();
            }
            else {
                $(".with-us").hide();
                $(".with-other").hide();
                $(".with-all").hide();
            }
        });
        $(document).on('click', 'input[name="shop"]', function () {
            if (this.checked) {
                $('textarea[name="shopDescription"]').show();
            } else {
                $('textarea[name="shopDescription"]').hide();
            }
        });
    });


</script>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="{{asset('js/map.js')}}"></script>
</body>
</html>
