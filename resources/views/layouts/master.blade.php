<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,  initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="msvalidate.01" content="FEF4EC416BE3DB1CC192FEFAA6C646C3" />
	<meta name="description" content="DeckSys is a brand of Makto Technology Private Limited and provides solutions on hosting services and solutions, dedicated servers and cloud infrastructure and management">
  	<meta name="keywords" content="Shared hosting, Linux Hosting, Windows Hosting, Dedicated Servers, Cloud Hosting, Cloud VPC, Shared Linux Hosting, Shared Windows Hosting, Rails Hosting">
  	<meta name="author" content="Team @DeckSys, Makto Technology Private Limited">

    <title>@yield('title')</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/font_styling.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
    <script type="text/javascript" src="js/app.js"></script>
    <link rel="stylesheet" href="{{asset('css/media_query.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <script type="text/javascript" src="{{asset('js/animation-decksys.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJpbVlz5zgmYaSxWbMa6isKzHDggAnr5I" type="text/javascript"></script>

    @yield('styles')

</head>
<body>
@if(\Request::is("/"))
    @include('partials.header')
@elseif(\Request::is("About"))
    @include('partials.About_header')
@elseif(\Request::is("smeHosting"))
    @include('partials.SME_header')
@elseif(\Request::is("dedicatedServer"))
    @include('partials.Dedicated_header')
@elseif(\Request::is("cloudHosting"))
    @include('partials.Cloud_header')
@elseif(\Request::is("wordpressHosting"))
    @include('partials.Wordpress_header')
@elseif(\Request::is("Contact"))
    @include('partials.Contact_header')
@elseif(\Request::is("ecommerceHosting"))
    @include('partials.ecommerce_header')
@elseif(\Request::is("EnterpriseServer_Hosting"))
    @include('partials.enterprise_header')
@elseif(\Request::is("Resellers"))
    @include('partials.Reseller_header')
@elseif(\Request::is("Terms_Service"))
    @include('partials.Terms_header')
@elseif(\Request::is("Privacy_Policy"))
    @include('partials.Privacy_header')
@elseif(\Request::is("Refund_Policy"))
    @include('partials.Refund_header')
@elseif(\Request::is("Freelancers"))
    @include('partials.freelancer_header')
@elseif(\Request::is("cart"))
    @include('partials.cart_header')
@elseif(\Request::is("payWithRazorpay"))
    @include('partials.Razorpay_header')
@elseif(\Request::is("Product"))
    @include('partials.Product_header')
@elseif(\Request::is("Domain"))
    @include('partials.domain_header')
@elseif(\Request::is("payment_success"))
    @include('partials.domain_header')
@elseif(\Request::is("ssl"))
    @include('partials.SSL_header')

@endif


@yield('content')
@include('partials.footer')
@yield('scripts')
<!-- Accordian Script Start-->
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
</script>
<!-- Accordian script End -->



<script  type="text/javascript">
    $( document ).ready(function() {
        $(".with-us").hide();
        $(".with-other").hide();
        $(".with-all").hide();
        $('textarea[name="shopDescription"]').hide();
        $("#work-option :radio").change(function () {
            var workType = $('input[name=work]:checked').val();
            if (workType == "wc") {
                $(".with-other").hide();
                $(".with-all").hide();
                $(".with-us").fadeIn(500);
            } else if (workType == "woc") {
                $(".with-us").hide();
                $(".with-all").hide();
                $(".with-other").fadeIn(500);
            }else if (workType =="oc"){
                $(".with-us").hide();
                $(".with-other").hide();
                $(".with-all").fadeIn(500);
            }
            else {
                $(".with-us").hide();
                $(".with-other").hide();
                $(".with-all").hide();
            }
        });
        $(document).on('click', 'input[name="shop"]', function () {
            if (this.checked) {
                $('textarea[name="shopDescription"]').show();
            } else {
                $('textarea[name="shopDescription"]').hide();
            }
        });
    });


</script>


<script src="{{asset('js/map.js')}}"></script>
</body>
</html>