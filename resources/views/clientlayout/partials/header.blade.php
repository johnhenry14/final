<div class="app app-header-fixed ">
  

    <!-- header -->
  <header id="header" class="app-header navbar" role="menu">
      <!-- navbar header -->
      <div class="navbar-header bg-dark">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
       
        <a class="navbar-brand" href="/">
          <img src="{{asset('images/decksys.png')}}"  alt="Decksys" class="decksysBrandLogo">
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
        <!-- buttons -->
        <div class="nav navbar-nav hidden-xs">
          <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app">
            <i class="fa fa-dedent fa-fw text"></i>
            <i class="fa fa-indent fa-fw text-active"></i>
          </a>
         
        </div>
        <!-- / buttons -->

        <!-- link and dropdown -->
        <ul class="nav navbar-nav hidden-sm">
          <li class="dropdown pos-stc" style="background-color: #a1e1ff;">
            <a href="http://projects.maktoinc.com/kb/" target="_blank">
              <span>Knowledge Base</span> 
	      </a>
          </li>
          <li class="dropdown" style="background-color: #bababa;">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
              <span translate="header.navbar.new.NEW">Support No: </span>
              <span> +91 - 73977 67219 </span>
            </a>
       
          </li>
        </ul>
        <!-- / link and dropdown -->

       
        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
         
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img src="{{asset('client/images/a0.png')}}" alt="...">
              </span>
              <span class="hidden-sm hidden-md">{{session()->get('login_user_name')}}</span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">
              
              <li>
                <a href="" ui-sref="app.page.profile"> <i class="fa fa-id-badge" aria-hidden="true"></i>
  Client ID : {{session()->get('login_id')}}</a>
              </li>
  <li class="divider"></li>

              <li>
                <a href="/profile" ui-sref="app.page.profile"> <i class="fa fa-user"></i> Profile</a>
              </li>
              
              <li class="divider"></li>
              
              <li>
                <a href="{{ route('logout.signout') }}" ><i class="fa fa-sign-out"></i> Logout</a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->
      </div>
      <!-- / navbar collapse -->
  </header>
  <!-- / header -->


    <!-- aside -->
  <aside id="aside" class="app-aside hidden-xs bg-dark">
      <div class="aside-wrap">
        <div class="navi-wrap" style="margin-top:25px;">
          

          <!-- nav -->
          <nav ui-nav class="navi clearfix">
            <ul class="nav">
              
              <li>
                <a href="/index" class="auto">      
                  
                  <i class="fa fa-home"></i>
                  <span>Dashboard</span>
                </a>
                
              </li>
  
              <li>
                <a href class="auto">      
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                 
                  <i class="fa fa-globe"></i>
                  <span>Domains</span>
                </a>
                <ul class="nav nav-sub dk">
                  <li class="nav-sub-header">
                    <a href>
                      <span>Domains</span>
                    </a>
                  </li>
                 <li>
                    <a href="registerdomain">
                      <span>Register Domain</span>
                    </a>
                  </li>                  <li>
                    <a href="mydomains">
                      <span>My Domains</span>
                    </a>
                  </li>

               
                </ul>
              </li>
              <li>
                <a href class="auto">
                  <span class="pull-right text-muted">
                  <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
             
                  </span>
                  <i class="fa fa-wrench"></i>
                  <span>Services</span>
                </a>
                <ul class="nav nav-sub dk">
                  <li class="nav-sub-header">
                    <a href>
                      <span>Services</span>
                    </a>
                  </li>
                  <li>
                    <a href="myservices">
                      <span>My Services</span>
                    </a>
                  </li>

<li>
                    <a href="Product">
                      <span>Add New Service</span>
                    </a>
                  </li>
                 
                </ul>
              </li>
              <li>
                <a href class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  
                  <i class="fa fa-money"></i>
                  <span>Accounts</span>
                </a>
                <ul class="nav nav-sub dk">
                
<li>
                    <a href="Getorders">
                      <span>Orders</span>
                    </a>
                  </li>
<li>
                    <a href="Funds">
                      <span>Funds</span>
                    </a>
                  </li>
                  <li>
                    <a href="invoice">
                      <span>Invoices</span>
                    </a>
                  </li>
                  
                 
                </ul>
              </li>
              <li>
                <a href class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <i class="glyphicon glyphicon-edit"></i>
                  <span>Tickets</span>
                </a>
                <ul class="nav nav-sub dk">
                  <li class="nav-sub-header">
                    <a href>
                      <span>Tickets</span>
                    </a>
                  </li>  <li>
                    <a href="RaiseTicket">
                      <span>Place Tickets</span>
                    </a>
                  </li>
                  
                  <li>
                    <a href="Tickets">
                      <span>Open Tickets</span>
                    </a>
                  </li>
                </ul>
              </li>
           
            </ul>
          </nav>
         
        </div>
      </div>
  </aside>
  <!-- / aside -->