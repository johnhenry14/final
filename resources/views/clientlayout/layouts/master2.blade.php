<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <title>DeckSys - Shared Hosting, Cloud Hosting, Dedicated Servers, VPS, Cloud VPS, Private Cloud, Rails Hosting</title>
    <link rel="stylesheet" href="client/lib/animate.css" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="{{asset('client/lib/simple-line-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('client/lib/bootstrap.css')}}" type="text/css" />

    <link rel="stylesheet" href="{{asset('client/css/font.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('client/css/app.css')}}" type="text/css" />

    @yield('styles')
</head>
<body>


@yield('content')


@yield('scripts')

<script src="{{asset('client/js/jquery.js')}}"></script>
<script src="{{asset('client/js/bootstrap.min.js')}}"></script>
<script src="{{asset('client/js/ui-load.js')}}"></script>
<script src="{{asset('client/js/ui-jp.config.js')}}"></script>
<script src="{{asset('client/js/ui-jp.js')}}"></script>
<script src="{{asset('client/js/ui-nav.js')}}"></script>
<script src="{{asset('client/js/ui-toggle.js')}}"></script>
<script src="{{asset('client/js/ui-client.js')}}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script  src="{{asset('client/js/index.js')}}"></script>

<script>

    var myObj, x;
    myObj = $response;
    x = myObj.status;
    document.getElementById("demo").innerHTML = x;

</script>
</body>
</html>