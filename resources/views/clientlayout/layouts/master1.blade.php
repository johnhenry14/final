<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="client/css/intlTelInput.css">
  <link rel="stylesheet" href="client/css/demo.css">
    <link rel="stylesheet" href="client/css/font_styling.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
  <script src="client/js/intlTelInput.js"></script>
  <script type="text/javascript" src="client/js/StatesDropdown.js"></script>



	<title>@yield('title')</title>


    <style>
        .invalid-feedback1 {
            margin-top: .25rem;
            font-size: .875rem;
            color: #dc3545;
        }
    .req{
  color:red;
}
.center-block {
  display: block;
  margin-right: auto;
  margin-left: auto;
}.custom{
    color:#fff;
}
.anchor {
  color: #fff !important;
}

    </style>
@yield('styles')
    </head>
    <body style="background-color:#c1bdba">


        @yield('content')


        @yield('scripts')


    <script> (function() {
    'use strict';
    window.addEventListener('load', function() {
      var form = document.getElementById('needs-validation');
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    }, false);
  })();

  //numbers and textonly//
  function isNumberKey(evt){  <!--Function to accept only numeric values-->
    //var e = evt || window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode != 46 && charCode > 31
	&& (charCode < 48 || charCode > 57))
        return false;
        return true;
	}

    function ValidateAlpha(evt)
    {
        var keyCode = (evt.which) ? evt.which : evt.keyCode
        if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)

        return false;
            return true;
    }

  </script>

 <script>

     var password = document.getElementById("password")
         , confirmpassword = document.getElementById("confirmpassword");

     function validatePassword(){
         if(password.value != confirmpassword.value) {
             confirmpassword.setCustomValidity("Passwords Don't Match");
         } else {
             confirmpassword.setCustomValidity('');
         }
     }

     password.onchange = validatePassword;
     confirmpassword.onkeyup = validatePassword;

 </script>

    <script>
    $(document).ready(function() {

    $('input[type=password]').keyup(function() {
        var pswd = $(this).val();
        //validate the length
if ( pswd.length < 8 ) {
    $('#length').removeClass('valid').addClass('invalid');
} else {
    $('#length').removeClass('invalid').addClass('valid');
}
//validate letter
if ( pswd.match(/[A-z]/) ) {
    $('#letter').removeClass('invalid').addClass('valid');
} else {
    $('#letter').removeClass('valid').addClass('invalid');
}

//validate capital letter
if ( pswd.match(/[A-Z]/) ) {
    $('#capital').removeClass('invalid').addClass('valid');
} else {
    $('#capital').removeClass('valid').addClass('invalid');
}

//validate number
if ( pswd.match(/\d/) ) {
    $('#number').removeClass('invalid').addClass('valid');
} else {
    $('#number').removeClass('valid').addClass('invalid');
}

if(pswd.match(/[@$,<>#:?_*&;]/))
{
    $('#special').removeClass('invalid').addClass('valid');
    
}
else
{
    $('#special').removeClass('valid').addClass('invalid');
}
}).focus(function() {
    $('#pswd_info').show();
}).blur(function() {
    $('#pswd_info').hide();
});
});
    
    </script>


    </body>
</html>