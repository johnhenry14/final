@extends('clientlayout.layouts.master1')
@section('title')
    Decksys | Reset Password
@endsection
@section('content')
    <div class="container" style="max-width: 100% !important;">

        <div class="row">
            <div class="offset-lg-2 col-lg-8 col-sm-12 border rounded main-section"  style="margin-top:100px; background-color: rgba(19, 35, 47, 0.9); color:#fff;">
                                <img class="center-block" src="client/images/decksys.png" alt="." style="padding-top:10px;">
                <hr>

                <form class="container" action="{{route('login.reset_password_action')}}" method="post" id="needs-validation" novalidate>
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <input type = "hidden" name = "email" value = "<?php echo $email; ?>">
                    <div class="row">
                        <div class="offset-lg-3 col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse custom" for="validationCustom01">New Password<span class="req">*</span></label>
                                <input type="password" class="form-control" name="password2" id="validationCustom01" placeholder="New Password" value="" required style="background-color: rgba(19, 35, 47, 0.4); color:#fff;"maxlength="100">
                                <div class="invalid-feedback">
                                    Enter Your New Password
                                </div>
                            </div>
                        </div></div>  <div class="row">
                        <div class="offset-lg-3 col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse custom" for="validationCustom02">Confirm Password<span class="req">*</span></label>
                                <input type="password" class="form-control" name="password2" id="validationCustom02" placeholder="Confirm Password" value="" style="background-color: rgba(19, 35, 47, 0.4); color:#fff;"required maxlength="25">
                                <div class="invalid-feedback">
                                    Enter Your Confirm Password
                                </div>
                            </div>
                        </div>

 <div id="pswd_info">
                        <h4  class="text-center">Password must meet the following requirements:</h4>
                        <ul style="list-style-type:none;"  class="text-center">
                            <li id="letter" class="invalid">At least <strong>one letter</strong></li>
                            <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
                            <li id="number" class="invalid">At least <strong>one number</strong></li>
                            <li id="special" class="invalid">At least <strong>one special character</strong></li>
                            <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
                        </ul>
                    </div>


                    </div>


                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-12 text-center" style="padding-bottom: 20px;">
                            <button class="btn btn-info" type="submit">Reset Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection