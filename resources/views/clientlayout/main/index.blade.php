@extends('clientlayout.layouts.master')

@section('title')
    DashBoard
@endsection
@section('content')




    <!-- content -->
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">


            <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
    app.settings.asideFolded = false; 
    app.settings.asideDock = false;">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <h1 class="m-n font-thin h3 text-black">Dashboard</h1>

                            </div>

                        </div>
                    </div>
                    <!-- / main header -->
                    <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
                        <!-- stats -->

                        <!-- / stats -->
                        <!-- frist row Starts-->
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="panel bg-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-globe fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                @foreach($domain as $key => $value)
                                                    @if($key=="totalresults")
                                                        <div class="huge" style="font-size:45px; color:#fff;">{{$value}}</div>
                                                    @endif
                                                @endforeach

                                                <div style="font-size:30px; color:#fff;" >Domains</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6">
                                <div class="panel bg-info">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-wrench fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                @foreach($products as $key => $value)
                                                    @if($key=='totalresults')
                                                        <div class="huge" style="font-size:45px; color:#fff;">{{$value}}</div>
                                                    @endif
                                                @endforeach

                                                <div style="font-size:30px; color:#fff;">Services</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="panel bg-dark">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-edit fa-5x "></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge" style="font-size:45px; color:#fff;">  @foreach($tickets as $key => $count)
                                                        @if($key == "totalresults")
                                                            {{$count}}
                                                        @endif
                                                    @endforeach</div>
                                                <div style="font-size:30px; color:#fff;">Issues</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <hr>





                        <!-- frist row End-->

<!-- Account Section Start-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default bg-light">
                                    <div class="panel-heading text-center" style="background-color: #bababa;">
                                        Accounts
                                    </div>
					<div class="table-responsive">
                                    <table class="table table-striped m-b-none" style="text-align: center;" data-sort-order="desc">
                                        <thead>
                                        <tr>
					<th style="text-align: center;">ID</th>	
                                            <th style="text-align: center;">Company Name</th>
					    <th style="text-align: center;">Total</th>
                                            <th style="padding-left: 55px;text-align: center;">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($invoices['totalresults'] == '0')
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
					    <td> - </td>
                                            <td> - </td>

                                        @else
                                            @foreach($invoices['invoices']['invoice'] as $value)
                                                <tr>
						    <td>{{$value['id']}} </td>
                                                    <td>{{$value['companyname']}}</td>
                                                    <td> <i class="fa fa-inr"></i> {{$value['total']}}</td>
                                                    <td>
                                                        @if ($value['status'] == "Paid")
                                                            <button class="btn btn-success">{{$value['status']}}</button>
                                                        @elseif($value['status']=="Cancelled")
                                                            <button class="btn btn-primary">{{$value['status']}}</button>
                                                        @elseif($value['status']=="Unpaid")
                                                            <button class="btn btn-danger ">{{$value['status']}}</button>
                                                        @elseif($value['status']=="Draft")
                                                            <button class="btn btn-secondary">{{$value['status']}}</button>
                                                        @elseif($value['status']=="Refunded")
                                                            <button class="btn btn-info ">{{$value['status']}}</button>
                                                        @elseif($value['status']=="Overdue")
                                                            <button class="btn btn-warning ">{{$value['status']}}</button>

                                                        @endif
                                                    </td>

                                                </tr>



                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
				</div>
                                </div>
                            </div>
                        </div>
                        <!-- Account Section End-->
                        <!-- second row start-->
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="panel no-border"> 
				

                                    <div class="panel-heading wrapper b-b b-light bg-info">

                                        <h4 class="font-thin m-t-none m-b-none text-white text-center">Services</h4>
                                    </div>
			 <div class="table-responsive">
                                    <table class="table table-striped m-b-none" data-sort-order="desc">
                                        <thead>
                                        <tr>
					    <th>OrderId</th>
                                            <th style="width: 300px;">Package Name</th>
                                            <th>Domain</th>
 				      	    <th>Regdate</th>
                                            <th>Next Due Date</th>
				            <th>Status</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($products['totalresults'] == '0')
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
					    <td> - </td>
                                            <td> - </td>
                                            <td> - </td>

                                        @else

                                            @foreach($products['products']['product'] as $product)
                                                <tr>
						    <td>{{$product['orderid']}}</td>
                                                    <td>{{$product['groupname']}} - ({{$product['name']}})</td>
                                                    <td>{{$product['domain']}}</td>
						    <td>{{$product['regdate']}}</td>
                                                    <td>{{$product['nextduedate']}}</td>
						    <td>{{$product['status']}}</td>

                                                </tr>

                                            @endforeach
                                        @endif

                                        </tbody>
                                    </table>
				</div>
                                </div>
                            </div>



                           


                        </div>
                        <hr>
                        <!-- second row End-->

<div class="row">
                            <div class="col-lg-12">

                                <div class="panel no-border"> 
<div class="panel-heading wrapper b-b b-light bg-primary">

                                        <h4 class="font-thin m-t-none m-b-none text-white text-center">Tickets</h4>
                                    </div>
                           

					<div class="table-responsive">

                                    <table class="table table-striped m-b-none" style="text-align: center;" data-sort-order="desc">
                                        <thead>
                                        <tr>
					  <th style="text-align:center"> S.No</th>
					   <th style="text-align:center"> Ticket Id</th>
                                            <th style="text-align:center">Ticket Name</th>
					    <th style="text-align:center">Department ID</th>
					    <th style="text-align:center">Submitter</th>


                                            <th style="padding-left: 55px;text-align:center">Status</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if($tickets['totalresults'] == '0')
                                            <td> - </td>
                                            <td> - </td>
					    <td> - </td>
                                            <td> - </td>
					    <td> - </td>
                                            <td> - </td>


                                        @else


                                            @foreach($tickets['tickets']['ticket'] as $key)
                                                <tr>
						    <td>{{$key['id']}}</td>
 						    <td>{{$key['tid']}}</td>
                                                    <td>{{$key['subject']}}</td>
						    <td>{{$key['deptid']}}</td>
                                                    <td>{{$key['name']}}</td>



                                                    <td style="width: 200px;padding-left: 44px;">
                                                        @if ($key['status'] == "Answered")
                                                            <button class="btn btn-success">{{$key['status']}}</button>
                                                        @elseif($key['status']=="Closed")
                                                            <button class="btn btn-primary">{{$key['status']}}</button>
                                                        @elseif($key['status']=="Open")
                                                            <button class="btn btn-danger">{{$key['status']}}</button>
                                                        @elseif($key['status']=="On Hold")
                                                            <button class="btn btn-secondary ">{{$key['status']}}</button>
                                                        @elseif($key['status']=="In Progress")
                                                            <button class="btn btn-info ">{{$key['status']}}</button>


                                                        @endif
                                                    </td>

                                                </tr>



                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                  
</div>
                                </div></div>
</div>




                        



                    </div>
                </div>
                <!-- / main -->

            </div>



        </div>
    </div>
    <!-- /content -->
@endsection
