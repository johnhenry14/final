@extends('clientlayout.layouts.master')

@section('title')
Decksys | My Funds
@endsection

@section('content')


<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Funds</h1>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
    <div class="text-right">
      <h2>Credit Balance - @foreach($balance  as $key=> $invoice)
@if($key=='credit')
{{$invoice}}
@endif
@endforeach </h2>
    </div>
    <div class="table-responsive" style="overflow-x:  hidden;">
      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"></div><div class="row"><div class="col-sm-12"><table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
        <thead>
          <tr role="row">
<th style="width: 186px;" class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Date</th>
<th style="width: 241px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Description</th>
<th style="width: 240px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Amount</th>

<th style="width: 240px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Action</th>
</tr>
        </thead>
        <tbody>
        @foreach($paymentinvoice['credits']['credit'] as $key=> $invoice)

                <tr>
			@for($key = 0; $key < 100; $key++)
                        @endfor
<td>{{$invoice['date']}}</td> 
<td>{{$invoice['description']}}</td> 


  			<td>{{$invoice['amount']}}</td> 
<td><button class="btn btn-info" data-toggle="modal" data-target="#myModalz"><i class="fa fa-plus" aira-hidden="true"></i></button></td> 

   			</tr>
                                     
                      @endforeach 
</tbody>
      </table></div></div></div>
    </div>
  </div>
</div>

	</div>
  </div>


      
    
<div class="container">
 

<!-- Modal -->
  <div class="modal fade" id="myModalz" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title text-center">Add Credit Balance</h3>
        </div>
        <div class="modal-body">
          <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form">
            <div class="form-group">
              <label>Date</label>
              <input type="text" name="domainname" class="form-control" value="{{date("d/m/Y")}}" placeholder="Enter Your Domain Name">
            </div>
            <div class="form-group">
              <label>Description</label>
              <input type="text" name="value" class="form-control" placeholder="Description">
            </div>

<div class="form-group">
              <label>Amount</label>
              <input type="text" name="value" class="form-control" placeholder="Credit Balance">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </div>
        </div>
        
      </div>
      
    </div>
  </div>
  


@endsection