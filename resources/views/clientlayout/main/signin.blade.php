@extends('clientlayout.layouts.master1')
@section('title')
    Signin
@endsection
@section('content')

        <div class="container" style="max-width: 100% !important;">
        <div class="row">
            <div class="offset-lg-2 col-lg-8 col-sm-12  border rounded main-section"    style="background-color:rgba(19, 35, 47, 0.9); margin-top:100px;">
                <img class="center-block" src="client/images/decksys.png" alt="." style="padding-top:10px;">
                <hr>
                <?php if(isset($_GET['error'])){ ?>
                <p style="text-align: center;color: red;font-size: 18px;">Enter valid Email or Password</p>
                <?php } ?>
                <?php if(isset($_GET['success'])){ ?>
               <p style="text-align: center;color: #fff;font-size: 18px;">Account Created Successfully</p>
                <?php } ?>
                <form class="container" action="{{route('login.signin_action')}}" method="post" id="needs-validation" novalidate>
 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                    <div class="row">
                        <div class="offset-lg-3 col-lg-6 col-sm-12 col-12" >
                            <div class="form-group">
                                <label class="text-inverse custom" for="validationCustom01">Email<span class="req">*</span></label>
                                <input type="Email" class="form-control" name="email" id="validationCustom01" placeholder="Email" value="" required style="background-color: rgba(19, 35, 47, 0.4); color:#fff;"maxlength="100">
                                <div class="invalid-feedback">
                                    Enter Your Email
                                </div>
                            </div>
                        </div></div>  <div class="row">
                        <div class="offset-lg-3 col-lg-6 col-sm-6 col-12" >
                            <div class="form-group">
                                <label class="text-inverse custom" for="validationCustom02">Password<span class="req">*</span></label>
                                <input type="password" class="form-control" name="password2" id="validationCustom02" placeholder="Password" value="" style="background-color: rgba(19, 35, 47, 0.4); color:#fff;"required maxlength="25">
                                <div class="invalid-feedback">
                                    Enter Your Password
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-12 col-sm-12 col-12">
                            <div class="form-group">


                                <p style="color:#fff;" class="text-center"><a href="/forgotpassword" class="anchor">Forgot Password</a> |<a href="signup" class="anchor"> Create an Account</a></p>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-12 text-center" style="padding-bottom: 20px;">
                            <button class="btn btn-info" type="submit">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection