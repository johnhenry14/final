@extends('clientlayout.layouts.master2')
<!-- content -->
<div id="content" class="container" role="main">
    <div class="app-content-body ">
        <div class="bg-light lter b-b wrapper-md hidden-print">
            <a href class="btn btn-sm btn-info pull-right" onClick="window.print(); return false"><i class="fa fa-print" aria-hidden="true"></i>
                &nbsp; Print</a>

            {{--<h1 class="m-n font-thin h3">Invoice</h1>--}}
            <a href="http://www.maktoinc.com" target="_blank">
                <img src="{{asset('images/makto.png')}}"  alt="Decksys" class="decksysBrandLogo" style="width: 45px;">
            </a>
        </div>
        <div class="wrapper-md">
            <div>

                <div class="row">
                    <div class="col-xs-6">
			<a href="/">
                        <img src="{{asset('images/decksys.png')}}"  alt="Makto" class="decksysBrandLogo" style="margin-top: 40px;"></a></div>
                    <div class="col-xs-6 text-right">
                        @foreach($payment as $key => $invoice)
                            @if($key == 'status')
                                <h1><strong>{{$invoice}}</strong></h1>
                            @endif
                        @endforeach
                        @foreach($payment as $key => $invoice)
                            @if($key == 'date')
                                <h5>Invoice Date : <strong>{{$invoice}}</strong></h5>
                            @endif
                            @if($key == 'invoiceid')
                                <h5>Invoice Id : <strong>{{$invoice}}</strong></h5>
                            @endif

                        @endforeach
                    </div>
                </div>
                <div class="well m-t bg-light lt">
                    <div class="row">
                        <div class="col-xs-6">
                            <strong>TO:</strong>
                            @foreach($clientdetails as $key => $value)
                                @if($key == 'fullname')
                                    <h4>{{$value}}</h4>
                                @elseif($key == 'companyname')
                                    {{$value}}<br>
                                @elseif($key == 'phonenumber')
                                    Phone: {{$value}}<br>
                                @elseif($key == 'address1')
                                    {{$value}}<br>
                                @elseif($key == 'city')
                                    {{$value}}<br>
                                @elseif($key == 'state')
                                    {{$value}}<br>
                                @elseif($key == 'postcode')
                                    {{$value}}<br>
                                @elseif($key == 'email')
                                    Email: {{$value}}<br>
                                @endif
                            @endforeach
                                                    </div>
                        <div class="col-xs-6 text-right">
                            <strong>Pay To:</strong>
                            <h4>Makto Technology Pvt Ltd</h4>
                            <p>B2, First Floor, Rathinam Technical Campus<br>
                                Eachanari, Pollachi Main Road,<br>
                                Coimbatore 641 021, Tamilnadu, India.<br>
                                Email: info@maktoinc.com<br>
				GST No: 33AAKCM8132C1ZQ<br>
                                CALL : +91-73391 88891
                            </p>
                        </div>
                    </div>
                </div>
                <p class="m-t m-b">

                @foreach($result2['orders']['order']['0'] as $key => $value)
                    @if($key =='date')
                        <p class="m-t m-b">Order date: <strong>{{$value}}</strong><br>
                            @endif
                            @if($key =='status')
                                Order status: <span class="label bg-success">{{$value}}</span><br>
                            @endif
			@endforeach
                            
			@foreach($result2['orders']['order']['0'] as $key => $value)
				@if($key =='id')
                                 Order ID: <strong>{{$value}}</strong>
			        @endif
			@endforeach
                        </p>
                       

                        <div class="line"></div>
                        <table class="table table-striped bg-white b-a">
                            <thead>
                            <tr>
                                {{--<th style="width: 60px">QTY</th>--}}
                                <th>DESCRIPTION</th>
                                <th></th>
                                <th style="width: 90px">TOTAL</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($payment['items']['item'] as $key => $invoice)
			<tr>
			@for($key = 0; $key < 100; $key++)
                        @endfor
  			<td>{{$invoice['description']}}</td> <td></td>
			<td><i class="fa fa-inr"></i> {{$invoice['amount']}}</td>
   			</tr>

                        @endforeach
                            <tr>
                                <td class="text-right" colspan="2"><strong>Sub Total</strong></td>
                                @foreach($payment as $key => $invoice)
                                    @if($key == 'subtotal')
                                        <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                                    @endif
                                @endforeach
                            </tr>
                            @foreach($clientdetails as $key => $value)
                                @if($key =='state')

@if($value != 'Tamil Nadu')
<tr>
                        <td class="text-right" colspan="2">
<strong>18.00% IGST</strong>
@foreach($payment as $key => $invoice)
                            @if($key == 'tax2')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach

@else
<tr>
                        <td class="text-right" colspan="2"><strong>9.00% CGST</strong></td>
                        @foreach($payment as $key => $invoice)
                            @if($key == 'tax')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
</tr>
                    
<tr>
<td class="text-right" colspan="2"><strong>9.00% SGST</strong></td>

@foreach($payment as $key => $invoice)
                            @if($key == 'tax2')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach

@endif

@endif

@endforeach
</td>
                        


                    </tr>


                            <tr>
                                <td class="text-right" colspan="2"><strong>Credit</strong></td>
                                @foreach($payment as $key => $invoice)
                                    @if($key == 'credit')
                                        <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr>
                                <td class="text-right" colspan="2"><strong>Total</strong></td>
                                @foreach($payment as $key => $invoice)
                                    @if($key == 'total')
                                        <td><strong> <i class="fa fa-inr"></i> {{$invoice}}</strong></td>
                                    @endif
                                @endforeach
                            </tr>
                            </tbody>
                        </table>


                       <div class="table-responsive">          
  <table class="table table-striped bg-white b-a">
    <thead>
      <tr>
        {{--<th style="width: 60px">QTY</th>--}}
                                <th>Transaction Date</th>
                                <th>Gateway</th>
                                <th>Transaction ID</th>
                                <th>Total</th>
                                {{--<th style="width: 90px">TOTAL</th>--}}
      </tr>
    </thead>
    <tbody>
      @foreach($payment as $value)
@if($value == 'Unpaid')

<tr>
<td>-</td>
<td>-</td>
<td>-</td>
<td>-</td>
</tr>
 <tr><td></td><td></td>
                                <td> <strong>Balance</strong> </td>
                                @foreach($payment as $key => $value)
                                    @if($key == 'total')
                                        <td ><strong> <i class="fa fa-inr"></i> {{$value}}</strong></td>
                                    @endif
                                @endforeach
                            </tr>

@elseif($value == 'Paid')
<tr>
@foreach($payment['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'date')
                                        <td>{{$value}}</td>
                                    @endif
                                @endforeach
                                @foreach($payment['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'gateway')
                                        <td>{{$value}}</td>
                                    @endif
                                @endforeach

                                @foreach($payment['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'transid')
                                        <td>{{$value}}</td>
                                    @endif
                                @endforeach

                                @foreach($payment['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'amountin')
                                        <td> <i class="fa fa-inr"></i> {{$value}}</td>
                                    @endif
                                @endforeach 





                                     <tr><td></td><td></td>
                                <td> <strong>Balance</strong> </td>
                                @foreach($payment['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'amountout')
                                        <td ><strong> <i class="fa fa-inr"></i> {{$value}}</strong></td>
                                    @endif
                                @endforeach
                            </tr>
@endif

@endforeach                           
    </tbody>
  </table>
 
        </div>
    </div>
</div>
<!-- /content -->