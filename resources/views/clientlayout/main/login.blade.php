@extends('clientlayout.layouts.master')

@section('title')
SignUp
@endsection

@section('content')
<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Sign up</h1>
</div>
<div class="wrapper-md" ng-controller="FormDemoCtrl">
  <div class="row">
   
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading font-bold">create an account</div>
        <div class="panel-body">
          <form class="bs-example form-horizontal" action="{{route('login.signin')}}" method="post">
          <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
          
            <div class="form-group">
              <label class="col-lg-2 control-label">Email</label>
              <div class="col-lg-6">
                <input type="email" class="form-control" name="email" placeholder="Email">
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-lg-2 control-label">password</label>
              <div class="col-lg-6">
                <input type="password" class="form-control" name="password2" placeholder="password">
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" class="btn btn-sm btn-success">Submit</button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  
  
	</div>
  </div>
          @endsection