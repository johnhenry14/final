@extends('clientlayout.layouts.master')

@section('title')
Decksys | My Invoice
@endsection

@section('content')



    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h1 block">My Invoice</h1>
            </div>
            <div class="wrapper-md">
                <div class="panel panel-default">

                     
    <div class="table-responsive">
      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

<div class="row"><div class="col-sm-12"><table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
        <thead>
                                        <tr role="row">
                                            <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending">Invoice ID</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Name</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Company Name</th>
                                            <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Paid Date</th>
                                            <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Total Amount</th>
                                            <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Due Date</th>
                                            <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
					
                                        @if($invoice['totalresults'] == '0')
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>

                                        @else

                                            @foreach($invoice['invoices']['invoice'] as $value)


                                                <tr role="row">
                                                    <td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending">
                                                        <a href="/clientinvoice?invoice_id=<?=$value['id'];?>"  style="color:#23b7e5">
                                                            {{$value['id']}}
                                                        </a></td>
                                                    <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">{{$value['firstname']}} {{$value['lastname']}}</td>


                                                    <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['companyname']}}</td>
                                                    <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"> {{$value['datepaid']}}</td>
                                                    <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">₹ {{$value['total']}}</td>
                                                    <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">{{$value['duedate']}}</td>

                                                    <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">

                                                        @if($value['status'] == "Paid")
                                                            <button class="btn btn-success btn-sm btn-block">{{$value['status']}}</button></td>
                                                    @elseif($value['status'] =="Unpaid")
                                                        <button class="btn btn-info btn-sm btn-block">{{$value['status']}}</button></td>
                                                    @elseif($value['status'] =="Cancelled")
                                                        <button class="btn btn-primary btn-sm btn-block">{{$value['status']}}</button></td>
                                                    @elseif($value['status'] =="Overdue")
                                                        <button class="btn btn-danger btn-sm btn-block">{{$value['status']}}</button></td>
                                                    @elseif($value['status'] =="Draft")
                                                        <button class="btn btn-warning btn-sm btn-block">{{$value['status']}}</button></td>
                                                    @elseif($value['status'] =="Refunded")
                                                        <button class="btn btn-default btn-sm btn-block">{{$value['status']}}</button></td>


                                                    @endif


                                                </tr>





                                            @endforeach

                                        @endif


                                        </tbody>
                                    </table></div></div>
                            </div>
                    </div>

                </div>









            </div>
        </div>
    </div>

@endsection