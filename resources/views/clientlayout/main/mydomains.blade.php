@extends('clientlayout.layouts.master')
@section('title')
  Decksys | My Domains
@endsection

@section('content')
<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    
    
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h1 block">My Domains</h1>
</div>

       @if (Session::has('message'))
        <p style="text-align: center;color: green;font-size: 18px;">DNS Record Updated Successfully</p>
        @endif
		@if (Session::has('message_delete'))
        <p style="text-align: center;color: green;font-size: 18px;">DNS Record Deleted Successfully</p>
        @endif
 <!-- Button trigger modal -->

<div class="wrapper-md">
  <div class="panel panel-default">
   
    <div class="table-responsive">
      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

<div class="row"><div class="col-sm-12"><table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
        <thead>
          <tr role="row">
          <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Domain Name</th>
          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Registration Date</th>
          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Expiry Date</th>

         

<th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action </th>
         </tr>
        </thead>
        <tbody>

        @if($domain['totalresults'] == '0')
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
	

        @else
@foreach($domain['domains']['domain'] as $product)

        <tr role="row">
          <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">{{$product['id']}}
</td>
          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"
           aria-label="Browser: activate to sort column ascending">      
          
                    {{$product['domainname']}}

</td>
<td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
   
{{$product['regdate']}}
   </td>
          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
   
{{$product['expirydate']}}
   </td>
        
   
          
<td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending"> <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
   </button>
    <ul class="dropdown-menu">
      <li><a href=""  style="color:#23b7e5" data-toggle="modal" data-target="#myModal1">Managed DNS</a></li>
      <li><a href="#">Renew</a></li>
      </ul>
  </div>
</div>
</td>
          
         </tr>
  
@endforeach
          @endif
    
        </tbody>
      </table></div></div>
     </div>
    </div>
    
  </div>
  


	</div>
  </div>
  </div>
  
  
         <!-- Modal -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog-lg">
    <div class="modal-content1">
      <div class="modal-header">
       
        <h2 class="modal-title text-center" id="myModalLabel" >DNS Management</h2>
      </div>
      
      <div class="modal-body">
      <div id="exTab1" class="container">	
<ul  class="nav nav-pills">
			<li class="active">
                 <a  href="#1a" data-toggle="tab">A Records <i class="fa fa-info-circle" aria-hidden="true" title="Address Or A Records (Also Host Records) Are DNS's Central Records. They Link A Domain To An IP Address"></i></a>
			</li>

            <li>
              <a  href="#6a" data-toggle="tab">AAAA Records <i class="fa fa-info-circle" aria-hidden="true" title="Similar To A Record, An AAAA Record Points A Domain Or Subdomain To An IPv6 Address; But A Records Only Utilize IPv4 Addresses"></i></a>
			</li>
			<li><a href="#2a" data-toggle="tab">MX Records <i class="fa fa-info-circle" aria-hidden="true" title="MX Records Or Mail Exchange Records Are Basic Records For Directing Email For A Domain Name"></i></a>
			</li>
			<li><a href="#3a" data-toggle="tab">CNAME Records <i class="fa fa-info-circle" aria-hidden="true" title="CNAME Records Or Canonical Name Records Function As Aliases For Domain Names Of Other Canonical Domain Name."></i></a>
			</li>
  			<li><a href="#4a" data-toggle="tab">NS Records <i class="fa fa-info-circle" aria-hidden="true" title="NS Records or Nameservers Enable DNS To Translate A Domain Name To An IP Address. For Ex, https://69.162.97.219 Resolves To Your Domain: https://www.yourdomain.com/. Also They Break A Domain Into Subdomains"></i></a>
			</li>
			<li><a href="#5a" data-toggle="tab">TXT Records <i class="fa fa-info-circle" aria-hidden="true" title="TXT Records Or Text Records Are Custom Records & Comprise Data Readable By Human Beings."></i></a>
			</li>

		</ul>

			<div class="tab-content clearfix">
			  <div class="tab-pane active" id="1a">
			  <button  class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModala">Add A Record</button>

                <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
						   <thead>
						   <tr>
						      <th>Host Name</th>
							  <th>Destination IP Address</th>
							  <th>TTL</th>
							  <th>Status</th>
							  <th>Action</th>
						   </tr>
						   </thead>
                             <tbody>
							 <?php 
							 unset($response['recsindb']);
							 unset($response['recsonpage']);
							 $i=1;
							 foreach($response as $a_record){ ?>

							     <tr>
								 <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending"><?=$a_record['host']?></td>
								 <td><?=$a_record['value']?></td>
								
								 <td><?=$a_record['timetolive']?></td>
								 <td><?=$a_record['status']?></td>
								
<td>
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModalA<?=$i;?>"><i class="fa fa-edit"></i></button>
<a class="btn btn-danger" href="/deleteArecord?domainname=<?=base64_encode($a_record['host']);?>&ip=<?=base64_encode($a_record['value']);?>"><i class="fa fa-trash-o"></i></a></td>


<!-- Modal -->
<td>  
  
  <!-- Modal -->
  <div class="modal fade" id="myModalA<?=$i;?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Edit A Record</strong></h4>
        </div>
        <div class="modal-body">
          <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form" class="a_record_form" action="{{route('dns.modify')}}" method="POST">
		  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="form-group">
              <label>Host Name</label>
              <input type="text" name="domainname"  class="form-control">
            </div>
            <div class="form-group">
              <label>Current IPv4 Address <span calss="req" style="color:red">*</span></label>
              <input type="text" name="currentvalue" value="<?=$a_record['value']?>"  class="form-control" readonly>
            </div>


<div class="form-group">
              <label>New IPv4 Address <span calss="req" style="color:red">*</span></label>
              <input type="text" name="newvalue" class="form-control">
            </div>

<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" value="<?=$a_record['timetolive']?>"  class="form-control">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
        </div>
       
      </div>
      
    </div>
  </div>
  
</td>
  <!-- Modal -->
								 </tr>
							 <?php $i++; } ?>
							 </tbody>
                           </table>
<div class="container">
    <!-- Trigger the modal with a button -->
  

  <!-- Modal -->
  <div class="modal fade" id="myModala" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Add A Record</strong></h4>
        </div>
       <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form" class="a_record_form">
            <div class="form-group">
              <label>Host Name</label>
              <input type="text" name="domainname"  class="form-control">
            </div>
            <div class="form-group">
              <label>Destination IPv4 Address <span calss="req" style="color:red">*</span></label>
              <input type="text" name="value"  class="form-control">
            </div>

<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl"  class="form-control">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </div>
	      </div>
      
    </div>
  </div>
  
</div>

				</div>


			  <div class="tab-pane" id="6a">
<button  class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModalb">Add AAAA Record</button>

                       <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
						   <thead>
						   <tr>
						      	<th>Host Name</th>

							  <th>Destination IP Address</th>
							  <th>TTL</th>
							  <th>Status</th>
<th>Action</th>
						   </tr>
						   </thead>
                             <tbody>
							 <?php 
							 unset($response1['recsindb']);
							 unset($response1['recsonpage']);
							 $i=1;
							 foreach($response1 as $a_record){ ?>
							     <tr>
								 <td><?=$a_record['host']?></td>
								 <td><?=$a_record['value']?></td>
								 <td><?=$a_record['timetolive']?></td>
								 <td><?=$a_record['status']?></td>
																
<td>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModalAA<?=$i;?>"><i class="fa fa-edit"></i></button> 
<a  class="btn btn-danger" href="/deleteAAAArecord?domainname=<?=base64_encode($a_record['host']);?>&ip=<?=base64_encode($a_record['value']);?>"><i class="fa fa-trash-o"></i></a></td>

<!-- Modal -->
<td>  
  
  <!-- Modal -->
  <div class="modal fade" id="myModalAA<?=$i;?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Edit AAAA Record</strong></h4>
        </div>
        <div class="modal-body">
          <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form" class="a_record_form" action="{{route('AAAA.modify')}}" method="POST">
 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

            <div class="form-group">
              <label>Host Name</label>
              <input type="text" name="domainname"  class="form-control">
            </div>
            <div class="form-group">
              <label>Current IPv6 Address <span calss="req" style="color:red">*</span></label>
              <input type="text" name="currentvalue" value="<?=$a_record['value']?>" class="form-control" readonly>
            </div>

<div class="form-group">
              <label>New IPv6 Address <span calss="req" style="color:red">*</span></label>
              <input type="text" name="newvalue" class="form-control">
            </div>

<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" value="<?=$a_record['timetolive']?>"  class="form-control">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
        </div>
       
      </div>
      
    </div>
  </div>
  
</td>
  <!-- Modal -->
							</tr>
							<?php $i++;} ?>
							</tbody>
							</table>
          
<div class="container">
    <!-- Trigger the modal with a button -->
 

  <!-- Modal -->
  <div class="modal fade" id="myModalb" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Add AAAA Record</strong></h4>
        </div>
       <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form" class="a_record_form">
            <div class="form-group">
              <label>Host Name</label>
              <input type="text" name="domainname"  class="form-control">
            </div>
            <div class="form-group">
              <label>Destination IPv6 Address <span calss="req" style="color:red">*</span></label>
              <input type="text" name="value"  class="form-control">
            </div>

<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl"  class="form-control">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </div>
	      </div>
      
    </div>
  </div>
  
</div>

				</div>

				<div class="tab-pane" id="2a">
<button  class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModalc">Add MX Record</button>


<table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
						   <thead>
						   <tr>
						      <th>Name</th>
							  <th>Priority</th>
							  <th>Value</th>
							  <th>TTL</th>
							  <th>Status</th>
							  <th>Action</th>
						   </tr>
						   </thead>
                             <tbody>
							 <?php 
							 unset($response2['recsindb']);
							 unset($response2['recsonpage']);
							 $i=1;
							 foreach($response2 as $a_record){ ?>
							     <tr>
								 <td><?=$a_record['host']?></td> 
							        <td><?=$a_record['priority']?></td>
								 <td><?=$a_record['value']?></td>
								 <td><?=$a_record['timetolive']?></td>
								 <td><?=$a_record['status']?></td>
																
<td>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal2<?=$i;?>"><i class="fa fa-edit"></i></button> 
<a  class="btn btn-danger" href="/deleteMXrecord?domainname=<?=base64_encode($a_record['host']);?>&ip=<?=base64_encode($a_record['value']);?>"><i class="fa fa-trash-o"></i></a>

</td>

<!-- Modal -->
<td>  
  
  <!-- Modal -->
  <div class="modal fade" id="myModal2<?=$i;?>" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Edit MX Record</strong></h4>
        </div>
        <div class="modal-body">
          <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
         <form role="form" class="a_record_form" action="{{route('MX.modify')}}" method="POST">
 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="form-group">
              <label>Host Name</label>
              <input type="text" name="domainname"  class="form-control">
            </div>
<div class="form-group">
              <label>Value <span calss="req" style="color:red">*</span></label>
             <input type="radio" value="notfqdn" name="dataportion">
        <input type="text" name="value" value="" class="form-control">

        <input type="radio" value="fqdn" checked name="dataportion">
		<input type="text" name="newvaluefqdn" value="{{$a_record['value']}}" class="form-control">
            </div>
    
<div class="form-group">
              <label>Priority <span calss="req" style="color:red">*</span></label>
              <input type="text" name="priority" value="<?=$a_record['priority']?>"  class="form-control">
            </div>

<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" value="<?=$a_record['timetolive']?>"  class="form-control">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
        </div>
       
      </div>
      
    </div>
  </div>
  
</td>
  <!-- Modal -->

								 </tr>
							 <?php $i++;} ?>
							 </tbody>
                           </table>


          
<div class="container">
    <!-- Trigger the modal with a button -->
 

  <!-- Modal -->
  <div class="modal fade" id="myModalc" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Add MX Record</strong></h4>
        </div>
       <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
		<form role="form">
            <div class="form-group">
              <label>Zone</label>
              <input type="text" name="domainname" class="form-control">
            </div>
            <div class="form-group">
              <label>Value <span calss="req" style="color:red">*</span></label>
             <input type="radio" value="notfqdn" checked="" name="dataportion">
        <input type="text" name="value" value="" class="form-control">
     
        <input type="radio" value="fqdn" name="dataportion">
        <input type="text" name="valuefqdn" value="" class="form-control">
            </div>
				<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" class="form-control">
            </div>
			<div class="form-group">
              <label>Priority</label>
              <input type="text" name="priority" class="form-control">
            </div>
        
            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>

        </div>
      </div>
    </div>
    </div>
	      </div>
      
    </div>
  </div>
  
</div>
</div>

        <div class="tab-pane" id="3a">
<button  class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModald">Add CNAME Record</button>
<table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
						   <thead>
						   <tr>
						      <th>Name</th>
							  <th>Value</th>
							 
							  <th>TTL</th>
							  <th>Status</th>
							  <th>Action</th>
						   </tr>
						   </thead>
                             <tbody>
							 <?php 
							 unset($response3['recsindb']);
							 unset($response3['recsonpage']);
							 $i=1;
							 foreach($response3 as $a_record){ ?>
							     <tr>
								 <td><?=$a_record['host']?></td>
								 <td><?=$a_record['value']?></td>
								 <td><?=$a_record['timetolive']?></td>
								 <td><?=$a_record['status']?></td>
																
<td>
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal3<?=$i;?>"><i class="fa fa-edit"></i></button> 
<button class="btn btn-danger"> <i class="fa fa-trash"></i></button>
</td>

<!-- Modal -->
<td>  
  
  <!-- Modal -->
  <div class="modal fade" id="myModal3<?=$i;?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Edit CNAME Record</strong></h4>
        </div>
        <div class="modal-body">
          <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form" class="a_record_form" action="" method="POST">
		  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="form-group">
              <label>Zone</label>
              <input type="text" name="domainname" class="form-control">
            </div>
            <div class="form-group">
              <label>Value <span calss="req" style="color:red">*</span></label>
             <input type="radio" value="notfqdn" checked="" name="dataportion">
        <input type="text" name="value" value="" class="form-control">
     
        <input type="radio" value="fqdn" name="dataportion">
							 <input type="text" name="valuefqdn" value="{{$a_record['value']}}" class="form-control">
            </div>

<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" class="form-control">
            </div>
        <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
        </div>
       
      </div>
      
    </div>
  </div>
  
</td>
  <!-- Modal -->

							</tr>
							 <?php $i++;} ?>
							 </tbody>
                        </table>

<div class="container">
    <!-- Trigger the modal with a button -->
  

  <!-- Modal -->
  <div class="modal fade" id="myModald" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Add CNAME Record</strong></h4>
        </div>
       <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form">
            <div class="form-group">
              <label>Zone</label>
              <input type="text" name="domainname" class="form-control">
            </div>
            <div class="form-group">
              <label>Value <span calss="req" style="color:red">*</span></label>
             <input type="radio" value="notfqdn" checked="" name="dataportion">
        <input type="text" name="value" value="" class="form-control">
     
        <input type="radio" value="fqdn" name="dataportion">
        <input type="text" name="valuefqdn" value="" class="form-control">
            </div>
			<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" class="form-control">
            </div>
        <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </div>
	      </div>
      
    </div>
  </div>
  
</div>
				</div>

          <div class="tab-pane" id="4a">
<button  class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModale">Add NS Record</button>
<table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">

						   <thead>
						   <tr>
						      <th>Name</th>
							  <th>Value</th>
							  <th>TTL</th>
							  <th>Status</th>
							  <th>Action</th>
						   </tr>
						   </thead>
                             <tbody>
							 <?php 
							 unset($response4['recsindb']);
							 unset($response4['recsonpage']);
							 $i=0;
							 foreach($response4 as $a_record){ ?>
							     <tr>
								 <td><?=$a_record['host']?></td>
								 <td><?=$a_record['value']?></td>
								 <td><?=$a_record['timetolive']?></td>
								 <td><?=$a_record['status']?></td>
																
<td>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal4<?=$i;?>"><i class="fa fa-edit"></i></button> 
<a class="btn btn-danger" href="/deleteNSrecord?domainname=<?=base64_encode($a_record['host']);?>&value=<?=base64_encode($a_record['value']);?>"><i class="fa fa-trash-o"></i></a>

 </td>

<!-- Modal -->
<td>  
  
  <!-- Modal -->
  <div class="modal fade" id="myModal4<?=$i;?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Edit NS Record</strong></h4>
        </div>
        <div class="modal-body">
          <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form"class="a_record_form" action="{{route('NS.modify')}}" method="POST">
		  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="form-group">
              <label>Zone</label>
              <input type="text" name="domainname" class="form-control">
            </div>
            <div class="form-group">
              <label>Value <span calss="req" style="color:red">*</span></label>
             <input type="radio" value="notfqdn" checked="" name="dataportion">
        <input type="text" name="value" value="" class="form-control">
     
        <input type="radio" value="fqdn" name="dataportion">
        <input type="text" name="valuefqdn" value="{{$a_record['value']}}" class="form-control">
            </div>


				<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" value="{{$a_record['timetolive']}}" class="form-control">
            </div>

			<button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
        </div>
       
      </div>
      
    </div>
  </div>
  
</td>
  <!-- Modal -->
								 </tr>
							 <?php $i++;} ?>
							 </tbody>
                           </table>



         
<div class="container">
    <!-- Trigger the modal with a button -->
 

  <!-- Modal -->
  <div class="modal fade" id="myModale" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Add NS Record</strong></h4>
        </div>
       <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form">
            <div class="form-group">
              <label>Zone</label>
              <input type="text" name="domainname" class="form-control">
            </div>
            <div class="form-group">
              <label>Value <span calss="req" style="color:red">*</span></label>
             <input type="radio" value="notfqdn" checked="" name="dataportion">
        <input type="text" name="value" value="" class="form-control">
     
        <input type="radio" value="fqdn" name="dataportion">
        <input type="text" name="valuefqdn" value="" class="form-control">
            </div>



<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" value="" class="form-control">
            </div>

            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </div>
	      </div>
      
    </div>
  </div>
  
</div>







				</div>

	<div class="tab-pane" id="5a">
<button  class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModalf">Add TXT Record</button>
<table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">

						   <thead>
						   <tr>
						      <th>Name</th>
							  <th>Value</th>
							  
							  <th>TTL</th>
							  <th>Status</th>
							  <th>Action</th>
						   </tr>
						   </thead>
                             <tbody>
							 <?php 
							 unset($response5['recsindb']);
							 unset($response5['recsonpage']);
							 $i=1;
							 foreach($response5 as $a_record){ ?>
							     <tr>
								 <td><?=$a_record['host']?></td>
								 <td><?=$a_record['value']?></td>
							
								 <td><?=$a_record['timetolive']?></td>
								 <td><?=$a_record['status']?></td>
																
<td>

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal5<?=$i;?>"><i class="fa fa-edit"></i></button>
 <button class="btn btn-danger"> <i class="fa fa-trash"></i></button>
 </td>

<!-- Modal -->
<td>  
  
  <!-- Modal -->
  <div class="modal fade" id="myModal5<?=$i;?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Edit TXT Record</strong></h4>
        </div>
        <div class="modal-body">
          <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
           <form role="form" class="a_record_form" action="{{route('TXT.modify')}}" method="POST">
 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

            <div class="form-group">
              <label>Host Name</label>
              <input type="text" name="domainname"  class="form-control">
            </div>
            <div class="form-group">
              <label>Current Value <span calss="req" style="color:red">*</span></label>
              <input type="text" name="currentvalue" value="{{$a_record['value']}}" class="form-control">
            </div>

<div class="form-group">
              <label>New Value <span calss="req" style="color:red">*</span></label>
              <input type="text" name="newvalue" class="form-control">
            </div>

<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl" value="{{$a_record['timetolive']}}"  class="form-control">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>

        </div>
      </div>
    </div>
        </div>
       
      </div>
      
    </div>
  </div>
  
</td>
  <!-- Modal -->

								
								 </tr>
							 <?php $i++;} ?>
							 </tbody>
                           </table>



          
<div class="container">
    <!-- Trigger the modal with a button -->
 

  <!-- Modal -->
  <div class="modal fade" id="myModalf" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><strong>Add TXT Record</strong></h4>
        </div>
       <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
          <form role="form" class="a_record_form">
            <div class="form-group">
              <label>Host Name</label>
              <input type="text" name="domainname"  class="form-control">
            </div>
            <div class="form-group">
              <label>Destination IPv4 Address <span calss="req" style="color:red">*</span></label>
              <input type="text" name="value"  class="form-control">
            </div>

<div class="form-group">
              <label>TTL <span calss="req" style="color:red">*</span></label>
              <input type="text" name="ttl"  class="form-control">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </div>
	      </div>
      
    </div>
  </div>
  
</div>







				</div>
      <div class="modal-footer">
      
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      
    </div>
  </div>
</div>
  

  
  
  <script>
  $(document).on('click', '.a_record_fecth', function(){ 
  
	 var domain_name=$(this).attr('data-domain'); 
	 var ip=$(this).attr('data-ip'); 
	 var ttl=$(this).attr('data-ttl'); 
	 $('#domain_name').val(domain_name);
	 $('#ip_add').val(ip);
	 $('#ttl').val(ttl);
	 $( "#model_update_popup").model( "show" );
	 
  });
  </script>


  
@endsection