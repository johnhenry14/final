<?php

class ResellerClubAPI {
	// Configuration Of Reseller Club API
	public $api_user_id = "711757";
	public $api_key = "74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi";
	
	// List of TDL's - TLDs for which the domain name availability needs to be checked
	public $tlds_list = array("com", "net", "in", "biz", "org", "asia","in.net","co.in","co.uk");
	
	//Variable Declaration
	public $domainname;


	//Constructor
	public function __construct()
    {
		if(isset($_GET['domain'])){
		$this->domainname = preg_replace('/[\s-]+/', '-', strtolower(substr(trim($_GET['domain']), 0, 253)) );
		}
	}
	
	//Get Domain Availability ResellerClub API
	public function DomainAvailability()
	{	
	$tld = "";
	foreach($this->tlds_list as $arrayitem)	{ $tld.= '&tlds=' . $arrayitem;	}
	$url = 'https://httpapi.com/api/domains/available.json?auth-userid=' . $this->api_user_id . '&api-key=' . $this->api_key . '&domain-name=' . $this->domainname . $tld . '&suggest-alternative=true';
		//if ($data) $url = sprintf("%s?%s", $url, http_build_query($data));
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$apidata = curl_exec($curl);
	
	$apidata_json = json_decode($apidata, TRUE);
	return $apidata_json;
	}
	
	public function DomainSuggestion()
	{	
	//$tld = "";
	//foreach($this->tlds_list as $arrayitem)	{ $tld.= '&tlds=' . $arrayitem;	}
	$url = 'https://httpapi.com/api/domains/v5/suggest-names.json?auth-userid=' . $this->api_user_id . '&api-key=' . $this->api_key . '&keyword=' . $this->domainname;
	
	//if ($data) $url = sprintf("%s?%s", $url, http_build_query($data));
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$apidata = curl_exec($curl);
	
	$apidata_json = json_decode($apidata, TRUE);
	return $apidata_json;
	}
	
	public function FormValidation()
	{
		if (isset($_GET['check']) && preg_match('/^[a-z0-9\-]+$/i', $this->domainname)&& isset($_GET['domain']) != "" && $this->tlds_list) { return true; }
	    else { return false; }
	
	}
	}
	$api = new ResellerClubAPI;
?>

@extends('clientlayout.layouts.master')

@section('title')
   Register Domain
@endsection

@section('content')



<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Register Domain</h1>
</div>

<div class="wrapper-md" ng-controller="FormDemoCtrl">

  
   
    <div class="panel-body" style="background-color:transparent;">

    <form method="GET">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6 col-sm-12">
                    <div class="input-group">
                    <span class="input-group-addon" style="padding-left:10px; background-color: #999;" class='unclickable'>www</span>
                      <input type="text" name="domain" class="form-control" placeholder="Type Your Domain Name Here ..." value="<?php echo $api->domainname; ?>">
                      <span class="input-group-addon">
     
      <select class="form-control" name="tld" style="width: 100px;">
      <option value="com">com</option>
      <option value="in">in</option>
      <option value="org">org</option>
      <option value="co.in">co.in</option>
      <option value="in.net">in.net</option>
      <option value="asia">asia</option>
      <option value="net">net</option>
      <option value="biz">biz</option>
      <option value="info">info</option>
      <option value="co.uk">co.uk</option>
          </select>  
          </span>
                      <span class="input-group-addon">
          <button type="submit" class="btn btn-sm btn-success" name="check" style="padding: 7px;">Submit</button>  
          </span>

                      
                </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </form>

<!-- second row start-->
<?php
    if ($api->FormValidation())
    {
        $data = $api->DomainAvailability();
        $domain_price_value=0;
        ?>
        <form action="{{route('cart.domain')}}"  method="POST" id="domain_add_form">
        <input type="hidden" name="flag"  value="1" />
                   <input type="hidden" name="domain" id="domain_name" value="" />
                   <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                   <input type="hidden" name="price" id="price_domain" value="" />
</form>
<div class="row">
<div class="col-lg-offset-2 col-lg-8 col-sm-12" style="margin-top: 30px;">

<div class="panel no-border">
            <div class="panel-heading wrapper b-b b-light bg-info">
              
              <h4 class="font-thin m-t-none m-b-none text-white text-center"><b>Available Domains</b></h4>              
            </div>
            <table class="table table-striped m-b-none" >
          <thead>
            <tr>
              <th style="text-align: center;">Domain Name</th>
              <th style="text-align: center;">Status</th>
              <th style="text-align: center;">Price</th>
              <th style="text-align: center;">Action</th>
              
            </tr>
          </thead>
          <tbody>
               
                
               <?php
               foreach($api->tlds_list as $arrayitem)
               {
                   $fulldomainname = $api->domainname . "." . $arrayitem;
                   //echo $fulldomainname;

                   ?>
                   
                   <tr style="text-align: center;">
                       <td class="domainname"><?php echo $fulldomainname; ?></td>
                       <td><?php if ($data[$fulldomainname]["status"] == "available") { echo 'Available'; }
                       else { echo 'Not Available'; } ?>
                   </td>
                   <td>
                   <?php
                $i = 0;
                $colorid = 1;
                foreach ($resellersupporttld as $value) {

                    
                    // $domainname = "test." .$value;
                    $domainname = ".$value";

                    
                   // echo $domain;
                    //echo $domainname;
                    if ($domainname != "") {
                        $selectedtld = $domainname;
                        $count = substr_count($selectedtld, '.');
                        if ($count == 1) {
                            $split = explode(".", $selectedtld, 2);
                            $selectedtld = $split[1];
                            //echo $selectedtld;
                            if (in_array($selectedtld, $domarray)) {
                                $selectedtld = "dom" . $selectedtld;
                            } elseif ($split[1] == "com") {
                                $selectedtld = "domcno";
                            } else {
                                $selectedtld = "dot" . $selectedtld;
                            }
                        }
                        if ($count == 2) {
                            $split = explode(".", $selectedtld, 3);
                            if ($split[2] == "com" || $split[2] == "net") {

                                if (in_array($split[1], $standardcomarray)) {
                                    $selectedtld = "centralnicstandard";
                                } elseif (in_array($split[1], $premiumcomarray)) {
                                    $selectedtld = "centralnicpremium";
                                } else if ($split[1] == "cn" && $split[2] == "com") {
                                    $selectedtld = "centralniccncom";
                                }
                            } else if ($split[2] == "de" && $split[1] == "com") {
                                $selectedtld = "centralniccomde";
                            } else if ($split[2] == "org" && $split[1] == "ae") {
                                $selectedtld = "centralnicstandard";
                            } else {
                                $selectedtld = "thirdleveldot" . $split[2];
                            }
                        }
                    } else {

                        $selectedtld = "domcno";
                        //default
                    }
                    $domainprice = $datajson[$selectedtld]["addnewdomain"][1];
                   // echo $domainprice;
                    if ($domainprice == "") {
                        $domainprice = "Not Available";
                    } else {
                        ?>
                            <div style="float: none">
                            <div class="hosting_row_div">      
                                <div class="first_hosting_col_div"  
                                <?php
                                 if ($colorid % 2 == 0) { 
                                     ?> id="even" <?php
                                     }
                                  else { ?> id="odd"
                                  <?php } ?>
                                   style="text-align: center;" >
                                 
                                </div>
                            </div> 
                        </div>
                        <?php
                            
                        if($arrayitem== $value){
                            if($data[$fulldomainname]["status"] == "available"){
                                $domain_price_value=$domainprice;
                                print $domainprice;
                               
                            }
                            else{
                                echo '-';
                            }
                            //echo 637.7;
                          
                        }  
                        // elseif($arrayitem=="in.net"){
                        //     print $value.$domainprice;
                        // }    
                        $colorid++;
                        $i++;
                       
                    }
                }

              
                ?>     </td>
                
                <td>
                <?php
                if($data[$fulldomainname]["status"] == "available")
                { ?>
                  <button type="button" class="btn btn-success addtodomain" data-domain="<?php echo $fulldomainname; ?>" data-price="<?=$domain_price_value?>" >Add to Cart</button>
               
                <?php }
                else{
                    echo '-';
                }
                ?></td>
               </tr>
               
               <?php } ?>
               
           </tbody>
        </table>
          </div>
</div>
</div>
</div>
    <div class="row">
    <div class="col-lg-offset-2 col-lg-8 col-sm-12">

    <div class="panel no-border">
            <div class="panel-heading wrapper b-b b-light bg-primary">
              
              <h4 class="font-thin m-t-none m-b-none text-white text-center"><b>Suggestion Domains</b></h4>              
            </div>
            <table class="table table-striped m-b-none" >
          <thead>
            <tr>
              <th style="width: 350px;text-align:  center;">Domain Name</th>
              <th style="text-align:  center;">Status</th>
      
              
            </tr>
          </thead>
                <tbody>
          <?php
               $suggestions = $api->DomainSuggestion();
              
                    foreach($suggestions as $domain => $details)
                    {

                       
                       // var_dump ($details);
                        if ($details["status"] == "available")
                        {
                            echo '<tr style="text-align:  center;">
                            <td class="domainname">'.strtolower($domain).'</td>
                            <td>Available</td>
                            
                            </tr>';
                         
                		$flag = 1;
                        }
                         //echo $domain;
                         //echo $domainprice;
                       
                    }
                
				if(!isset($flag))
				{
					echo '<tr><td>No Suggestion Avaliable</td></tr>';
				}
                ?>
            </tbody>
        </table>
          </div>
</div>
</div>
</div>
<?php } ?>
</div>
</div><!-- /.container -->     
     
            
    </div>
  </div>
</div>

<script>
$('.addtodomain').click(function(e){
   $domain_name=$(this).attr('data-domain');
   $domain_price=$(this).attr('data-price');
   $('#domain_name').val($domain_name);
   $('#price_domain').val($domain_price);
   $('#domain_add_form').submit();
});
</script>
	</div>
@endsection