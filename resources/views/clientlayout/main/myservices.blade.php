@extends('clientlayout.layouts.master')

@section('title')
  Decksys | My Services
@endsection

@section('content')
<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h1 block">My Services</h1>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
   
     
    <div class="table-responsive">
      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

<div class="row"><div class="col-sm-12"><table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
        <thead>
          <tr role="row">
          <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Products/Services</th>
          <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Domain Name</th>
          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Registration Date</th>
          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Due Date</th>
          <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action </th>
         </tr>
        </thead>
        <tbody>
        @if($value['totalresults'] == '0')
            <td> - </td>
            <td> - </td>
            <td> - </td>
            <td> - </td>
            <td> - </td>
            <td> - </td>

        @else
        @foreach($value['products']['product'] as $key)


        <tr role="row">
          <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">{{$key['id']}}</td>
          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">
          <!--<a href="" style="color:#23b7e5" data-toggle="modal" data-target="#myModal">-->{{$key['groupname']}} - {{$key['name']}}</td>
          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['domain']}} </td>
          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['regdate']}}</td>
          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['nextduedate']}}</td>
          

          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
          <label class="checkbox-inline">
       <input type="checkbox" checked data-toggle="toggle">Auto Renew
</label>
</td>

         </tr>
         <!-- Modal -->
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog-lg">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
   
          <button type="button" class="close" data-dismiss="modal" >&times;</button>
          <h4 class="modal-title"><b>{{$key['groupname']}} - {{$key['name']}}</b></h4>
        </div>

        <div class="modal-body modal-header">
          <p>Domain Name - {{$key['domain']}}</p>
        </div>
   
        <div class="row" style="padding-top:20px;">
      <div class="col-lg-6">
              <a href="" class="block panel padder-v bg-primary item" >
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-center text-muted customFont" >Email Accounts</p>
               
              </a>
            </div>
  <div class="col-lg-6"  style="padding-left:20px;"><a href="" class="block panel padder-v bg-info item">
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-muted text-center customFont">Addon Domain</p>
                
              </a>
            </div>
      </div>
      

        <div class="row">
      <div class="col-lg-6">
              <a href="" class="block panel padder-v bg-info item" >
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-center text-muted customFont" >Add Sub Domain</p>
               
              </a>
            </div>
  <div class="col-lg-6"  style="padding-left:20px;"><a href="" class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-muted text-center customFont">change Primary Domain</p>
                
              </a>
            </div>
      </div>
       
      </div>
      
    </div>
  </div>

<!-- my modal closed -->
         @endforeach
            @endif
        
        </tbody>
      </table></div></div>
      </div>
    </div>
    
  </div>
  




 

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <div class="wrapper-md">
  <div class="row">
  <div class="col-lg-6 ">
              <a href="" class="block panel padder-v bg-default item">
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-center text-black customFont" >Shared Linux Hosting</p>
               
              </a>
            </div>
      </div>
      <div class="row">
      <div class="col-lg-6"><a href="" class="block panel padder-v bg-info item">
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-muted text-center customFont">Email Accounts</p>
                
              </a></div>
  <div class="col-lg-6"  style="padding-left:20px;">
              <a href="" class="block panel padder-v bg-primary item">
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-center text-muted customFont" >Add on Domain</p>
               
              </a>
            </div>
  
      </div>
      <div class="row">
      <div class="col-lg-6">
              <a href="" class="block panel padder-v bg-primary item" >
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-center text-muted customFont" >Add Sub Domain</p>
               
              </a>
            </div>
  <div class="col-lg-6"  style="padding-left:20px;"><a href="" class="block panel padder-v bg-info item">
                <span class="text-white font-thin h1 block text-center"></span>
                <p class="text-muted text-center customFont">change Primary Domain</p>
                
              </a>
            </div>
      </div>
 </div>
  </div>

</div>

 


	</div>
  </div>
  </div>
  
@endsection