<footer class="appFooter">
        <div class="footerContent">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <ul class="products">
                            <h4>Products</h4>
                            <li><a href="dedicatedServer">Dedicated Servers</a></li>
                            <li><a href="cloudHosting">Cloud Hosting</a></li>
                            <li><a href="wordpressHosting">Managed Wordpress</a></li>
                            <li><a href="smeHosting">SME / Shared Hosting</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <ul class="solutions">
                            <h4>Solutions</h4>
                            <li><a href="Freelancers">Agencies/Freelancers</a></li>
                            <li><a href="ecommerceHosting">eCommerce Hosting</a></li>
                            <!-- <li><a href="EnterpriseServer_Hosting">Enterprise Server Hosting</a></li> -->
                            <li><a href="Resellers">Resellers</a></li>
                         
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <ul class="support">
                            <h4>Support</h4>
                            <li><a href="">Knowledgebase</a></li>
                            <li><a href="signin">Ticket Center</a></li>
                            <li><a>Phone Support</a></li>

                            <li><a onclick="$crisp.push(['do', 'chat:open'])" style="cursor: pointer
							">Chat with an expert</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <ul class="contactUs">
                            <h4>Contact us</h4>
                            <li><a href="mailto:info@decksys.com">eMail:info@decksys.com</a></li>
                            <li><a href="#">Phone: +91 73977 67219</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="decksysSocialSupport">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
                        <p>Disclaimer.Decksys is a Brand of Makto Technology Private Limited</p>
                        <p>Copyrights 2018.<a href="http://www.maktoinc.com" target="_blank" style="color: #fff;">Makto Technology Private Limited</a></p>
                        <p style="color: #fff;"> <a href="Terms_Service" style="color: #fff;">Terms of Service</a> | <a href="Privacy_Policy" style="color: #fff;">Privacy Policy</a> | <a href="Refund_Policy" style="color: #fff;">Refund Policy</a></p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 socialConnect text-center">
                        <p>Connect On Social
                            <a href="https://twitter.com/decksysinf" target="_blank">
                                <img src="{{asset('images/twitter.png')}}" alt="Twitter" class="socialImageContainer">
                            </a>
                            <a href="https://www.facebook.com/decksysinf" target="_blank">
                                <img src="{{asset('images/facebook.png')}}" alt="Facebook" class="socialImageContainer">
                            </a>
                            <a href="https://www.linkedin.com/company/decksys" target="_blank">
                                <img src="{{asset('images/linkedin.png')}}" alt="Linkedin" class="socialImageContainer">
                            </a>
                        </p>
                    </div>
					<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 text-center">
                        <p>Payment Modes
                            
                            <img src="{{asset('images/visa.png')}}" alt="Visa" style="height: 25px;">
                            <img src="{{asset('images/mastercard.png')}}" alt="MasterCard" style="height: 25px;">
                            <img src="{{asset('images/american-express.png')}}" alt="American Express" style="height: 25px;">
                      		<img src="{{asset('images/rupay.png')}}" alt="Rupay" style="height: 25px;">
                         
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="3e55bc02-2699-4c82-970d-160c09b96dfd";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
 

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108125124-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'AW-845455057');
  gtag('config', 'UA-108125124-2');
</script>

<section class="helpline">
        <ul class="fixedHelpline">
            <li>
                <img src="{{asset('images/phone_ring.gif')}}" alt="Help">
                <p>Helpline : (91) 40 4596 7079</p>
            </li>
        </ul>
    </section>