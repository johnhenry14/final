@extends('layouts.master')
@section('title')
eCommerce Hosting - DeckSys, Brand of Makto Technology Private Limited
@endsection
@section('content')

    <section class="smeHostingSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <ul class="smeHostingTabsContainer">
                        <li class="active" data-tab="linuxHosting">
                            <p>Ecommerce Hosting</p>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </section>
	
	<section class="tabContentContainer dedicatedServer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <section class="tabContent active slideFromRight" id="linuxHosting">
                       @foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "8")
                               
                                @if($value['name'] == "Super Lite")
                                    @foreach($value['pricing'] as $key)
                <form action="{{route('cart.addCartEcommerce')}}" method="post">
                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
											<input type="hidden" name="package" value="" id="package"/>
											<input type = "hidden" name = "price" value = "" id="p_price">
                                            <input type="hidden" name="name1" value="{{$value['name']}}" />
                                            <input type="hidden" name="gid" value="{{$value['pid']}}" />
                                            <div class="d_s_ContentContainer">
                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                    <div class="d_s_Content">
                                                        <h4>{{$value['name']}}</h4>
                                                        <ul class="d_s_FeatureList">
                                                          <li><p>1 website</p></li>
                                <li><p>10 GB SSD storage </p>
                                </li>
                                <li>
                                    <p>SFTP access</p>
                                </li>
                                <li>
                                    <p>Free domain with annual plan</p>
                                </li>
<li>
								 <select class="form-control select_plan" style="width:95%" name="package_price" attr-x="Super Lite" attr-y="{{$value['pid']}}">
								<option value="1-monthly-{{$key['monthly']}}" selected>1 Month at ₹ {{$key['monthly']/1}} / Month</option>
								
								<option value="1-annually-{{$key['annually']/12}}">1 Year at ₹ {{$key['annually']/12}} / Year</option>
								<option value="2-biennially-{{$key['biennially']/24}}">2 Years at ₹ {{$key['biennially']/24}} / Year</option>
								<option value="3-triennially-{{$key['triennially']/36}}">3 Years at ₹ {{$key['triennially']/36}} / Year</option>
								
								</select>
								
								 </li>

                                       </ul>
@foreach($value['configoptions']['configoption'] as $configure) 

								 <div class="col-md-12" style="clear:both; padding: 0px">
<h4 class="text-center">{{$configure['name']}}</h4>
	<?php $i=0; ?>
@foreach($configure['options']['option'] as $options)

  
<label class="containerRadio">{{$options['name']}}  

<input type="radio" name="ecommerce_hostings" value="{{$configure['id']}}_{{$options['id']}}-{{$options['name']}}-{{$options['pricing']['INR']['monthly']}}" 
<?php if($i==0){ echo "checked";} else {echo "";} ?> >
<span class="checkmark"></span>
</label>
<?php $i++; ?>
@endforeach</div><br><br> @endforeach

<button type="submit" class="center-block">Buy Now</button>

                                                    </div>
                                                </div>
                                        </form>
                                   @endforeach
				   @elseif($value['name'] == "Lite")
                                    @foreach($value['pricing'] as $key)

   <form action="{{route('cart.addCartEcommerce')}}" method="post">
                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                            <input type="hidden" name="package" value="" id="package1"/>
											<input type = "hidden" name = "price" value = "" id="p_price1">
                                            <input type="hidden" name="name1" value="{{$value['name']}}" />
                                            <input type="hidden" name="gid" value="{{$value['pid']}}" />
                
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                <div class="d_s_Content">
                                                    <h4>{{$value['name']}}</h4>
                                                    <ul class="d_s_FeatureList">
                                                    <li><p>1 website</p></li>
													<li><p>15 GB SSD storage </p></li>
													<li><p>SSH/SFTP access</p></li>
													<li><p>Free domain with annual plan</p></li>
								<li>
								<select class="form-control select_plan1" style="width:95%" name="package_price" attr-x="Lite" attr-y="{{$value['pid']}}">
								<option value="1-monthly-{{$key['monthly']}}" selected>1 Month at ₹ {{$key['monthly']/1}} / Month</option>
								
								<option value="1-annually-{{$key['annually']/12}}" selected>1 Year at ₹ {{$key['annually']/12}} / Year</option>
								<option value="2-biennially-{{$key['biennially']/24}}">2 Years at ₹ {{$key['biennially']/24}} / Year</option>
								<option value="3-triennially-{{$key['triennially']/36}}">3 Years at ₹ {{$key['triennially']/36}} / Year</option>
								
								</select>
								
								 </li>
 

                                               </ul>
@foreach($value['configoptions']['configoption'] as $configure) 

								 <div class="col-md-12" style="clear:both; padding: 0px">
<h4 class="text-center">{{$configure['name']}}</h4>
	<?php $i=0; ?>
@foreach($configure['options']['option'] as $options)

  
<label class="containerRadio">{{$options['name']}}  

<input type="radio" name="ecommerce_hostings" value="{{$configure['id']}}_{{$options['id']}}-{{$options['name']}}-{{$options['pricing']['INR']['monthly']}}" 
<?php if($i==0){ echo "checked";} else {echo "";} ?> >
<span class="checkmark"></span>
</label>
<?php $i++; ?>
@endforeach</div><br><br> @endforeach
<button type="submit" class="center-block"> Buy Now </button>
                                                </div>
                                            </div>
                                        </form>
                                    @endforeach
				   @elseif($value['name'] == "Professional")
                                    @foreach($value['pricing'] as $key)

   <form action="{{route('cart.addCartEcommerce')}}" method="post">
                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                           <input type="hidden" name="package" value="" id="package2"/>
											<input type = "hidden" name = "price" value = "" id="p_price2">
                                            <input type="hidden" name="name1" value="{{$value['name']}}" />
                                            <input type="hidden" name="gid" value="{{$value['pid']}}" />
                
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                <div class="d_s_Content">
                                                    <h4>{{$value['name']}}</h4>
                                                    <ul class="d_s_FeatureList">
                                                    <li><p>5 website</p></li>
                                <li><p>30 GB SSD storage </p></li>
                                <li><p>SSH/SFTP access</p></li>
                                <li><p>Free domain with annual plan</p></li>
								<li>
								<select class="form-control select_plan2" style="width:95%" name="package_price" attr-x="Professional" attr-y="{{$value['pid']}}">
								<option value="1-monthly-{{$key['monthly']}}" selected>1 Month at ₹ {{$key['monthly']/1}} / Month</option>
								
								<option value="1-annually-{{$key['annually']/12}}" selected>1 Year at ₹ {{$key['annually']/12}} / Year</option>
								<option value="2-biennially-{{$key['biennially']/24}}">2 Years at ₹ {{$key['biennially']/24}} / Year</option>
								<option value="3-triennially-{{$key['triennially']/36}}">3 Years at ₹ {{$key['triennially']/36}} / Year</option>
								
								</select>
								
								 </li>
 

                                               </ul>
@foreach($value['configoptions']['configoption'] as $configure) 

								 <div class="col-md-12" style="clear:both; padding: 0px">
<h4 class="text-center">{{$configure['name']}}</h4>
	<?php $i=0; ?>
@foreach($configure['options']['option'] as $options)

  
<label class="containerRadio">{{$options['name']}}  

<input type="radio" name="ecommerce_hostings" value="{{$configure['id']}}_{{$options['id']}}-{{$options['name']}}-{{$options['pricing']['INR']['monthly']}}" 
<?php if($i==0){ echo "checked";} else {echo "";} ?> >
<span class="checkmark"></span>
</label>
<?php $i++; ?>
@endforeach</div><br><br> @endforeach
<button type="submit" class="center-block"> Buy Now </button>
                                                </div>
                                            </div>
                                        </form>

        @endforeach
		@elseif($value['name'] == "Ultimate")
                                    @foreach($value['pricing'] as $key)

   <form action="{{route('cart.addCartEcommerce')}}" method="post">
                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
										    <input type="hidden" name="package" value="" id="package3"/>
											<input type = "hidden" name = "price" value = "" id="p_price3">
                                            <input type="hidden" name="name1" value="{{$value['name']}}" />
                                            <input type="hidden" name="gid" value="{{$value['pid']}}" />
                
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                <div class="d_s_Content">
                                                    <h4>{{$value['name']}}</h4>
                                                    <ul class="d_s_FeatureList">
                                <li><p>5 website</p></li>
                                <li><p>30 GB SSD storage </p></li>
                                <li><p>SSH/SFTP access</p></li>
                                <li><p>Free domain with annual plan</p></li>
								<li>
								<select class="form-control select_plan3" style="width:95%" name="package_price" attr-x="Ultimate" attr-y="{{$value['pid']}}">
								<option value="1-monthly-{{$key['monthly']}}" selected>1 Month at ₹ {{$key['monthly']/1}} / Month</option>
								<option value="1-annually-{{$key['annually']/12}}" selected>1 Year at ₹ {{$key['annually']/12}} / Year</option>
								<option value="2-biennially-{{$key['biennially']/24}}">2 Years at ₹ {{$key['biennially']/24}} / Year</option>
								<option value="3-triennially-{{$key['triennially']/36}}">3 Years at ₹ {{$key['triennially']/36}} / Year</option>
								</select>
								
								 </li>
 

                                               </ul>
@foreach($value['configoptions']['configoption'] as $configure) 

								 <div class="col-md-12" style="clear:both; padding: 0px">
<h4 class="text-center">{{$configure['name']}}</h4>
	<?php $i=0; ?>
@foreach($configure['options']['option'] as $options)

  
<label class="containerRadio">{{$options['name']}}  

<input type="radio" name="ecommerce_hostings" value="{{$configure['id']}}_{{$options['id']}}-{{$options['name']}}-{{$options['pricing']['INR']['monthly']}}" 
<?php if($i==0){ echo "checked";} else {echo "";} ?> >
<span class="checkmark"></span>
</label>
<?php $i++; ?>
@endforeach</div><br><br> @endforeach
<button type="submit" class="center-block"> Buy Now </button>
                                                </div>
                                            </div>
                                        </form>
                                    @endforeach


    </section>
    @endif
    @endif
    @endforeach

    </div>
    </div>
    </div>
    </section>
	
    <section class="tabHostingFeatureContainer" style>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <section class="tabFeatureContent active slideFromRight" id="linuxHosting">
                        <h3>All our Ecommerce Hosting has these Feature</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddOnXS">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="magento"style="width: 86px;"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Magento</h4>
                                        <p>
                                            Secure the Website using Let's Encrypt available to all the hosting account at no cost.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="joomla"></div>
                                    <div class="a_fContentContainer">
                                        <h4>X - Cart</h4>
                                        <p style="margin-top: -5px !important;">
                                            Get a complete access over your website using cPanel backed by the powerful WHM
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="opencart"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Opencart</h4>
                                        <p>
                                            Deploy application from a list of 300+ availble one click installer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="woocommerce"></div>
                                    <div class="a_fContentContainer">
                                        <h4>WooCommerce</h4>
                                        <p>
                                            Managed IOPS, CPU, Memory, IO, number of Processes and Limits.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="prestashop"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Prestashop</h4>
                                        <p>
                                            Deploy application from a list of 300+ availble one click installer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="zencart"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Zencart</h4>
                                        <p>
                                            Managed IOPS, CPU, Memory, IO, number of Processes and Limits.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="chatFeature">
                            <div class="container-fluid">
                                <div class="row">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-4 col-lg-6">
<button class="btn btn-primary" style="background-color:#5348ae;border:0px;padding: 11px 30px 13px 30px;border-radius: 0px;"  onclick="$crisp.push(['do', 'chat:open'])">Chat With Experts</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                   
                </div>
            </div>
        </div>
    </section>
<section class="whatWeDo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="w_w_d_Content">
                        <p>
                            DeckSys is a service brand of Makto Technology Private Limited, providing solutions on hosting and IT infrastructure. Servers at DeckSys are backed by high security servers with 99.9% uptime.
                        </p>
                        <p>
                            The company provides range of products and services including Shared hosting, Dedicated servers, Business &amp; Enterprise eMaiI solutions, Linux KVM, Public / Private &amp; Hybrid Cloud servers.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="howToPay">
                        <p>Pay With : <img src="images/razorpay.png" alt="Razorpay" class="paymentMethod"></p>
                    </div>
                </div>
            </div>
        </div>
        </section>
		<script>


        $(document).on('change', '.select_plan', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id').val(pid);
            $('#package').val(package_name);
            $('#p_price').val(price);
        });
		
		$(document).on('change', '.select_plan1', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id1').val(pid);
            $('#package1').val(package_name);
            $('#p_price1').val(price);
        });
		
		$(document).on('change', '.select_plan2', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id2').val(pid);
            $('#package2').val(package_name);
            $('#p_price2').val(price);
        });
		
		
		$(document).on('change', '.select_plan3', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id3').val(pid);
            $('#package3').val(package_name);
            $('#p_price3').val(price);
        });
		</script>


@endsection