@extends('layouts.master')
@section('title')
DeckSys | Refund Policy
@endsection
@section('content')
<section class="CloudHostingSection">
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <ul class="CloudHostingTabsContainer">
                <li class="active" data-tab="CloudSME">
                    <p class="text-uppercase">Refund Policy</p>
                </li>
            </ul>
        </div>
    </div>
</div>
</section>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           
            <h4 class="text-left customHeading">RETURNS</h4>
            <p>Hosting plans will automatically expire until a plan is renewed. In order to cancel service, you must contact DeckSys  Billing Team by creating ticket in the members area with proper payment details and account details. Cancellation requests must be received by DeckSys Team with minimum of thirty (30) days prior to the end of your Billing Cycle. DeckSys will confirm the cancellation request when it is processed. If you do not receive a confirmation, please contact DeckSys as soon as possible.</p>
            <p>Strictly there are NO Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or refunds for special Promotional Packages, Discounted Packages or Coupons</p>
            <p>Strictly NO refunds will be processed for package that are cancelled during the FREE period. [ For example, In a 2 +1 year pack, the last year is provided as FREE and hence no refund can be processed for/during that period ]</p>
            <p>Strictly NO refunds will be processed if the account fee is not paid Full in advance before the account is setup</p>
            <p>Strictly NO refunds will be processed if a refund is requested after 90 days from the date of order of the account</p>
            <p>Strictly NO refunds will be processed if an account has violated the terms of service or involved in activities that has resulted in incurring charges to bring back services to normal</p>
            <p>Strictly NO refunds will be processed if an account was involved in abuse / resource over usage previously</p>
            <p>Domain Credits purchased cannot be refunded</p>
            <p>Only first time accounts are eligible for refund. Accounts created by the same customer / related to the same customer will not be eligible for refund</p>
            <p>Dedicated Servers and Cloud Servers are not eligible for any refunds.</p>
            <p>All request for refund must be provided in writing by creating a ticket in the members area. Only refund request that does not fall under any of the above conditions will be responded or taken into consideration. The maximum refund possible is 50 % of account fee that is paid in full advance before account setup.</p>
            <p>Setup fee [both levied or waived off ], Software costs, Administration costs, other third party expenses will not be refunded. No processing fee   will be charged for refunds. Refund processing will take 15 days from the date of acceptance of refund by DeckSys.</p>
            <p>No refund will be paid as cash or check / bank transfer or deposit. Refunds are credited as hosting credits or domain credits only.</p>
        </div>
    </div>
</div>

@endsection