@extends('clientlayout.layouts.master2')
<!-- content -->
<div id="content" class="container" role="main">
    <div class="app-content-body ">
        <div class="bg-light lter b-b wrapper-md hidden-print">
            <a href class="btn btn-sm btn-info pull-right" onClick="window.print(); return false"><i class="fa fa-print" aria-hidden="true"></i>
                &nbsp; Print</a>
   <a href="http://www.maktoinc.com" target="_blank">
<img src="{{asset('images/makto.png')}}"  alt="Makto" class="decksysBrandLogo"></a></div>
        </div>
        <div class="wrapper-md">
            <div>
               
                <div class="row">
 <div class="col-xs-6">
 <a href="http://www.decksys.com" target="_blank">
 <img src="{{asset('images/decksys.png')}}"  alt="Makto" class="decksysBrandLogo" style="margin-top:20px;"></a></div>
                    <div class="col-xs-6 text-right">
			@foreach($paymentinvoice as $key => $invoice)
				@if($key == 'status')
                                	<h1 class="font-initial">{{$invoice}}</h1>
                                @endif
			@endforeach
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'date')
                                <h5>Invoice Date : {{$invoice}}</h5>
                            @endif
                            @if($key == 'invoiceid')
                                <h5>Invoice Id : {{$invoice}}</h5>
                            @endif

                        @endforeach
                    </div>
                </div>
                <div class="well m-t bg-light lt">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6">
                            TO:
                            @foreach($clientdetails as $key => $value)
                                @if($key == 'fullname')
                                    <h4>{{$value}}</h4>
                                @elseif($key == 'companyname')
                                    {{$value}}<br>
                                @elseif($key == 'phonenumber')
                                    Phone: {{$value}}<br>
                                @elseif($key == 'address1')
                                    {{$value}}<br>
                                @elseif($key == 'city')
                                    {{$value}}<br>
                                @elseif($key == 'state')
                                    {{$value}}<br>
                                @elseif($key == 'postcode')
                                    {{$value}}<br>
                                @elseif($key == 'email')
                                    Email: {{$value}}<br>
                                @endif
                            @endforeach
                            
                        </div>
                        <div class="col-xs-6 col-sm-6 text-right">
                            Pay To:
                            <h4>Makto Technology Pvt Ltd</h4>
                            <p>B2, First Floor, Rathinam Technical Campus<br>
                                Eachanari, Pollachi Main Road,<br>
                                Coimbatore 641 021, Tamilnadu, India.<br>
                                Email: info@maktoinc.com<br>
				GST No: 33AAKCM8132C1ZQ<br>
                                CALL : +91 - 73391 88891
                            </p>
                        </div>
                    </div>
                </div>


      <p class="m-t m-b">
                    @foreach($result2['orders']['order']['0'] as $key => $value)
                        @if($key =='date')
                        <p class="m-t m-b">Order date: {{$value}}<br>
                            @endif
                        @if($key =='status')
                    Order status: <span class="label bg-success">{{$value}}</span><br>
                        @endif
                    @endforeach
                    Order ID: {{$order_id}}
               </p>
                <div class="line"></div>
                <table class="table table-striped bg-white b-a">
                    <thead>
                    <tr>
                        <th class="font-initial">DESCRIPTION</th>
                        <th></th>
                        <th style="width: 150px" class="font-initial">TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>
					

                        @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			<tr>
			@for($key = 0; $key < 100; $key++)
                        @endfor
  		

 <td>
{{$invoice['description']}} 
<!--@if($invoice['type']=="Hosting")

$chain=$invoice['description'];
$regex = '/^([a-zA-Z0-9\-\s\(\/]*\))/';
$data['regex'] = $regex;
$data['chain'] = $chain;
if(preg_match( $regex, $chain, $matches)){
    $remaining = str_replace($matches[0], "", $chain);
    $parts = explode(':', $remaining);
    $data['match'] = $matches[0];
}
print_r($data['match']);
?>
@elseif($invoice['type']=="DomainRegister")
{{$invoice['description']}}
@elseif($invoice['type']=="DomainTransfer")
{{$invoice['description']}}
@else
{{$invoice['description']}}	
@endif-->
</td>
<td></td>
<td><i class="fa fa-inr"></i> {{$invoice['amount']}}</td>
   			</tr>

                        @endforeach
                
                    <tr>
                        <td class="text-right" colspan="2">Sub Total</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'subtotal')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    

                    
@foreach($clientdetails as $key => $value)
                                @if($key =='state')

@if($value != 'Tamil Nadu')
<tr>
                        <td class="text-right" colspan="2">
18.00% IGST
@foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach

@else
<tr>
                        <td class="text-right" colspan="2">9.00% CGST</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
</tr>
                    
<tr>
<td class="text-right" colspan="2">9.00% SGST</td>

@foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach

@endif

@endif

@endforeach
</td>
                        


                    </tr>



                    <tr>
                        <td class="text-right" colspan="2">Credit</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'credit')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-right" colspan="2">Total</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'total')
                                <td><i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    </tbody>
                </table>


                        <div class="line"></div>
<div class="table-responsive">
                        <table class="table table-striped bg-white b-a">
                            <thead>
                            <tr>
                                {{--<th style="width: 60px">QTY</th>--}}
                                <th class="font-initial">Transaction Date</th>
                                <th class="font-initial">Gateway</th>
                                <th class="font-initial">Transaction ID</th>
                                <th class="font-initial">Total</th>
                               {{--<th style="width: 90px">TOTAL</th>--}}
                            </tr>

                            </thead>
                            <tbody>
                            <tr>
                                @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'date')
                                        <td>{{$value}}</td>
                                    @endif
                                @endforeach
                                @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'gateway')
                                            <td>{{$value}}</td>
                                        @endif
                                    @endforeach

                                    @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'transid')
                                            <td>{{$value}}</td>
                                        @endif
                                    @endforeach

                                    @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'amountin')
                                            <td> <i class="fa fa-inr"></i> {{$value}}</td>
                                        @endif
                                    @endforeach

                            </tr>
                            <tr><td></td><td></td>
                            <td> Balance </td>
                                @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'amountout')
                                        <td ><i class="fa fa-inr"></i> {{$value}}</td>
                                    @endif
                                @endforeach
                            </tr>


                            </tbody>
                        </table>


            </div>
</div>
        </div>
    </div>
</div>
<!-- /content -->