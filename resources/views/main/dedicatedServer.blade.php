@extends('layouts.master')
@section('title')
    DeckSys | Shared Linux and Windows Hosting, Dedicated Servers, VPS & Cloud Hosting, Coimbatore India
@endsection
@section('content')
    <section class="smeHostingSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <ul class="smeHostingTabsContainer">
                        <li class="active" data-tab="linuxHosting">
                            <p>Dedicated Server</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="tabContentContainer dedicatedServer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <section class="tabContent active slideFromRight" id="linuxHosting">
                        <!-- <h4>Get it up in below 30 minutes</h4> -->
						<?php //echo "<pre>";print_r($products['products']['product']);exit; ?>
                        @foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "6")
                               
                                @if($value['name'] == "Xeon E3 1230v3 Quad Core Server")
                                    @foreach($value['pricing'] as $key)
                                        <form action="{{route('cart.add_dedicated_server')}}" method="post">
                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                            <input type="hidden" name="name1" value="{{$value['name']}}" />
					    <input type="hidden" name="msetupfee" value="{{$key['msetupfee']}}" />
                                            <input type="hidden" name="price" value="{{$key['monthly']}}" />
                                            <input type="hidden" name="gid" value="{{$value['pid']}}" />
                                            <div class="d_s_ContentContainer">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                    <div class="d_s_Content">
                                                        <h4>{{$value['name']}}</h4>
                                                        <ul class="d_s_FeatureList">
                                                            <li>Xeon E3 1230v3 Quad Core Server</li>
<li>8 GB DDR3 ECC</li>
<li>1 TB SATA Enterprise</li>
<li>5 IP Pool (5 Usable)</li>
<li>100 Mbps</li>
<li>5 TB</li>
</ul>

<h4 style="background-color:#00adef;margin: 0px;"><i class="fa fa-inr"></i> {{$key['monthly']}}</h4>   

<?php foreach($value['configoptions']['configoption'] as $configure){ 
if($configure['name']=='Server Addons - OS')
{
	$label="Operating System";$name="os";
}
elseif ($configure['name']=='Server Addons - Database')
{
	$label="Database";$name="db";
}
else
{
	$label="Control Panel";$name="cp";
}
 ?>
<div class="col-md-12" style="clear:both; padding: 0px">
<h4 class="text-center">{{$label}}</h4>

<?php foreach($configure['options']['option'] as $options)
{
  ?>

<label class="containerRadio">{{$options['name']}}  
@if($options['pricing']['INR']['monthly'] == '0.00')
	@elseif($options['pricing']['INR']['monthly'] != '0.00')- <i class="fa fa-inr"></i>{{$options['pricing']['INR']['monthly']}}
@endif




<input type="radio" name="{{$name}}" value="{{$configure['id']}}_{{$options['id']}}-{{$options['name']}}-{{$options['pricing']['INR']['monthly']}}" checked="checked">
<span class="checkmark"></span>
</label>

<?php } ?> </div><br><br> <?php } ?>
<button type="submit">Buy Now
                                                        </button>
                                                    </div>
                                                </div>
                                        </form>
                                    @endforeach
                                @elseif($value['name'] =="Xeon E3 1240v5 Quad Core Server")
                                    @foreach($value['pricing'] as $key)
                                        <form action="{{route('cart.add_dedicated_server')}}" method="post">
                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                            <input type="hidden" name="name1" value="{{$value['name']}}" />
                                            <input type="hidden" name="price" value="{{$key['monthly']}}" />
                                            <input type="hidden" name="gid" value="{{$value['pid']}}" />
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="d_s_Content">
                                                    <h4>{{$value['name']}}</h4>
                                                    <ul class="d_s_FeatureList">
                                                        <li>Xeon E3 1240v5 Quad Core Server</li>
<li>8 GB DDR3 ECC</li>
<li>1 TB SATA Enterprise</li>
<li>5 IP Pool (5 Usable)</li>
<li>100 Mbps</li>
<li>5 TB</li>  
 

                                               </ul>
<h4 style="background-color:#00adef;margin: 0px;"><i class="fa fa-inr"></i> {{$key['monthly']}}</h4>   
<?php foreach($value['configoptions']['configoption'] as $configure){ 
if($configure['name']=='Server Addons - OS')
{
	$label="Operating System";$name="os";
}
elseif ($configure['name']=='Server Addons - Database')
{
	$label="Database";$name="db";
}
else
{
	$label="Control Panel";$name="cp";
}
 ?>
<div class="col-md-12" style="clear:both;padding: 0px">
<h4 class="text-center">{{$label}}</h4>
<?php foreach($configure['options']['option'] as $options)
{
  ?>
  
<label class="containerRadio">{{$options['name']}} 
@if($options['pricing']['INR']['monthly'] == '0.00')
	@elseif($options['pricing']['INR']['monthly'] != '0.00')- <i class="fa fa-inr"></i>{{$options['pricing']['INR']['monthly']}}
@endif

<input type="radio" name="{{$name}}" value="{{$configure['id']}}_{{$options['id']}}-{{$options['name']}}-{{$options['pricing']['INR']['monthly']}}" checked="checked">
<span class="checkmark"></span>
</label>

<?php } ?> </div><br><br> <?php } ?>  
<button type="submit"> Buy Now </button>
                                                </div>
                                            </div>
                                        </form>
                                    @endforeach
                                @elseif($value['name'] =="Xeon E3 1270v5 Quad Core Server")
                                    @foreach($value['pricing'] as $key)
                                        <form action="{{route('cart.add_dedicated_server')}}" method="post">
                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                            <input type="hidden" name="name1" value="{{$value['name']}}" />
                                            <input type="hidden" name="price" value="{{$key['monthly']}}" />
                                            <input type="hidden" name="gid" value="{{$value['pid']}}" />
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="d_s_Content">
                                                    <h4>{{$value['name']}}</h4>
                                                    <ul class="d_s_FeatureList">
                                                       <li>Xeon E3 1270v5 Quad Core Server</li>
<li>8 GB DDR3 ECC</li>
<li>1 TB SATA Enterprise</li>
<li>5 IP Pool (5 Usable)</li>
<li>100 Mbps</li>
<li>5 TB</li> 

</ul>
<h4 style="background-color:#00adef;margin: 0px;"><i class="fa fa-inr"></i> {{$key['monthly']}}</h4>   
<?php foreach($value['configoptions']['configoption'] as $configure){ 
if($configure['name']=='Server Addons - OS')
{
	$label="Operating System";$name="os";
}
elseif ($configure['name']=='Server Addons - Database')
{
	$label="Database";$name="db";
}
else
{
	$label="Control Panel";$name="cp";
}
 ?>
<div class="col-md-12" style="clear:both; padding: 0px">
<h4 class="text-center">{{$label}}</h4>
<?php foreach($configure['options']['option'] as $options)
{
  ?>
  
<label class="containerRadio">{{$options['name']}} 
@if($options['pricing']['INR']['monthly'] == '0.00')
	@elseif($options['pricing']['INR']['monthly'] != '0.00')- <i class="fa fa-inr"></i>{{$options['pricing']['INR']['monthly']}}
@endif
<input type="radio" name="{{$name}}" value="{{$configure['id']}}_{{$options['id']}}-{{$options['name']}}-{{$options['pricing']['INR']['monthly']}}" checked="checked">
<span class="checkmark"></span>
</label>

<?php } ?> </div><br><br> <?php } ?><button type="submit">Buy Now</button>
                                                </div>
                                            </div>
                </div>
                </form>

        @endforeach


    </section>
    @endif
    @endif
    @endforeach

   



    </div>
    </div>
    </div>
    </section>


    <section class="tabHostingFeatureContainer d_s_thfOverride">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <section class="tabFeatureContent active slideFromRight" id="linuxHosting">
                        <h3>All our Linux Dedicated Server Contains</h3>

                        <div class="d_s_FeatureContainer">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <ul class="d_s_FeatureList">
                                    <li>
                                        <p>Load Balancer</p>
                                    </li>
                                    <li>
                                        <p>Free Firewall</p>
                                    </li>
                                    <li>
                                        <p>1 Year Softaculous</p>
                                    </li>
                                    <li>
                                        <p>Ping Monitoring</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <ul class="d_s_FeatureList">
                                    <li>
                                        <p>Hardware Replacement</p>
                                    </li>
                                    <li>
                                        <p>Free Setup</p>
                                    </li>
                                    <li>
                                        <p>Free Remote Reboot & Root Access</p>
                                    </li>
                                    <li>
                                        <p>Free IP v6 - 256 (On Demand)</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <ul class="d_s_FeatureList">
                                    <li>
                                        <p>24x7x365 Technical Support</p>
                                    </li>
                                    <li>
                                        <p>Quick & Easy Migration</p>
                                    </li>
                                    <li>
                                        <p>99.98% Uptime</p>
                                    </li>
                                    <li>
                                        <p>5 Free IPv4 Addresses</p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="chatFeature">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                     <button class="btn btn-primary center-block" style="background-color:#5348ae;border:0px;padding: 11px 30px 13px 30px;border-radius: 0px;"  onclick="$crisp.push(['do', 'chat:open'])">Chat With Experts</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="tabFeatureContent slideFromRight" id="windowsHosting">
                        <h3>All our Windows Dedicated Server Contains</h3>
                        <div class="d_s_FeatureContainer">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <ul class="d_s_FeatureList">
                                    <li>
                                        <p>Load Balancer</p>
                                    </li>
                                    <li>
                                        <p>Free Firewall</p>
                                    </li>
                                    <li>
                                        <p>1 Year Softaculous</p>
                                    </li>
                                    <li>
                                        <p>Ping Monitoring</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <ul class="d_s_FeatureList">
                                    <li>
                                        <p>Hardware Replacement</p>
                                    </li>
                                    <li>
                                        <p>Free Setup</p>
                                    </li>
                                    <li>
                                        <p>Free Remote Reboot & Root Access</p>
                                    </li>
                                    <li>
                                        <p>Free IP v6 - 256 (On Demand)</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <ul class="d_s_FeatureList">
                                    <li>
                                        <p>24x7x365 Technical Support</p>
                                    </li>
                                    <li>
                                        <p>Quick & Easy Migration</p>
                                    </li>
                                    <li>
                                        <p>99.98% Uptime</p>
                                    </li>
                                    <li>
                                        <p>8 Free IPv4 Addresses</p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="chatFeature">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <button class="btn btn-primary" style="background-color:#5348ae;border:0px;padding: 11px 30px 13px 30px;border-radius: 0px;"  onclick="$crisp.push(['do', 'chat:open'])">Chat With Experts</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <section class="whatWeDo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="w_w_d_Content">
                        <p>
                            DeckSys is a service brand of Makto Technology Private Limited, providing solutions on hosting and IT infrastructure. Servers at DeckSys are backed by high security servers with 99.9% uptime.
                        </p>
                        <p>
                            The company provides range of products and services including Shared hosting, Dedicated servers, Business & Enterprise eMaiI solutions, Linux KVM, Public / Private & Hybrid Cloud servers,.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="howToPay">
                        <p>Pay With : <img src="../images/hosting-features/razorpay.png" alt="Razorpay" class="paymentMethod"></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
	<script type="text/javascript">
	function calcscore(){
    var score = 0;
    $(".calc:checked").each(function(){
        score+=parseInt($(this).val(),10);
    });
    $("input[name=sum]").val(score)
}
$().ready(function(){
    $(".calc").change(function(){
        calcscore()
    });
});
	</script>

@endsection