@extends('clientlayout.layouts.master1')
<!-- content -->
<div id="content" class="container" role="main">
    <div class="app-content-body ">


        <div class="bg-light lter b-b wrapper-md hidden-print">
            <a href class="btn btn-sm btn-info pull-right" onClick="window.print();"><i class="fa fa-print" aria-hidden="true"></i>
                &nbsp; Print</a>
            {{--<a href class="btn btn-sm btn-info pull-right" onClick="window.print();"><i class="fa fa-download" aria-hidden="true"></i>--}}
                {{--Download</a>--}}
            <h1 class="m-n font-thin h3">Invoice</h1>
        </div>
        <div class="wrapper-md">
            <div>
                <img src="{{asset('images/makto.png')}}"  alt="Makto" class="decksysBrandLogo">
                <div class="row">
                    <div class="col-xs-6">


                        <h4>Makto Technology Pvt Ltd</h4>
                            <p>B2, First Floor, Rathinam Technical Campus<br>
                            Eachanari, Pollachi Main Road,<br>
                            Coimbatore 641 021, Tamilnadu, India.<br>
                            Email: info@maktoinc.com
                        </p>
                        <p>CALL : +91 – 73391 88891<br></p>
                    </div>
                    <div class="col-xs-6 text-right">
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'date')
                                <h5>Invoice Date : {{$invoice}}</h5>
                            @endif
                                <p class="h4">#9048392</p>
                        @endforeach
                    </div>
                </div>
                <div class="well m-t bg-light lt">
                    <div class="row">
                        <div class="col-xs-6">
                            <strong>TO:</strong>
                            @foreach($clientdetails as $key => $value)
                                @if($key == 'fullname')
                                    <h4>{{$value}}</h4>
                                @elseif($key == 'companyname')
                                    {{$value}}<br>
                                @elseif($key == 'phonenumber')
                                    Phone: {{$value}}<br>
                                @elseif($key == 'address1')
                                    {{$value}}<br>
                                @elseif($key == 'city')
                                    {{$value}}<br>
                                @elseif($key == 'state')
                                    {{$value}}<br>
                                @elseif($key == 'postcode')
                                    {{$value}}<br>
                                @elseif($key == 'email')
                                    Email: {{$value}}<br>

                                @endif
                            @endforeach

                            @foreach($paymentinvoice as $key => $invoice)
                                @if($key == 'paymentmethod')

                                    <p><strong>Payment Method : {{$invoice}}</strong></p>
                                @endif
                            @endforeach

                        </div>
                        <div class="col-xs-6 text-right">
                            <strong>Pay To:</strong>
                            <h4>Makto Technology Pvt Ltd</h4>
                            <p>B2, First Floor, Rathinam Technical Campus<br>
                                Eachanari, Pollachi Main Road,<br>
                                Coimbatore 641 021, Tamilnadu, India.<br>
                                Email: info@maktoinc.com
                            </p>
                            <p>CALL : +91 – 73391 88891<br></p>
                        </div>
                    </div>
                </div>
                <p class="m-t m-b">Order date: <strong>26th Mar 2013</strong><br>
                    Order status: <span class="label bg-success">Shipped</span><br>
                    Order ID: <strong>#9399034</strong>
                </p>
                <div class="line"></div>
                <table class="table table-striped bg-white b-a">
                    <thead>
                    <tr>
                        {{--<th style="width: 60px">QTY</th>--}}
                        <th>DESCRIPTION</th>
                        <th style="width: 140px">UNIT PRICE</th>
                        <th style="width: 90px">TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($paymentinvoice['items']['item']['0'] as $key => $invoice)
                            @if($key == 'description')
                                <td>{{$invoice}}</td>
                            @endif
                            @if($key == 'amount')

                                <td>₹ {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Sub Total</strong></td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'subtotal')
                                <td>₹ {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-right"><strong></strong></td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax')
                                <td>₹ {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>

                    <tr>
                        <td>9.00% SGST</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                                <td>₹ {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td>Credit</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'credit')
                                <td>₹ {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td><strong>Total</strong></td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'total')
                                <td><strong>₹ {{$invoice}}</strong></td>
                            @endif
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>
<!-- /content -->