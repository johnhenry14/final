@extends('layouts.master')
@section('title')
DeckSys - Shared Hosting, Cloud Hosting, Dedicated Servers, VPS, Cloud VPS, Private Cloud, Rails Hosting
@endsection

@section('content')
<section class="supportSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h4>24x7 Managed support*, industry specific solutions</h4>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <p>Our support staff helps you round the clock, assist In bootstrapping and know what, you are behind secured firewalls. </p>
                </div>
            </div>
        </div>
    </section>

    <section class="hostingTypes">
        <div class="container-fluid">
            <div class="row">
              <a href="Cloud_Hosting">  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hosting clearfix hostingBox">
                    <div class="hostingTypeContainer">
                        <div class="hostingTypeImageContainer" id="cloudHosting"></div>
                        <div class="hostingTypeContent">
                            <h3 style="color:#000">Cloud Hosting</h3>
                            <p>
                              
                            </p>
                            <p style="color:#000">
                                Pricing at <i class="fa fa-inr" aria-hidden="true"></i> 349/mo
                            </p>
                        </div>
                    </div>
                </div></a>
               <a href="dedicated_server"> <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hosting clearfix hostingBox">
                    <div class="hostingTypeContainer">
                        <div class="hostingTypeImageContainer" id="dedicatedServer"></div>
                        <div class="hostingTypeContent">
                            <h3 style="color:#000">Dedicated Server</h3>
                            <p>
                               
                            </p>
                            <p style="color:#000">
                                Pricing at <i class="fa fa-inr" aria-hidden="true"></i> 9000/mo
                            </p>
                        </div>
                    </div></a>
                </div>
                <a href="Managed_wordpress"><div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hosting clearfix hostingBox">
                    <div class="hostingTypeContainer">
                        <div class="hostingTypeImageContainer" id="managedWordpress"></div>
                        <div class="hostingTypeContent">
                            <h3 style="color:#000">Managed Wordpress</h3>
                            <p>
                                
                            </p>
                            <p style="color:#000">
                                Pricing at <i class="fa fa-inr" aria-hidden="true"></i> 199/mo
                            </p>
                        </div>
                    </div>
                </div></a>
                <a href="SME_Hosting"><div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hosting clearfix hostingBox">
                    <div class="hostingTypeContainer">
                        <div class="hostingTypeImageContainer" id="smeSharedHosting"></div>
                        <div class="hostingTypeContent">
                            <h3 style="color:#000">SME / Shared Hosting</h3>
                            <p>
                               
                            </p>
                            <p style="color:#000">
                                Pricing at <i class="fa fa-inr" aria-hidden="true"></i> 139/mo
                            </p>
                        </div>
                    </div>
                </div></a>
            </div>
        </div>
    </section>

    <section class="featureSection">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="col-sm-offset-1 col-xs-12  col-sm-2 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="phoneSupport"></div>
                            <div class="featureContentContainer">
                                <h3>24 x 7 Support</h3>
                                <p>Talk With Our Experts</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="liveChat"></div>
                            <div class="featureContentContainer">
                                <h3>Live Chat</h3>
                                <p>Get Instant Update</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="upTime"></div>
                            <div class="featureContentContainer">
                                <h3>99.9% Uptime</h3>
                                <p>It's a Pinky Promise</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="submitTicket"></div>
                            <div class="featureContentContainer">
                                <h3>Submit a Ticket</h3>
                                <p>We Solve Your Hosting Issues</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="moneyBack">
                                
                            </div>
                            <div class="featureContentContainer">
                                <h3>30 Days Money back</h3>
                                <p>Guarantee</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-offset-1"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="clientsThoughtSection">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <h3>WHAT OUR CLIENTS SAY</h3>
                    <div class="clientThoughtContainer">
                        <p class="clientThought">
                            "Decksys has been excellent platform to host our website in terms of reliability , availability and Security."
                        </p>
                        <div class="client">
                            <p class="clientName">-Bashyam P</p>
                            <p class="clientCompanyName"><i>Precon Structures Pvt Limited</i></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
