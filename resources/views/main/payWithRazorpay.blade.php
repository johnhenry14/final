@extends('layouts.master')
@section('title')
    Decksys
@endsection

@section('content')
<section class="smeHostingSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <ul class="smeHostingTabsContainer">
                        <li>
                            <p>Review & Pay</p>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <div class="container">
                    <div class="well m-t bg-light lt" style="margin-top:20px;">
                    <div class="row">
                        <div class="col-xs-12 col-lg-6 col-sm-6">
                            <strong>FROM:</strong><br>
                        <!-- <img src="{{asset('images/makto.png')}}"  alt="Makto" class="decksysBrandLogo"> -->
                            <h4>Makto Technology Pvt Ltd</h4>
                            <p><a href="http://www.maktoinc.com">www.maktoinc.com</a></p>
                            <p>B2, First Floor, Rathinam Technical Campus<br>
                                Eachanari, Pollachi Main Road,<br>
                                Coimbatore 641 021, Tamilnadu, India.<br>
                                Email: info@maktoinc.com
                            </p>
                            <p>CALL : +91-73391 88891<br></p>
                        </div>
                        <div class="col-xs-12 col-lg-6 col-sm-6 textleft text-right">
                            <strong>TO:</strong>
                            @foreach($results as $key => $value)
                                @if($key == 'fullname')
                                    <h4>{{$value}}</h4>
                                @elseif($key == 'companyname')
                                    {{$value}}<br>
                                @elseif($key == 'phonenumber')
                                    Phone: {{$value}}<br>
                                @elseif($key == 'address1')
                                    {{$value}}<br>
                                @elseif($key == 'city')
                                    {{$value}}<br>
                                @elseif($key == 'state')
                                    {{$value}}<br>
                                @elseif($key == 'postcode')
                                    {{$value}}<br>
                                @elseif($key == 'email')
                                    Email: {{$value}}<br>
                                    </p>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="line"></div>
<div class="table-responsive">
                <table class="table bg-white b-a table-bordered">
                    <thead>
                    <tr>
                        <th style="width: 60px">PRODUCT</th>

                        <th style="width: 140px">PRICE</th>
                        <th style="width: 140px">TAX</th>
                        <th style="width: 90px">SUB TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cartdata as $key => $value)
                        <tr>
                            <td>{{$value->name}} - {{$value->options->package}}</td>

                            <td><i class="fa fa-inr"></i> {{$var=$value->price}} </td>

                            <td>GST @ 18.00% - <i class="fa fa-inr"></i> {{round($var1=$value->tax,2)}}<br>
                            </td>
                            <td><i class="fa fa-inr"></i> {{round($var+$var1,2)}}</td>
                        </tr>
                    @endforeach
                    <tr><td></td>
                        <td colspan="2" class="text-right no-border"><strong>Total</strong></td>
                        <td><strong><i class="fa fa-inr"></i> {{round(Cart::total(),2)}}</strong></td>
                    </tr>
                    </tbody>
                </table>
</div>
            </div>
        </div>


    </div>
    </div>
    <!-- /content -->

    </div>



    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>Error!</strong> {{ $message }}
                    </div>
                @endif
                {!! Session::forget('error') !!}
                @if($message = Session::get('success'))
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> {{ $message }}
                    </div>
                @endif
                {!! Session::forget('success') !!}
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-lg-4 text-right"><button id="rzp-button1" class="btn btn-primary btn-lg text-center">Pay With Razorpay</button></div>
            <div class="col-lg-offset-4"></div>
        </div>

    </div>
    <form method="post" id="razorpay_form" action="{{route('payment.success')}}">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
        <input type="hidden" id="payment_id" name="payment_id" />
        <input type="hidden" id="order_id" name="order_id" value="<?=$order_id;?>" />
        <input type="hidden" id="order_id" name="invoiceid" value="<?=$invoiceid;?>" />
    </form>

    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
        var options = {
            "key": "rzp_test_pu7EjHhQTbrbqk",
            //"amount":"1000",
            "amount": "{{round(Cart::total())*100}}", // 2000 paise = INR 20
            "name": "Decksys",
            "description": "Purchase Description",
            "image": "images/decksys.png",
            "handler": function (response){
                $('#payment_id').val(response.razorpay_payment_id);
                //$('#order_id').val();// php order_id values
                $('#razorpay_form').submit();

            },
            "prefill": {
                "name": "<?=$results['fullname'];?>",
                "email": "<?=$results['email'];?>",
                "contact": "<?=$results['phonenumber'];?>"
            },
            "notes": {
                "address": "<?=$results['address1'].''.$results['address2'];?>"
            },
            "theme": {
                "color": "#F37254"
            }
        };
        var rzp1 = new Razorpay(options);

        document.getElementById('rzp-button1').onclick = function(e){
            rzp1.open();
            e.preventDefault();
        }
    </script>
@endsection