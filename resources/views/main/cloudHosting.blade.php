@extends('layouts.master')
@section('title')
Cloud Hosting - DeckSys, Brand of Makto Technology Private Limited
@endsection
@section('content')
<section class="CloudHostingSection">
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <ul class="CloudHostingTabsContainer">
                <li class="active" data-tab="CloudSME">
                    <p>Cloud Hosting</p>
                </li>
            </ul>
        </div>
    </div>
</div>
</section>
<section class="tabContentContainer CloudHostingServer">
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
  

@foreach($products['products']['product'] as $key)
@if($key['gid']=="3")
@if($key['name']=="Cloud - Lite")
@foreach($key['pricing'] as $value)
                <div class="C_H_ContentContainer">
                <form action="{{route('cart.add_cloud_hosting')}}" method="post">
                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                        <input type="hidden" name="name" value="{{$key['name']}}" />
                        <input type="hidden" name="price" value="{{$value['monthly']}}" />
                        <input type="hidden" name="gid" value="{{$key['pid']}}" />
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-offset-2 col-lg-4">
                        <div class="C_H_Content">
                            
                            <h4>{{$key['name']}}</h4>
                            <ul class="C_H_FeatureList">
                                <li>
                                    <p>1 Website</p>
                                </li>
                                <li>
                                    <p>2 Cores vCPU</p>
                                </li>
                                <li>
                                    <p>1 Gb Space</p>
                                </li>
                                <li>
                                    <p>2 GB RAM</p>
                                </li>
                                <li>
                                    <p>10 GB Bandwidth</p>
                                </li>
                                <li>
                                <button><i class="fa fa-inr" aria-hidden="true"></i> {{$value['monthly']}}</button>
                                   
                                </li>    
                            </ul>
                       
                        </div>
                    </div>
                    </form>
                    @endforeach

                    @elseif($key['name'] == "Cloud - Super Lite")
@foreach($key['pricing'] as $value)

   <form action="{{route('cart.add_cloud_hosting')}}" method="post">
                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                        <input type="hidden" name="name" value="{{$key['name']}}" />
                        <input type="hidden" name="price" value="{{$value['monthly']}}" />
                        <input type="hidden" name="gid" value="{{$key['pid']}}" />

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="C_H_Content">
                            <h4>{{$key['name']}}</h4>
                            <ul class="C_H_FeatureList">
                                <li>
                                    <p>Unlimited Websites</p>
                                </li>
                                <li>
                                    <p>4 Cores vCPU</p>
                                </li>
                                <li>
                                    <p>Unlimited Space</p>
                                </li>
                                <li>
                                    <p>4 GB RAM</p>
                                </li>
                                <li>
                                    <p>Unlimited Bandwidth</p>
                                </li>
                                <li>
                                <button><i class="fa fa-inr" aria-hidden="true"></i> {{$value['monthly']}}</button>
                                   
                                </li>    
                            </ul>
                           
                        </div>
                    </div>
                    </form>
                    @endforeach

               <!-- @elseif($key['name'] == "Cloud - Professional")
@foreach($key['pricing'] as $value)
<form action="{{route('cart.add_cloud_hosting')}}" method="post">
                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                        <input type="hidden" name="name" value="{{$key['name']}}" />
                        <input type="hidden" name="price" value="{{$value['monthly']}}" />
                        <input type="hidden" name="gid" value="{{$key['pid']}}" />
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="C_H_Content">
                            <h4>{{$key['name']}}</h4>
                            <ul class="C_H_FeatureList">
                                <li>
                                    <p>Unlimited Websites</p>
                                </li>
                                <li>
                                    <p>8 Cores vCPU</p>
                                </li>
                                <li>
                                    <p>Unlimited Space</p>
                                </li>
                                <li>
                                    <p>8 GB RAM</p>
                                </li>
                                <li>
                                    <p>Unlimited Bandwidth</p>
                                </li>
                                <li>
                                <button><i class="fa fa-inr" aria-hidden="true"></i> {{$value['monthly']}}</button>
                                   
                                </li>       
                            </ul>
                            
                        </div>
                    </div>
                    </form>
                    @endforeach-->

                    @endif
@endif
@endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<section class="tabHostingFeatureContainer C_H_thfOverride">
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <section class="tabFeatureContent active slideFromRight" id="CloudHosting">
                    <h3>All our Cloud Hosting has these Feature</h3>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 paddOnXS">
                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                            <div class="availableFeatures">
                                <div class="a_fImageContainer" id="letsEncrypt"></div>
                                <div class="a_fContentContainer">
                                    <h4>SSL Layer</h4>
                                    <p>
                                        Secure the Website using Let's Encrypt available to all the hosting account at no cost.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                            <div class="availableFeatures">
                                <div class="a_fImageContainer" id="webHostManager"></div>
                                <div class="a_fContentContainer">
                                    <h4>Web Host Manager</h4>
                                    <p>
                                        Get a complete access over your website using cPanel backed by the powerful WHM
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                            <div class="availableFeatures">
                                <div class="a_fImageContainer" id="softaculous"></div>
                                <div class="a_fContentContainer">
                                    <h4>Softaculous</h4>
                                    <p>
                                        Deploy application from a list of 300+ availble one click installer.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                            <div class="availableFeatures">
                                <div class="a_fImageContainer" id="cloudLinux"></div>
                                <div class="a_fContentContainer">
                                    <h4>Light Weight Env</h4>
                                    <p>
                                        Managed IOPS, CPU, Memory, IO, number of Processes and Limits.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dividerCustom show_on_lg"></div>
                    <div class="asideF_L_Container">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3" style="margin-top: 35px;">
                            
                           <ul class="featuresList fa-ul" style="margin-top:-10px">
                                <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                    Support, PHP, Perl, Rails, MySql, Python
                                </li>
                                <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                    Domains, Subdomains and Advanced DNS management
                                </li>
                                <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                    AWStats to track website visitor logs and information
                                </li>
                                <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                    eMail Services on RoundCube / SquirrelMail / Horde
                                </li>
                                <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                        File Manager services or access files using FTP / FTPS over secured infrastructure.
                                </li>
                                <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                    30 days money back guarantee
                                </li>
                                <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                    Support Over Phone , eMail / chat round the year
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="chatFeature">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-4 col-lg-6">
<button class="btn btn-primary" style="background-color:#5348ae;border:0px;padding: 11px 30px 13px 30px;border-radius: 0px;"  onclick="$crisp.push(['do', 'chat:open'])">Chat With Experts</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>

<section class="acc" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <h2 class="text-center" style="color: #8781bd;"><span>FAQ</span></h2>
            <div class="col-md-12 offset-md-2">
                <div class="bd-example" data-example-id="">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <div class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        
                                        <h3 style="color: #00adef;">Why should I move to cloud?</h3>

                                    </a>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>

                            <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                                <div class="card-block" style="color: #8781bd;">
                                    Reduced IT costs. Moving to cloud computing may reduce the cost of managing and maintaining your IT systems.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingTwo">
                                <div class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                         <h3 style="color: #00adef;">What is the difference between Cloud Hosting and Shared Hosting?</h3>

                                    </a>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                                <div class="card-block" style="color: #8781bd;">
                                    Reduced IT costs. Moving to cloud computing may reduce the cost of managing and maintaining your IT systems.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingThree">
                                <div class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        <h3 style="color: #00adef;">How Relaiable is your Cloud Hosting?</h3>

                                    </a>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                                <div class="card-block" style="color: #8781bd;">
                                    Reduced IT costs. Moving to cloud computing may reduce the cost of managing and maintaining your IT systems.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <div class="mb-0">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                        
                                        <h3 style="color: #00adef;" class="disabled">Is a Dedicated IP available?</h3>

                                    </a>
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                            </div>

                            <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                                <div class="card-block" style="color: #8781bd;">
                                    Reduced IT costs. Moving to cloud computing may reduce the cost of managing and maintaining your IT systems.
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="whatWeDo">
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <div class="w_w_d_Content">
                <p>
                    DeckSys is a service brand of Makto Technology Private Limited, providing solutions on hosting and IT infrastructure. Servers at DeckSys are backed by high security servers with 99.9% uptime.
                </p>
                <p>
                    The company provides range of products and services including Shared hosting, Dedicated servers, Business & Enterprise eMaiI solutions, Linux KVM, Public / Private & Hybrid Cloud servers.
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <div class="howToPay">
                <p>Pay With : <img src="images/hosting-features/razorpay.png" alt="Razorpay" class="paymentMethod"></p>
            </div>
        </div>
    </div>
</div>
</section>
@endsection