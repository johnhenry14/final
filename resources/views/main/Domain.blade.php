<?php

class ResellerClubAPI {
	// Configuration Of Reseller Club API
	public $api_user_id = "711757";
	public $api_key = "74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi";
	
	// List of TDL's - TLDs for which the domain name availability needs to be checked
	public $tlds_list = array("com", "net", "in", "biz", "org", "asia","in.net","co.in","co.uk");
	
	//Variable Declaration
	public $domainname;


	//Constructor
	public function __construct()
    {
		if(isset($_GET['domain'])){
		$this->domainname = preg_replace('/[\s-]+/', '-', strtolower(substr(trim($_GET['domain']), 0, 253)) );
		}
	}
	
	//Get Domain Availability ResellerClub API
	public function DomainAvailability()
	{	
	$tld = "";
	foreach($this->tlds_list as $arrayitem)	{ $tld.= '&tlds=' . $arrayitem;	}
	$url = 'https://httpapi.com/api/domains/available.json?auth-userid=' . $this->api_user_id . '&api-key=' . $this->api_key . '&domain-name=' . $this->domainname . $tld . '&suggest-alternative=true';
		//if ($data) $url = sprintf("%s?%s", $url, http_build_query($data));
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$apidata = curl_exec($curl);
	
	$apidata_json = json_decode($apidata, TRUE);
	return $apidata_json;
	}
	
	public function DomainSuggestion()
	{	
	//$tld = "";
	//foreach($this->tlds_list as $arrayitem)	{ $tld.= '&tlds=' . $arrayitem;	}
	$url = 'https://httpapi.com/api/domains/v5/suggest-names.json?auth-userid=' . $this->api_user_id . '&api-key=' . $this->api_key . '&keyword=' . $this->domainname;
	
	//if ($data) $url = sprintf("%s?%s", $url, http_build_query($data));
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$apidata = curl_exec($curl);
	
	$apidata_json = json_decode($apidata, TRUE);
	return $apidata_json;
	}
	
	public function FormValidation()
	{
		if (isset($_GET['check']) && preg_match('/^[a-z0-9\-]+$/i', $this->domainname)&& isset($_GET['domain']) != "" && $this->tlds_list) { return true; }
	    else { return false; }
	
	}
	}
	$api = new ResellerClubAPI;
?>

@extends('layouts.master')
@section('title')
DeckSys | Shared Linux and Windows Hosting, Dedicated Servers, VPS & Cloud Hosting, Coimbatore India
@endsection
@section('content')
<section class="CloudHostingSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-12 col-lg-12">
                    <ul class="CloudHostingTabsContainer">
                        <li class="active" style="font-size:20px;">
                            <p>Domains</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<div class="container">
        <div class="row" style="margin-top:20px; " >
        <div class="col-lg-offset-1 col-lg-10 col-sm-11">
            <div class="thumbnail" style="padding: 0px;">
            <div class="caption" style="padding: 0px;">
            
<div style="background-color: #23b7e5;padding: 15px;">
@if (Session::has('message'))
        <p style="text-align: center;color: #474e9e;font-size: 18px;">Domain Nameservers Updated Successfully</p>
        @endif
<div id="work-option">
     
        <div class="options col-lg-offset-2" style="color: #FFF;"> 
            <input type="radio" name="work" value="wc" id="r1" style="margin-left: 50px;">
			<label for="r1">Register Domain</label>
		    <input type="radio" name="work" value="woc" id="r2" style="margin-left: 30px;">
			<label for="r2">Transfer your domain from another registrar</label>
            
          
     
        </div>
    </div>


 <div class="with-us" style="margin: 35px 0px 25px 0px;">
       
  <form method="GET" id="nameserver_text">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6 col-sm-12">
                    <div class="input-group">
                    <span class="input-group-addon" style="padding-left:10px; background-color: #999;" class='unclickable'>www</span>
                      <input type="text" name="domain" class="form-control" placeholder="Register your Domain" value="<?php echo $api->domainname; ?>">
                      <span class="input-group-addon">
     
      <select class="form-control" name="tld" style="width: 100px;">
      <option value="com">com</option>
      <option value="in">in</option>
      <option value="org">org</option>
      <option value="co.in">co.in</option>
      <option value="in.net">in.net</option>
      <option value="asia">asia</option>
      <option value="net">net</option>
      <option value="biz">biz</option>
      <option value="info">info</option>
      <option value="co.uk">co.uk</option>
          </select>  
          </span>
                      <span class="input-group-addon">
          <button type="submit" class="btn btn-sm btn-success" name="check" style="padding: 7px; margin-left: 20px; ">Submit</button>  
          </span>

                      
                </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </form>
       
    </div>
     <div class="with-other" style="margin: 35px 0px 25px 0px;">
     <form method="POST" id="nameserver_text" action="{{route('cart.domaintransfer')}}">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
		<input type = "hidden" name = "domaintype" value = "Transfer">
<input type = "hidden" name = "price" value = "637">
		
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6 col-sm-12">
				
				
                    <div class="form-group">
                                    <label for="inputTransferDomain">Domain Name</label>
                                    <input type="text" class="form-control" name="domain" id="inputTransferDomain" value="" placeholder="example.com" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="" data-original-title="Please enter your domain" required>
                                </div>
								
								
                                <div id="transferUnavailable" class="alert alert-warning slim-alert text-center hidden"></div>
                                                            </div>
															
                             <div class="text-right col-lg-offset-3 col-lg-6 col-sm-12">
                                <button type="submit" id="btnTransferDomain" class="btn btn-primary btn-transfer center-block">Add to Cart
                                </button>
                            </div>
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
    </form>


                           
                        
           
                           
                        </div>
          
    


    </div>

</div>
               
              </div>
            </div>
          </div>
        </div>
       
        </div>
        <!-- second row start-->
<?php
    if ($api->FormValidation())
    {
        $data = $api->DomainAvailability();
        $domain_price_value=0;
        ?>
        <div class="container">
<div class="row">
<form action="{{route('cart.domain')}}"  method="POST" id="domain_add_form">
<input type="hidden" name="flag"  value="0" />
                   <input type="hidden" name="domain" id="domain_name" value="" />
                   <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                   <input type="hidden" name="price" id="price_domain" value="" />
</form>
<div class="table-responsive">
            <table class="table table-striped m-b-none" >
          <thead >
            <tr style="border-top: 1px solid #ddd;">
              <th style="text-align: center;">Domain Name</th>
              <th style="text-align: center;">Status</th>
              <th style="text-align: center;">Price</th>
              <th style="text-align: center;">Action</th>
              
            </tr>
          </thead>
          <tbody>
               
                
               <?php
               foreach($api->tlds_list as $arrayitem)
               {
                   $fulldomainname = $api->domainname . "." . $arrayitem;
                   //echo $fulldomainname;

                   ?>
                  
                   <tr style="text-align: center;">
                       <td class="domainname" style="border-color:transparent; background-color:transparent;"><?php echo $fulldomainname; ?></td>
                       <td style="border-color:transparent;"><?php if ($data[$fulldomainname]["status"] == "available") { echo 'Available'; }
                       else { echo 'Not Available'; } ?>
                   </td>
                   <td style="border-color:transparent;">
                   <?php
                $i = 0;
                $colorid = 1;
                foreach ($resellersupporttld as $value) {

                    
                     $domainname = "test." .$value;
                    $domainname = ".$value";

                    
                   // echo $domain;
                    //echo $domainname;
                    if ($domainname != "") {
                        $selectedtld = $domainname;
                        $count = substr_count($selectedtld, '.');
                        if ($count == 1) {
                            $split = explode(".", $selectedtld, 2);
                            $selectedtld = $split[1];
                            //echo $selectedtld;
                            if (in_array($selectedtld, $domarray)) {
                                $selectedtld = "dom" . $selectedtld;
                            } elseif ($split[1] == "com") {
                                $selectedtld = "domcno";
                            } else {
                                $selectedtld = "dot" . $selectedtld;
                            }
                        }
                        if ($count == 2) {
                            $split = explode(".", $selectedtld, 3);
                            if ($split[2] == "com" || $split[2] == "net") {

                                if (in_array($split[1], $standardcomarray)) {
                                    $selectedtld = "centralnicstandard";
                                } elseif (in_array($split[1], $premiumcomarray)) {
                                    $selectedtld = "centralnicpremium";
                                } else if ($split[1] == "cn" && $split[2] == "com") {
                                    $selectedtld = "centralniccncom";
                                }
                            } else if ($split[2] == "de" && $split[1] == "com") {
                                $selectedtld = "centralniccomde";
                            } else if ($split[2] == "org" && $split[1] == "ae") {
                                $selectedtld = "centralnicstandard";
                            } else {
                                $selectedtld = "thirdleveldot" . $split[2];
                            }
                        }
                    } else {

                        $selectedtld = "domcno";
                        //default
                    }
                    $domainprice = $datajson[$selectedtld]["addnewdomain"][1];
                   // echo $domainprice;
                    if ($domainprice == "") {
                        $domainprice = "Not Available";
                    } else {
                        ?>
                        <input type="hidden" name="price" value="<?=$domainprice;?>" />
                            <div style="float: none">
                            <div class="hosting_row_div">      
                                <div class="first_hosting_col_div"  
                                <?php
                                 if ($colorid % 2 == 0) { 
                                     ?> id="even" <?php
                                     }
                                  else { ?> id="odd"
                                  <?php } ?>
                                   style="text-align: center;" >
                                 
                                </div>
                            </div> 
                        </div>
                        <?php
                            
                        if($arrayitem== $value){
                            if($data[$fulldomainname]["status"] == "available"){ 
                                $domain_price_value=$domainprice;
                                ?>
                                
                                <?=$domainprice;?>
                               
                           <?php }
                            else{
                                echo '-';
                            }
                            //echo 637.7;
                          
                        }  
                        // elseif($arrayitem=="in.net"){
                        //     print $value.$domainprice;
                        // }    
                        $colorid++;
                        $i++;
                       
                    }
                }

              
                ?>     </td>
                
                <td style="border-color:transparent;">
                <?php
                if($data[$fulldomainname]["status"] == "available")
                { ?>
                    <button type="button" class="btn btn-success addtodomain" data-domain="<?php echo $fulldomainname; ?>" data-price="<?=$domain_price_value?>" >Add to Cart</button>
               <?php }
                else{
                    echo '-';
                }
                ?></td>
               </tr>
               </form>
               <?php } ?>
               
           </tbody>
        </table>
     </div>
</div>
</div>
<div class="container">
    <div class="row">
    
            <table class="table table-striped m-b-none" >
          <thead>
            <tr style="border-top: 1px solid #ddd;  ">
              <th style="width: 350px;text-align:  center;">Domain Name</th>
              <th style="text-align:  center;">Status</th>
      
              
            </tr>
          </thead>
                <tbody>
          <?php
               $suggestions = $api->DomainSuggestion();
              //echo "<pre>";print_r($suggestions);exit;
                    foreach($suggestions as $domain => $details)
                    {

                       
                       // var_dump ($details);
                        if ($details["status"] == "available")
                        {
                            echo '<tr style="text-align:  center;">
                            <td class="domainname" style="border-color:transparent; background-color:transparent;">'.strtolower($domain).'</td>
                            <td style="border-color:transparent; background-color:transparent;">Available</td>
                            
                            </tr>';
                         
                		$flag = 1;
                        }
                         //echo $domain;
                         //echo $domainprice;
                       
                    }
                
				if(!isset($flag))
				{
					echo '<tr><td>No Suggestion Avaliable</td></tr>';
				}
                ?>
            </tbody>
        </table>
          </div>

</div>
<?php } ?>
</div>
</div><!-- /.container -->     
     
            
    </div>
  </div>
</div>

<script>
$('.addtodomain').click(function(e){
   $domain_name=$(this).attr('data-domain');
   $domain_price=$(this).attr('data-price');
   $('#domain_name').val($domain_name);
   $('#price_domain').val($domain_price);
   $('#domain_add_form').submit();
});
</script>

	</div>
    
@endsection