@extends('layouts.master')
@section('title')
About us - DeckSys, Brand of Makto Technology Private Limited
@endsection
@section('content')
<section class="CloudHostingSection">
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <ul class="CloudHostingTabsContainer">
                <li class="active" data-tab="CloudSME">
                    <p class="text-uppercase" style="font-size:20px;">About Us</p>
                </li>
            </ul>
        </div>
    </div>
</div>
</section>
<section>
<div class="container">
  <div class="row">
   
   
    <div class="col-md-8 col-sm-9 col-xs-12">
      <div class="heading padding_bottom_100 text-left padding_top_50">
        <h2 class=" no_padding text-uppercase color-black text-center">Who We Are</h2>
        <p class="padding_top_30 font-14 text-justify" style="font-family: Nunito-Regular !important;">Decksys is a fastest growing web hosting company, located in the Manchester city of India, Coimbatore. It is a service Brand of Makto Technology Private Limited which  provides solutions on software and IT Services. DeckSys provides solutions and services on Shared Linux hosting, shared windows hosting, Dedicated Windows / Linux servers, Cloud windows /linux servers, Enterprise and Business eMail solutions and Linux KVM hosting.</p>
        <h6 class="padding_top_20 text-uppercase color-black" style="text-decoration:none; font-weight:600; font-family: Nunito-Regular !important;">Our Offerings</h6>

<p class="padding_top_20 color-grey cus"> <ul class="padding_top_10 cus" style="list-style-type:decimal; padding-left: 15px; font-family: Nunito-Regular !important;">
          <li>Shared Linux / Windows Hosting</li>
          <li>Dedicated Servers</li>
          <li>Linux KVM</li>
          <li>Cloud Hosting</li>
          <li>Business eMail</li>
        </ul></p>
        
        
        <h6 class=" padding_top_20 text-uppercase color-black" style="text-decoration:none; font-weight:600; font-family: Nunito-Regular !important;">Support</h6>
        <p class="padding_top_20 color-grey cus">You can always reach us at +91-73977 67219/ <a href="mailto:support@decksys.com">support@decksys.com</a> / place a <a href="https://decksys.com/members/submitticket.php">Ticket</a> / chat with an expert.
        </p>
        
      </div>
    </div>
    <div class="col-md-4 col-sm-3 col-xs-12 margin_top_20"> <img src="images/about.jpg" alt="" title="" style="width: 350px;"/> </div>
  </div>
  </div>
  </section>
<section class="whatWeDo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="w_w_d_Content">
                        <p>
                            DeckSys is a service brand of Makto Technology Private Limited, providing solutions on hosting and IT infrastructure. Servers at DeckSys are backed by high security servers with 99.9% uptime.
                        </p>
                        <p>
                            The company provides range of products and services including Shared hosting, Dedicated servers, Business &amp; Enterprise eMaiI solutions, Linux KVM, Public / Private &amp; Hybrid Cloud servers.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="howToPay">
                        <p>Pay With : <img src="images/razorpay.png" alt="Razorpay" class="paymentMethod"></p>
                    </div>
                </div>
            </div>
        </div>
        </section>

@endsection