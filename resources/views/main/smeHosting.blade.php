@extends('layouts.master')
@section('title')
    Shared Hosting - DeckSys, Brand of Makto Technology Private Limited
@endsection
@section('content')
    <section class="smeHostingSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <ul class="smeHostingTabsContainer">
                        <li class="active" data-tab="linuxHosting">
                            <p>Linux Hosting</p>
                        </li>
                        <li class="" data-tab="windowsHosting">
                            <p>Windows Hosting</p>
                        </li>
                        <li class="" data-tab="linuxPlesk">
                            <p>Linux Plesk</p>
                        </li>
                        <li class="" data-tab="railsHosting">
                            <p>Rails Hosting</p>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="tabContentContainer">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10 col-lg-10">
                    <section class="tabContent active slideFromRight" id="linuxHosting">
                        <h3>Packages and Pricing</h3>

                        <form method="post" action="{{route('cart.add')}}"  >
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <input type = "hidden" name = "hosting" value = "linux">
                            <input type = "hidden" name = "hosting_id" id="hosting_id" value = "1">
                            <input type = "hidden" name = "package" value = "" id="package">
                            <input type = "hidden" name = "price" value = "" id="p_price">

<div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <td>Features</td>
                                    <td>Super Lite</td>
                                    <td>Business Lite</td>
                                    <td>Business Pro</td>
                                    <td>Business Elite</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Space</td>
                                    <td>100 GB Space</td>
                                    <td>Unlimited Space</td>
                                    <td>Unlimited Space</td>
                                    <td>Unlimited Space</td>
                                </tr>
                                <tr>
                                    <td>Bandwidth</td>
                                    <td>500 GB Transfer</td>
                                    <td>Unlimited Transfer</td>
                                    <td>Unlimited Transfer</td>
                                    <td>Unlimited Transfer</td>
                                </tr>
                                <tr>
                                    <td>Domains</td>
                                    <td>1 Website</td>
                                    <td>1 Website</td>
                                    <td>5 Websites</td>
                                    <td>Unlimited Websites</td>
                                </tr>

                                <tr>
                                    <td>Email Accounts</td>
                                    <td>25 Emails</td>
                                    <td>Unlimited Emails</td>
                                    <td>Unlimited Emails</td>
                                    <td>Unlimited Emails</td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 139/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 229/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 349/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 469/month</strong></td>
                                </tr>
                                @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "1")
                                        @if($key['name'] == "Super Lite")

                                            @foreach($key['pricing'] as $linux)
                                                <tr>
                                                    <td>Price</td>
                                                    <td><select class="form-control select_plan" name="package_price" attr-x="Super Lite" attr-y="<?=$key['pid'];?>"  >
 							<option value="">-- Select Plan --</option>
                                                           
                                                            <option value="1-monthly-{{$linux['monthly']/1}}">1 Month for ₹ {{$linux['monthly']/1}} - Per Month</option>
                                                            <option value="6-semiannually-{{$linux['semiannually']/6}}">6 Months for ₹ {{$linux['semiannually']/6}} - Per Month</option>
                                                            <option value="1-annually-{{$linux['annually']/12}}">1 Year for ₹ {{$linux['annually']/12}} - Per Month</option> 
                        				    <option value="2-biennially-{{$linux['biennially']/24}}">2 Years for ₹ {{$linux['biennially']/24}} - Per Month</option>
							    <option value="3-triennially-{{$linux['triennially']/36}}">3 Years for ₹ {{$linux['triennially']/36}} - Per Month</option>
                                                        </select></td>
                                                    @endforeach


                                                    @elseif($key['name'] == "Lite")
                                                        @foreach($key['pricing'] as $linux)
                                                            <td><select class="form-control select_plan" name="package_price" attr-x="Lite" attr-y="<?=$key['pid'];?>">
								<option value="">-- Select Plan --</option>                                                         

                                                                    <option value="1-monthly-{{$linux['monthly']}}">1 Month for ₹ {{$linux['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$linux['semiannually']/6}}">6 Months for ₹ {{$linux['semiannually']/6}} - Per Month</option>
 								    <option value="1-annually-{{$linux['annually']/12}}">1 Year for ₹ {{$linux['annually']/12}} - Per Month</option> 
                                                                    <option value="2-biennially-{{$linux['biennially']/24}}">2 Years for ₹ {{$linux['biennially']/24}} - Per Month</option>
								<option value="3-triennially-{{$linux['triennially']/36}}">3 Years for ₹ {{$linux['triennially']/36}} - Per Month</option>
                                                                </select></td>

                                                        @endforeach




                                                    @elseif($key['name'] == "Professional")
                                                        @foreach($key['pricing'] as $linux)
                                                            <td><select class="form-control select_plan" name="package_price" attr-x="Professional" attr-y="<?=$key['pid'];?>"   >
								<option value="">-- Select Plan --</option>
                                                                    <option value="1-monthly-{{$linux['monthly']}}">1 Month for ₹ {{$linux['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$linux['semiannually']/6}}">6 Months for ₹ {{$linux['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$linux['annually']/12}}">1 Year for ₹ {{$linux['annually']/12}} - Per Month</option>
								    <option value="2-biennially-{{$linux['biennially']/24}}">2 Years for ₹ {{$linux['biennially']/24}} - Per Month</option>
								    <option value="3-triennially-{{$linux['triennially']/36}}">3 Years for ₹ {{$linux['triennially']/36}} - Per Month</option>
                                                                </select>
							    </td>

                                                        @endforeach


                                                    @elseif($key['name'] == "Ultimate")
                                                        @foreach($key['pricing'] as $linux)
                                                            <td><select class="form-control select_plan" name="package_price" attr-x="Ultimate" attr-y="<?=$key['pid'];?>"   >
								    <option value="">-- Select Plan --</option>
                                                                    <option value="1-monthly-{{$linux['monthly']}}">1 Month for ₹ {{$linux['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$linux['semiannually']/6}}">6 Months for ₹ {{$linux['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$linux['annually']/12}}">1 Year for ₹ {{$linux['annually']/12}} - Per Month</option>
                    						    <option value="2-biennially-{{$linux['biennially']/24}}">2 Years for ₹ {{$linux['biennially']/24}} - Per Month</option>
								    <option value="3-triennially-{{$linux['triennially']/36}}">3 Years for ₹ {{$linux['triennially']/36}} - Per Month</option>
                                                                </select></td>

                                                        @endforeach
                                                    @endif
                                                    @endif
                                                    @endforeach

                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Super Lite" attr-y="Super_Lite"  >Buy Now</button></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Lite" attr-y="Lite">Buy Now</button></td>
                                                    <td><button type="submit"  class="btn btn-info" attr-x="Professional" attr-y="Professional">Buy Now</button></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Ultimate" att-y="Ultimate">Buy Now</button></td>
                                                </tr>
                                </tbody>
                            </table>
</div>
                        </form>
                    </section>

                    <!--Start Windows Hosting -->
                    <section class="tabContent slideFromRight" id="windowsHosting">
                        <h3>Packages and Pricing</h3>
                        <form method="post" action="{{route('cart.add')}}"  >
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <input type = "hidden" name = "hosting" value = "windows">
                            <input type = "hidden" name = "hosting_id" id="hosting_id1" value = "2">
                            <input type = "hidden" name = "package" value = "" id="package1">
                            <input type = "hidden" name = "price" value = "" id="p_price1">
                            <input type = "hidden" name = "month" value = "" id="p_month1">
<div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <td>Features</td>
                                    <td>Super Lite</td>
                                    <td>Business Lite</td>
                                    <td>Business Pro</td>
                                    <td>Business Elite</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Space</td>
                                    <td>100 GB Space</td>
                                    <td>Unlimited Space</td>
                                    <td>Unlimited Space</td>
                                    <td>Unlimited Space</td>
                                </tr>
                                <tr>
                                    <td>Bandwidth</td>
                                    <td>500 GB Transfer</td>
                                    <td>Unlimited Transfer</td>
                                    <td>Unlimited Transfer</td>
                                    <td>Unlimited Transfer</td>
                                </tr>
                                <tr>
                                    <td>Domains</td>
                                    <td>1 Website</td>
                                    <td>1 Website</td>
                                    <td>5 Websites</td>
                                    <td>Unlimited Websites</td>
                                </tr>

                                <tr>
                                    <td>Email Accounts</td>
                                    <td>25 Emails</td>
                                    <td>Unlimited Emails</td>
                                    <td>Unlimited Emails</td>
                                    <td>Unlimited Emails</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 139/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 229/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 349/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 469/month</strong></td>
                                </tr>
                                @foreach($products['products']['product'] as $key)

                                    @if($key['gid'] == "2")
                                        @if($key['name'] == "Super Lite")

                                            @foreach($key['pricing'] as $windows)
                                                <tr>
                                                    <td>Price</td>
                                                    <td><select class="form-control select_plan1" name="package_price" attr-x="Super Lite" attr-y="<?=$key['pid'];?>"   >
							    <option value="">-- Select Plan --</option>                                                            
                                                            <option value="1-monthly-{{$windows['monthly']}}" >1 Month for ₹ {{$windows['monthly']/1}} - Per Month</option>
                                                            <option value="6-semiannually-{{$windows['semiannually']/6}}">6 Months for ₹ {{$windows['semiannually']/6}} - Per Month</option>
                                                         
  						            <option value="1-annually-{{$windows['annually']/12}}">1 Year for ₹ {{$windows['annually']/12}} - Per Month</option>
                                                          
 						             <option value="2-biennially-{{$windows['biennially']/24}}">2 Years for ₹ {{$windows['biennially']/24}} - Per Month</option>
                                                             <option value="3-triennially-{{$windows['triennially']/36}}">3 Years for ₹ {{$windows['triennially']/36}} - Per Month</option>
                                                        </select></td>
                                                    @endforeach
                                                    @elseif($key['name'] == "Lite")
                                                        @foreach($key['pricing'] as $windows)

                                                            <td><select class="form-control select_plan1" name="package_price" attr-x="Lite" attr-y="<?=$key['pid'];?>" >
<option value="">-- Select Plan --</option>                                                                    
                                                                    <option value="1-monthly-{{$windows['monthly']}}" >1 Month for ₹ {{$windows['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$windows['semiannually']/6}}">6 Months for ₹ {{$windows['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$windows['annually']/12}}">1 Year for ₹ {{$windows['annually']/12}} - Per Month</option>

 <option value="2-biennially-{{$windows['biennially']/24}}">2 Years for ₹ {{$windows['biennially']/24}} - Per Month</option>
<option value="3-triennially-{{$windows['triennially']/36}}">3 Years for ₹ {{$windows['triennially']/36}} - Per Month</option>


                                                                </select></td>

                                                        @endforeach
                                                    @elseif($key['name'] == "Professional")
                                                        @foreach($key['pricing'] as $windows)

                                                            <td><select class="form-control select_plan1" name="package_price" attr-x="Professional" attr-y="<?=$key['pid'];?>" >
								    <option value="">-- Select Plan --</option>                                                                    
                                                                    <option value="1-monthly-{{$windows['monthly']}}" >1 Month for ₹ {{$windows['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$windows['semiannually']/6}}">6 Months for ₹ {{$windows['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$windows['annually']/12}}">1 Year for ₹ {{$windows['annually']/12}} - Per Month</option>
 								    <option value="2-biennially-{{$windows['biennially']/24}}">2 Years for ₹ {{$windows['biennially']/24}} - Per Month</option>
							 	    <option value="3-triennially-{{$windows['triennially']/36}}">3 Years for ₹ {{$windows['triennially']/36}} - Per Month</option>


                                                                </select></td>

                                                        @endforeach
                                                    @elseif($key['name'] == "Ultimate")
                                                        @foreach($key['pricing'] as $windows)
                                                            <td><select class="form-control select_plan1" name="package_price" attr-x="Ultimate" attr-y="<?=$key['pid'];?>" >
								    <option value="">-- Select Plan --</option>                                                              
                                                                    <option value="1-monthly-{{$windows['monthly']}}" >1 Month for ₹ {{$windows['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$windows['semiannually']/6}}">6 Months for ₹ {{$windows['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$windows['annually']/12}}">1 Year for ₹ {{$windows['annually']/12}} - Per Month</option>
 								    <option value="2-biennially-{{$windows['biennially']/24}}">2 Years for ₹ {{$windows['biennially']/24}} - Per Month</option>
								    <option value="3-triennially-{{$windows['triennially']/36}}">3 Years for ₹ {{$windows['triennially']/36}} - Per Month</option>


                                                                </select>

                                                            </td>

                                                        @endforeach
                                                </tr>
                                                @endif
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td><button type="submit" class="btn btn-info" attr-x="Super Lite" attr-y="Super_Lite">Buy Now</button></td>
                                                <td><button type="submit" class="btn btn-info" attr-x="Lite" attr-y="Lite">Buy Now</button></td>
                                                <td><button type="submit"  class="btn btn-info" attr-x="Professional" attr-y="Professional">Buy Now</button></td>
                                                <td><button type="submit" class="btn btn-info" attr-x="Ultimate" att-y="Ultimate">Buy Now</button></td>
                                            </tr>
                                </tbody>
                            </table>
</div>
                        </form>
                    </section>
                    <!--End Windows Hosting -->


                    <!--Start Linux Plesk Hosting -->

                    <section class="tabContent slideFromRight" id="linuxPlesk">
                        <h3>Packages and Pricing</h3>
                        <form method="post" action="{{route('cart.add')}}"  >
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <input type = "hidden" name = "hosting" value = "linuxplesk">
                            <input type = "hidden" name = "hosting_id" id="hosting_id2" value = "5">
                            <input type = "hidden" name = "package" value = "" id="package2">
                            <input type = "hidden" name = "price" value = "" id="p_price2">
<div class="table-responsive">
                            <table class="table table-condensed">
<thead>
                                <tr>
                                    <td>Features</td>
                                    <td>Super Lite</td>
                                    <td>Business Lite</td>
                                    <td>Business Pro</td>
                                    <td>Business Elite</td>
                                </tr>
                                </thead>
<tbody>
                                
                                                                <tr>
                                    <td>Space</td>
                                    <td>100 GB Space</td>
                                    <td>Unlimited Space</td>
                                    <td>Unlimited Space</td>
                                    <td>Unlimited Space</td>
                                </tr>
                                
                                <tr>
                                    <td>Bandwidth</td>
                                    <td>500 GB Transfer</td>
                                    <td>Unlimited Transfer</td>
                                    <td>Unlimited Transfer</td>
                                    <td>Unlimited Transfer</td>
                                </tr>
                                <tr>
                                    <td>Domains</td>
                                    <td>1 Website</td>
                                    <td>1 Website</td>
                                    <td>5 Websites</td>
                                    <td>Unlimited Websites</td>
                                </tr>

                                <tr>
                                    <td>Email Accounts</td>
                                    <td>25 Emails</td>
                                    <td>Unlimited Emails</td>
                                    <td>Unlimited Emails</td>
                                    <td>Unlimited Emails</td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 139/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 229/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 349/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 469/month</strong></td>
                                </tr>
                                </tr>
                                @foreach($products['products']['product'] as $key)

                                    @if($key['gid'] == "4")
                                        @if($key['name'] == "Super Lite")

                                            @foreach($key['pricing'] as $linuxplesk)
                                                <tr>
                                                    <td>Price</td>
                                                    <td><select class="form-control select_plan2" name="package_price" attr-x="Super Lite" attr-y="<?=$key['pid'];?>"  >
							    <option value="">-- Select Plan --</option>                                                            
                                                            <option value="1-monthly-{{$linuxplesk['monthly']}}">1 Month for ₹ {{$linuxplesk['monthly']/1}} - Per Month</option>
                                                            <option value="6-semiannually-{{$linuxplesk['semiannually']/6}}">6 Months for ₹ {{$linuxplesk['semiannually']/6}} - Per Month</option>
                                                            <option value="1-annually-{{$linuxplesk['annually']/12}}">1 Year for ₹ {{$linuxplesk['annually']/12}} - Per Month</option>                                                                                  
							    <option value="2-biennially-{{$linuxplesk['biennially']/24}}">2 Years for ₹ {{$linuxplesk['biennially']/24}} - Per Month</option>						
							    <option value="3-triennially-{{$linuxplesk['triennially']/36}}">3 Years for ₹ {{$linuxplesk['triennially']/36}} - Per Month</option>
                                                        </select></td>
                                                    @endforeach


                                                    @elseif($key['name'] == "Lite")
                                                        @foreach($key['pricing'] as $linuxplesk)
                                                            <td><select class="form-control select_plan2" name="package_price" attr-x="Lite" attr-y="<?=$key['pid'];?>" >
								    <option value="">-- Select Plan --</option>                                                                 
                                                                    <option value="1-monthly-{{$linuxplesk['monthly']}}">1 Month for ₹ {{$linuxplesk['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$linuxplesk['semiannually']/6}}">6 Months for ₹ {{$linuxplesk['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$linuxplesk['annually']/12}}">1 Year for ₹ {{$linuxplesk['annually']/12}} - Per Month</option>
							<option value="2-biennially-{{$linuxplesk['biennially']/24}}">2 Years for ₹ {{$linuxplesk['biennially']/24}} - Per Month</option>						
							<option value="3-triennially-{{$linuxplesk['triennially']/36}}">3 Years for ₹ {{$linuxplesk['triennially']/36}} - Per Month</option>

                                                                </select></td>

                                                        @endforeach




                                                    @elseif($key['name'] == "Professional")
                                                        @foreach($key['pricing'] as $linuxplesk)
                                                            <td><select class="form-control select_plan2" name="package_price" attr-x="Professional" attr-y="<?=$key['pid'];?>" >
								    <option value="">-- Select Plan --</option>                                                                    
                                                                    <option value="1-monthly-{{$linuxplesk['monthly']}}">1 Month for ₹ {{$linuxplesk['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$linuxplesk['semiannually']/6}}">6 Months for ₹ {{$linuxplesk['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$linuxplesk['annually']/12}}">1 Year for ₹ {{$linuxplesk['annually']/12}} - Per Month</option>
							      	    <option value="2-biennially-{{$linuxplesk['biennially']/24}}">2 Years for ₹ {{$linuxplesk['biennially']/24}} - Per Month</option>						
								    <option value="3-triennially-{{$linuxplesk['triennially']/36}}">3 Years for ₹ {{$linuxplesk['triennially']/36}} - Per Month</option>
                                                                </select></td>
                                                        @endforeach


                                                    @elseif($key['name'] == "Ultimate")
                                                        @foreach($key['pricing'] as $linuxplesk)
                                                            <td><select class="form-control select_plan2" name="package_price" attr-x="Ultimate" attr-y="<?=$key['pid'];?>" >
								    <option value="">-- Select Plan --</option>                                                                    
                                                                    <option value="1-monthly-{{$linuxplesk['monthly']}}">1 Month for ₹ {{$linuxplesk['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$linuxplesk['semiannually']/6}}">6 Months for ₹ {{$linuxplesk['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$linuxplesk['annually']/12}}">1 Year for ₹ {{$linuxplesk['annually']/12}} - Per Month</option>
								    <option value="2-biennially-{{$linuxplesk['biennially']/24}}">2 Years for ₹ {{$linuxplesk['biennially']/24}} - Per Month</option>						
							            <option value="3-triennially-{{$linuxplesk['triennially']/36}}">3 Years for ₹ {{$linuxplesk['triennially']/36}} - Per Month</option>
                                                                </select></td>

                                                        @endforeach
                                                    @endif
                                                    @endif
                                                    @endforeach

                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Super Lite" attr-y="Super_Lite"  >Buy Now</button></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Lite" attr-y="Lite">Buy Now</button></td>
                                                    <td><button type="submit"  class="btn btn-info" attr-x="Professional" attr-y="Professional">Buy Now</button></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Ultimate" att-y="Ultimate">Buy Now</button></td>
                                                </tr>
                                                </tbody>
                            </table>
</div>
                        </form>


                    </section>
                    <!--End Linux Plesk Hosting -->

                    <!--Start Rails Hosting -->

                    <section class="tabContent slideFromRight" id="railsHosting">
                        <h3>Packages and Pricing</h3>
                        <form method="post" action="{{route('cart.add')}}"  >
                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                            <input type = "hidden" name = "hosting" value = "rails">
                            <input type = "hidden" name = "hosting_id" id="hosting_id3" value = "4">
                            <input type = "hidden" name = "package" value = "" id="package3">
                            <input type = "hidden" name = "price" value = "" id="p_price3">
<div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <td>Features</td>
                                    <td>Super Lite</td>
                                    <td>Business Lite</td>
                                    <td>Business Pro</td>
                                    <td>Business Elite</td>
                                </tr>
                                </thead>
                                <tbody>
<tr>
                                    <td>Space</td>
                                    <td>100 GB Space</td>
                                    <td>Unlimited Space</td>
                                    <td>Unlimited Space</td>
                                    <td>Unlimited Space</td>
                                </tr>
                                <tr>
                                    <td>Bandwidth</td>
                                    <td>500 GB Transfer</td>
                                    <td>Unlimited Transfer</td>
                                    <td>Unlimited Transfer</td>
                                    <td>Unlimited Transfer</td>
                                </tr>
                               <tr>
                                    <td>Domains</td>
                                    <td>1 Website</td>
                                    <td>1 Website</td>
                                    <td>5 Websites</td>
                                    <td>Unlimited Websites</td>
                                </tr>

                                <tr>
                                    <td>Email Accounts</td>
                                    <td>25 Emails</td>
                                    <td>Unlimited Emails</td>
                                    <td>Unlimited Emails</td>
                                    <td>Unlimited Emails</td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 139/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 229/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 349/month</strong></td>
                                    <td><strong>Plan Starts <i class="fa fa-inr" aria-hidden="true"></i> 469/month</strong></td>
                                </tr>
                                @foreach($products['products']['product'] as $key)

                                    @if($key['gid'] == "5")
                                        @if($key['name'] == "Super Lite")

                                            @foreach($key['pricing'] as $rails)
                                                <tr>
                                                    <td>Price</td>
                                                    <td><select class="form-control select_plan3" name="package_price" attr-x="Super Lite" attr-y="<?=$key['pid'];?>" >
							   <option value="">-- Select Plan --</option>                                                        
                                                           <option value="1-monthly-{{$rails['monthly']}}"> 1 Month for ₹ {{$rails['monthly']/1}} - Per Month</option>
                                                           <option value="6-semiannually-{{$rails['semiannually']/6}}">6 Months for ₹  {{$rails['semiannually']/6}} - Per Month</option>
                                                           <option value="1-annually-{{$rails['annually']/12}}">1 Year for ₹ {{$rails['annually']/12}} - Per Month</option>
                                                        <option value="2-biennially-{{$rails['biennially']/24}}">2 Years for ₹ {{$rails['biennially']/24}} - Per Month</option>
 							<option value="3-triennially-{{$rails['triennially']/36}}">3 Years for ₹ {{$rails['triennially']/36}} - Per Month</option>
                                                        </select></td>
                                                    @endforeach


                                                    @elseif($key['name'] == "Lite")
                                                        @foreach($key['pricing'] as $rails)
                                                            <td><select class="form-control select_plan3" name="package_price" attr-x="Lite" attr-y="<?=$key['pid'];?>" >
								    <option value="">-- Select Plan --</option>                                                                    
                                                                    <option value="1-monthly-{{$rails['monthly']}}">1 Month for ₹ {{$rails['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$rails['semiannually']/6}}">6 Months for ₹ {{$rails['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$rails['annually']/12}}">1 Year for ₹ {{$rails['annually']/12}} - Per Month</option>
								    <option value="2-biennially-{{$rails['biennially']/24}}">2 Years for ₹ {{$rails['biennially']/24}} - Per Month</option>
 								    <option value="3-triennially-{{$rails['triennially']/36}}">3 Years for ₹ {{$rails['triennially']/36}} - Per Month</option>
                                                                </select></td>
                                                        @endforeach


                                                    @elseif($key['name'] == "Professional")
                                                        @foreach($key['pricing'] as $rails)
                                                            <td><select class="form-control select_plan3" name="package_price" attr-x="Professional" attr-y="<?=$key['pid'];?>" >
								    <option value="">-- Select Plan --</option>                                                               
                                                                    <option value="1-monthly-{{$rails['monthly']}}">1 Month for ₹ {{$rails['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$rails['semiannually']/6}}">6 Months for ₹ {{$rails['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$rails['annually']/12}}">1 Year for ₹ {{$rails['annually']/12}} - Per Month</option>
							            <option value="2-biennially-{{$rails['biennially']/24}}">2 Years for ₹ {{$rails['biennially']/24}} - Per Month</option>
 								    <option value="3-triennially-{{$rails['triennially']/36}}">3 Years for ₹ {{$rails['triennially']/36}} - Per Month</option>
                                                                </select></td>
                                                        @endforeach


                                                    @elseif($key['name'] == "Ultimate")
                                                        @foreach($key['pricing'] as $rails)
                                                            <td><select class="form-control select_plan3" name="package_price" attr-x="Ultimate" attr-y="<?=$key['pid'];?>" >
								    <option value="">-- Select Plan --</option>                                                                    
                                                                    <option value="1-monthly-{{$rails['monthly']}}">1 Month for ₹ {{$rails['monthly']/1}} - Per Month</option>
                                                                    <option value="6-semiannually-{{$rails['semiannually']/6}}">6 Months for ₹ {{$rails['semiannually']/6}} - Per Month</option>
                                                                    <option value="1-annually-{{$rails['annually']/12}}">1 Year for ₹ {{$rails['annually']/12}} - Per Month</option>
								    <option value="2-biennially-{{$rails['biennially']/24}}">2 Years for ₹ {{$rails['biennially']/24}} - Per Month</option>
 								    <option value="3-triennially-{{$rails['triennially']/36}}">3 Years for ₹ {{$rails['triennially']/36}} - Per Month</option>
                                                                </select></td>
                                                        @endforeach
                                                    @endif
                                                    @endif
                                                    @endforeach

                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Super Lite" attr-y="Super_Lite">Buy Now</button></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Lite" attr-y="Lite">Buy Now</button></td>
                                                    <td><button type="submit"  class="btn btn-info" attr-x="Professional" attr-y="Professional">Buy Now</button></td>
                                                    <td><button type="submit" class="btn btn-info" attr-x="Ultimate" att-y="Ultimate">Buy Now</button></td>
                                                </tr>
                                </tbody>
                            </table>
</div>
</form>
                    </section>
                    <!--End Rails Hosting -->





                </div>
            </div>
        </div>
    </section>

    <section class="tabHostingFeatureContainer" style="background-color:  #fff;text-align: center;">
<div classs="container">
<div class="col-lg-12">

<div class="col-lg-12">For 5 years and 10 years plan contact our customer care at (91) 40 4596 7079</div>
</div>
</div>
</section>


    <section class="tabHostingFeatureContainer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <section class="tabFeatureContent active slideFromRight" id="linuxHosting">
                        <h3>All our Linux Hosting has these Feature</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 paddOnXS">
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="letsEncrypt"></div>
                                    <div class="a_fContentContainer">
                                        <h4>SSL Layer</h4>
                                        <p>
                                            Secure the Website using Let's Encrypt available to all the hosting account at no cost.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="webHostManager"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Web Host Manager</h4>
                                        <p>
                                            Get a complete access over your website using cPanel backed by the powerful WHM
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="softaculous"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Softaculous</h4>
                                        <p>
                                            Deploy application from a list of 300+ availble one click installer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="cloudLinux"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Light Weight Env</h4>
                                        <p>
                                            Managed IOPS, CPU, Memory, IO, number of Processes and Limits.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dividerCustom show_on_lg"></div>
                        <div class="asideF_L_Container">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                <ul class="featuresList fa-ul" style="margin-top:-10px;">
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                        Support, PHP, Perl, Rails, MySql, Python
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                        Domains, Subdomains and Advanced DNS management
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                        AWStats to track website visitor logs and information
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
                                        eMail Services on RoundCube / SquirrelMail / Horde
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>                                        
                                            File Manager services or access files using FTP / FTPS over secured infrastructure.
                                        
                                    </li>
                                    <li>
                                        <span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
					30 days money back guarantee
                                    </li>
                                    <li>
                                        <span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
			Support Over Phone , eMail / chat round the year
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="chatFeature">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
<button class="btn btn-primary center-block" style="background-color:#5348ae;border:0px;padding: 11px 30px 13px 30px;border-radius: 0px;"  onclick="$crisp.push(['do', 'chat:open'])">Chat With Experts</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="tabFeatureContent slideFromRight" id="windowsHosting">
                        <h3>All our Windows Hosting has these Feature</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="letsEncrypt"></div>
                                    <div class="a_fContentContainer">
                                        <h4>SSL Layer</h4>
                                        <p>
                                            Secure the Website using Let's Encrypt available to all the hosting account at no cost.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="hostInterface"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Host Interface</h4>
                                        <p>
                                            Enhanced version of Plesk with more features support Docker and WordPress Toolkit.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="codeMgt"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Source Code MGMT</h4>
                                        <p>
                                            Deploy locally or remotely with support to Github, Travis, Bitbuckef and more.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="dockerSupport"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Docker Support</h4>
                                        <p>
                                            Catalogue of images, upload custom image, deploy and manage Docker container.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dividerCustom show_on_lg"></div>
                        <div class="asideF_L_Container">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                <ul class="featuresList fa-ul" style="margin-top:-10px;">
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
					Supports ASPNET, PAP, MS SQL MySQL Nodejs Supports ASPNET, PAP, MS SQL MySQL Nodejs 
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Domains, Subdomains and Advanced DNS management
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Wordpress Toolkit
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        eMail Services on SmarterMail
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        
                                            File Manager services or access files using FTP / FTPS over secured infrastructure.
                                        
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        30 days money back guarantee
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Support Over Phone , eMail / chat round the year
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="chatFeature">
                            <div class="container-fluid">
                                <div class="row">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

<button class="btn btn-primary center-block" style="background-color:#5348ae;border:0px;padding: 11px 30px 13px 30px;border-radius: 0px;"  onclick="$crisp.push(['do', 'chat:open'])">Chat With Experts</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="tabFeatureContent slideFromRight" id="linuxPlesk">
                        <h3>All our Linux Plesk has these Feature</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="letsEncrypt"></div>
                                    <div class="a_fContentContainer">
                                        <h4>SSL Layer</h4>
                                        <p>
                                            Secure the Website using Let's Encrypt available to all the hosting account at no cost.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="hostInterface"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Host Interface</h4>
                                        <p>
                                            Enhanced version of Plesk with more features support Docker and WordPress Toolkit.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="codeMgt"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Source Code MGMT</h4>
                                        <p>
                                            Deploy locally or remotely with support to Github, Travis, Bitbuckef and more.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="dockerSupport"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Docker Support</h4>
                                        <p>
                                            Catalogue of images, upload custom image, deploy and manage Docker container.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dividerCustom show_on_lg"></div>
                        <div class="asideF_L_Container">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                <ul class="featuresList fa-ul" style="margin-top:-10px">
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Supports Ruby on Rails, PHP, MySQL, Nodejs
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Domain, subdomains and Advanced DNS management.
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Wordpress Toolkit
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        eMail services on Horde / Roundcube</li>
                                    <li>
                                        <span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                            File Manager services or access files using FTP / FTPS over secured infrastructure.
                                        
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        30 days money back guarantee
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Support Over Phone , eMail / chat round the year
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="chatFeature">
                            <div class="container-fluid">
                                <div class="row">
                                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

<button class="btn btn-primary center-block" style="background-color:#5348ae;border:0px;padding: 11px 30px 13px 30px;border-radius: 0px;"  onclick="$crisp.push(['do', 'chat:open'])">Chat With Experts</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="tabFeatureContent slideFromRight" id="railsHosting">
                        <h3>All our Rails Hosting has these Feature</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="letsEncrypt"></div>
                                    <div class="a_fContentContainer">
                                        <h4>SSL Layer</h4>
                                        <p>
                                            Secure the Website using Let's Encrypt available to all the hosting account at no cost.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="hostInterface"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Host Interface</h4>
                                        <p>
                                            Enhanced version of Plesk with more features support Docker and WordPress Toolkit.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="codeMgt"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Source Code MGMT</h4>
                                        <p>
                                            Deploy locally or remotely with support to Github, Travis, Bitbuckef and more.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <div class="availableFeatures">
                                    <div class="a_fImageContainer" id="dockerSupport"></div>
                                    <div class="a_fContentContainer">
                                        <h4>Docker Support</h4>
                                        <p>
                                            Catalogue of images, upload custom image, deploy and manage Docker container.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dividerCustom show_on_lg"></div>
                        <div class="asideF_L_Container">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                                <ul class="featuresList fa-ul" style="margin-top:-10px">
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Supports Ruby on Rails, PHP, MySQL, Nodejs 
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Domain, subdomains and Advanced DNS management.
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Wordpress Toolkit
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        eMail services on Horde / Roundcube
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        
                                            File Manager services or access files using FTP / FTPS over secured infrastructure.
                                        
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        30 days money back guarantee
                                    </li>
                                    <li><span class="fa-li" ><i class="fa fa-chevron-right" aria-hidden="true"></i></span>

                                        Support Over Phone , eMail / chat round the year
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="chatFeature">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

<button class="btn btn-primary center-block" style="background-color:#5348ae;border:0px;padding: 11px 30px 13px 30px;border-radius: 0px;"  onclick="$crisp.push(['do', 'chat:open'])">Chat With Experts</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <section class="whatWeDo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="w_w_d_Content">
                        <p>
                            DeckSys is a service brand of Makto Technology Private Limited, providing solutions on hosting and IT infrastructure. Servers at DeckSys are backed by high security servers with 99.9% uptime.
                        </p>
                        <p>
                            The company provides range of products and services including Shared hosting, Dedicated servers, Business & Enterprise eMaiI solutions, Linux KVM, Public / Private & Hybrid Cloud servers.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="howToPay">
                        <p>Pay With : <img src="images/hosting-features/razorpay.png" alt="Razorpay" class="paymentMethod"></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>


        $(document).on('change', '.select_plan', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id').val(pid);
            $('#package').val(package_name);
            $('#p_price').val(price);
        });

        $(document).on('change', '.select_plan1', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id1').val(pid);
            $('#package1').val(package_name);
            $('#p_price1').val(price);
        });
        $(document).on('change', '.select_plan2', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id2').val(pid);
            $('#package2').val(package_name);
            $('#p_price2').val(price);
        });

        $(document).on('change', '.select_plan3', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id3').val(pid);
            $('#package3').val(package_name);
            $('#p_price3').val(price);
        });


$(document).on('change', '.select_plan4', function(){
            var package_name=$(this).attr('attr-x');
            var pid=$(this).attr('attr-y');
            var price=$(this).val();
            $('#hosting_id4').val(pid);
            $('#package4').val(package_name);
            $('#p_price4').val(price);
        });

    </script>


@endsection

