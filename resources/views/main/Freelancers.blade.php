@extends('layouts.master')
@section('title')
    Agencies & Freelancers - DeckSys, Brand of Makto Technology Private Limited
@endsection
@section('content')

    <section class="CloudHostingSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <ul class="CloudHostingTabsContainer">
                        <li data-tab="CloudSME" style="font-size:20px;">
                            <p>Our Products</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<div class="container">
        <div class="row" style="margin-top:20px; " >
        <div class="col-sm-4 col-md-4">
            <div class="thumbnail" style="padding: 0px;">
              
              <div class="caption" style="padding: 0px;">
    <div style="
    background-color: #23b7e5;
    padding: 15px;
">
                <img src="images/products_decksys/sharedhosting_prod.png" alt="..." style="float:right;width: 60px;" class="image-responsive"><h3 style="
    color: #FFF;
">Shared Hosting</h3></div>
                <p style="
    padding: 10px;
">Customizable single-tenant dedicated servers provide the highest level of security and performance for your site or app</p>
                <p style="
    padding: 10px;
">Starting at ₹ 109/Month<a href="/SME_Hosting" class="btn btn-primary btn-sm" style="float:right;" role="button">Buy Now</a></p>
              </div>
            </div>
          </div>

        
        <div class="col-sm-4 col-md-4">
            <div class="thumbnail" style="padding: 0px;">
              
              <div class="caption" style="padding: 0px;">
    <div style="background-color: #7266ba;padding: 15px;">
                <img src="images/products_decksys/dedicatedservers_product.png" alt="..." style="float:right;width: 60px;" class="image-responsive"><h3 style="
    color: #FFF;
">Dedicated Hosting  </h3></div>
                <p style="
    padding: 10px;
">Customizable single-tenant dedicated servers provide the highest level of security and performance for your site or app</p>
                <p style="
    padding: 10px;
">Starting at ₹ 8000/Month<a href="/dedicated_server" class="btn btn-primary btn-sm" style="float:right;" role="button">Buy Now</a></p>
              </div>
            </div>
          </div>
        
          <div class="col-sm-4 col-md-4">
            <div class="thumbnail" style="padding: 0px;">
              
              <div class="caption" style="padding: 0px;">
    <div style="background-color: #23b7e5;padding: 15px;">
                <img src="images/products_decksys/cloudhosting_product.png" alt="..." style="float:right;width: 60px;" class="image-responsive"><h3 style="
    color: #FFF;
">Cloud Hosting  </h3></div>
                <p style="
    padding: 10px;
">Customizable single-tenant dedicated servers provide the highest level of security and performance for your site or app</p>
                <p style="
    padding: 10px;
">Starting at ₹ 390/Month<a href="/Cloud_Hosting" class="btn btn-primary btn-sm" style="float:right;" role="button">Buy Now</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="row"   >

           <div class="col-sm-4 col-md-4">
            <div class="thumbnail" style="padding: 0px;">
              
              <div class="caption" style="padding: 0px;">
    <div style="
    background-color: #7266ba;
    padding: 15px;
">
                <img src="images/products_decksys/wordpress.png" alt="..." style="float:right;width: 60px;" class="image-responsive"><h3 style="
    color: #FFF;
">Wordpress Hosting  </h3></div>
                <p style="
    padding: 10px;
">Customizable single-tenant dedicated servers provide the highest level of security and performance for your site or app</p>
                <p style="
    padding: 10px;
">Starting at ₹ 199/Month<a href="/Managed_wordpress" class="btn btn-primary btn-sm" style="float:right;" role="button">Buy Now</a></p>
              </div>
            </div>
          </div>


             <div class="col-sm-4 col-md-4">
            <div class="thumbnail" style="padding: 0px;">
              
              <div class="caption" style="padding: 0px;">
    <div style="background-color: #23b7e5;padding: 15px;">
                <img src="images/products_decksys/ecommercehosting_product.png" alt="..." style="float:right;width: 60px;" class="image-responsive"><h3 style="
    color: #FFF;
">Ecommerce Hosting  </h3></div>
                <p style="
    padding: 10px;
">Customizable single-tenant dedicated servers provide the highest level of security and performance for your site or app</p>
                <p style="
    padding: 10px;
">Starting at ₹ 199/Month<a href="/Ecommerce_Hosting" class="btn btn-primary btn-sm" style="float:right;" role="button">Buy Now</a></p>
              </div>
            </div>
          </div>

<div class="col-sm-4 col-md-4">
            <div class="thumbnail" style="padding: 0px;">
              
              <div class="caption" style="padding: 0px;">
    <div style="
    background-color: #7266ba;
    padding: 15px;
">
                <img src="images/products_decksys/enterprisehosting_product.png" alt="..." style="float:right;width: 60px;" class="image-responsive"><h3 style="
    color: #FFF;
">Enterprise Hosting</h3></div>
                <p style="
    padding: 10px;
">Customizable single-tenant dedicated servers provide the highest level of security and performance for your site or app</p>
                 <p style="
    padding: 10px;
">Starting at ₹ 199/Month<a href="" class="btn btn-primary btn-sm" style="float:right;" role="button">Buy Now</a></p>
              </div>
            </div>
          </div>
         
        
         
        </div>
        </div>
<section class="whatWeDo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="w_w_d_Content">
                        <p>
                            DeckSys is a service brand of Makto Technology Private Limited, providing solutions on hosting and IT infrastructure. Servers at DeckSys are backed by high security servers with 99.9% uptime.
                        </p>
                        <p>
                            The company provides range of products and services including Shared hosting, Dedicated servers, Business &amp; Enterprise eMaiI solutions, Linux KVM, Public / Private &amp; Hybrid Cloud servers.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="howToPay">
                        <p>Pay With : <img src="images/razorpay.png" alt="Razorpay" class="paymentMethod"></p>
                    </div>
                </div>
            </div>
        </div>
        </section>
@endsection