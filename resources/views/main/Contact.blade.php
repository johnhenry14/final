@extends('layouts.master')
@section('title')
DeckSys | Shared Linux and Windows Hosting, Dedicated Servers, VPS & Cloud Hosting, Coimbatore India
@endsection
@section('content')

<div class="container page-header">
     <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div id="map1" class="map">
        </div>
</div>
        <div class="col-md-6 col-sm-6 col-xs-12 contactAddress">
        
                <div class="panel panel-success">
                    <div class="text-center mapHeader">Reach Us</div>
                    <div class="panel-body text-center">
                      
                        <p class="text-justify">

Makto Technology Private Limited,
                        Block B2 1st Floor,<br>Rathinam Technical Campus,<br>Pollachi Main Road,Eachanari,Coimbatore 641 021<br><br>
<b>Sales</b><br>
                        Email -  <a href="mailto:sales@decksys.com">sales@decksys.com</a> <br>
Phone Number - (91) 40 4596 7079 (Press 1 for Sales)<br><br>
<b>Support</b><br> 
                       Email -  <a href="mailto:support@decksys.com">support@decksys.com</a><br>

                        Phone Number - (91) 40 4596 7079 (Press 2 for Support)

</p>                        
                        
                  
                    </div>
                </div>
            </div>
        </div>
</div>


@endsection