@extends('layouts.master')
@section('title')
    DeckSys | Shared Linux and Windows Hosting, Dedicated Servers, VPS & Cloud Hosting, Coimbatore India
@endsection
@section('content')
    <section class="smeHostingSection">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <ul class="smeHostingTabsContainer">
                        <li>
                            <p>Your Order</p>
                        </li>
                      
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="tabContentContainer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-12" style="margin-top:40px;">
                    <section class="tabContent active" id="linuxHosting">
                     <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <td style="border-top-color: #5972c2 !important;
	border-left-color: #5972c2 !important;">Product</td>
                                <td>Price</td>
                                <td>Tax</td>
                                <td>Subtotal</td>
                                <td>Actions</td>
                            </tr>
                            </thead>
                            <tbody>
						
						
						<?php //echo "<pre>";print_r($cartdata);exit;?>
                            @foreach($cartdata as $key => $value)
                                <form action="{{route('cart.remove')}}" method="POST">
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="rowid" value="{{$value->rowId}}" />
                                    <tr>

                                        <td>{{$value->name}} - {{$value->options->package}} {{$value->options->os}}
                                        <!--  - @if(isset($value->options->bicycle))
                                            {{$value->options->bicycle}}
                                                    @endif-->
                                        </td>
<!--<td>@if($key=='msetupfee')

{{$value}}
@endif
</td>-->


<td>
@foreach($value->options->addons as $key=>$addon)
@for($key=0;$key<100;$key++)
	
@endfor
@if($addon->price !="0.00")
<p>{{$addon->name}} - <i class="fa fa-inr"></i> {{$addon->price}}</p>
@else
	
@endif
@endforeach
<strong> Total - <i class="fa fa-inr"></i> {{$var1=$value->price}} </strong>

</td>

                                        
                  			<td>GST @ 18.00% - <i class="fa fa-inr"></i> {{round($var2=$value->tax,2)}}<br></td>
                                        <td><i class="fa fa-inr"></i> {{round($var1+$var2,2)}}</td>

                                        <td><!-- <button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button> -->
                                            <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></td>

                                    </tr>
                                </form>
                            @endforeach
                            </tbody>
                            <tfoot>

                            <tr>
                                <td><a href="/Product" class="btn" style="background-color: #7266ba; color:#FFF"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                                <td style="border-right: 1px solid transparent !important; "></td>
                                <td style="border-right: 1px solid transparent !important; "></td>
                                <td style="padding-top: 15px;">Total <i class="fa fa-inr"></i> {{round(Cart::total())}}</td>

                                <td><a href="/payWithRazorpay" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
                            </tr>
                            </tfoot>


                        </table>
</div>
                    </section>






                </div>
            </div>
        </div>
    </section>
@endsection