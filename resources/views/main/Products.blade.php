@extends('layouts.master')
@section('title')
DeckSys | Shared Linux and Windows Hosting, Dedicated Servers, VPS & Cloud Hosting, Coimbatore India
@endsection
@section('content')
<div class="container">
<div class="row" style="padding-top:20px;">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="images/product_shared.jpg" alt="...">
      <div class="caption">
        <h3>Shared Hosting <a href="/SME_Hosting" class="btn btn-primary btn-md" style="margin-left: 75px;" role="button">Buy Now</a></h3>
          
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="images/product_dedicated.jpg" alt="...">
      <div class="caption">
        <h3>Dedicated Server <a href="/dedicated_server" class="btn btn-primary btn-md" style="margin-left: 55px;" role="button">Buy Now</a></h3>
    
       </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="images/product_cloud.jpg" alt="...">
      <div class="caption">
        <h3>Cloud Hosting <a href="/Cloud_Hosting" class="btn btn-primary btn-md" style="margin-left: 90px;" role="button">Buy Now</a></h3>
      
        </div>
    </div>
  </div>
</div>
<div class="row">


  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="images/product_wordpress.jpg" alt="...">
      <div class="caption">
        <h3>Wordpress Hosting <a href="/Managed_wordpress" class="btn btn-primary btn-md" style="margin-left: 37px;" role="button">Buy Now</a></h3>

      </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="images/product_ecommerce.jpg" alt="...">
      <div class="caption">
        <h3>Ecommerce Hosting <a href="/Ecommerce_Hosting" class="btn btn-primary btn-md" style="margin-left: 25px;" role="button">Buy Now</a></h3>
       
     </div>
    </div>
  </div>

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="images/product_enterprise.jpg" alt="...">
      <div class="caption">
        <h3>Enterprise Hosting <a href="" class="btn btn-primary btn-md" style="margin-left: 45px;" role="button">Buy Now</a></h3>
   
    </div>
    </div>
  </div>
</div>
</div>
@endsection