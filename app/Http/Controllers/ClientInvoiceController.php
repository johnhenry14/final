<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;
use App\Repositories\Invoice;
use App\Repositories\Clients;
use Illuminate\Support\Facades\Input;
use Cart;

class ClientInvoiceController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pay()
    {
        $order_id=$_GET['invoice_id'];
        $clientid = session()->get('login_id');

        $data = json_encode(Cart::content());
        $cartdata = json_decode($data);

        $payment = Whmcs::GetInvoice([
            'clientid'=>$clientid,
            'invoiceid' => $order_id,
        ]);

//dd($payment);


        $order_id = Input::get('order_id');
        $clientdetails = Whmcs::GetClientsDetails([
            'clientid' => $clientid,
        ]);
        $result2 = Whmcs::GetOrders([]);


        
        return view('clientlayout.main.clientinvoice', compact('payment','clientdetails','result2','order_id'));
    }

}
