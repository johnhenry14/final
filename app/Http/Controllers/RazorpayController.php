<?php
namespace App\Http\Controllers;
use App\Repositories\Clients;
use App\Repositories\AcceptOrder;
use App\Repositories\AddInvoicePayment;
use App\Repositories\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Razorpay\Api\Api;
use Cart;
use Session;
use Redirect;
use PDF;
use Crypt;

class RazorpayController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function payWithRazorpay()

    {

        $value = session()->get('login_id');
        if ($value != '') {
            $results = Whmcs::GetClientsDetails([
                'clientid' => $value,
            ]);
            $data = json_encode(Cart::content());
            $cartdata = json_decode($data);
			// echo "<pre>";print_r($cartdata);exit;
            $i = 0;
            $j = 0;
            $domain = array();
            $pid = array();
            $billingcycle = array();
            $package = array();
            $name = array();
            $domain_reg = array();
            $domain_regperiod = array();
			$customfield=array();
			$addons=array();
			$k=0;$data_server=array();
			//echo "<pre>";print_r($cartdata);exit;
            foreach ($cartdata as $cartdomain) {
                if ($cartdomain->options->type == 'domain') {
                    $domain[$i] = $cartdomain->name;
                    $domain_reg[$i] = 'register';
                    $domain_regperiod[$i] = '1';
                    $i++;

                }
				elseif ($cartdomain->options->package == 'Domain Transfer') {
                    $domain[$i] = $cartdomain->name;
                    $domain_reg[$i] = 'transfer';
                    $domain_regperiod[$i] = '1';
                    $i++;
                }
				elseif ($cartdomain->options->type == 'hosting') {
                    $billingcycle[$j] = $cartdomain->options->bicycle;
                    $name[$j] = $cartdomain->name;
                    $pid[$j] = $cartdomain->options->gid;
                    $package[$j] = $cartdomain->options->package;
	                $j++;
                }
				
				
				elseif ($cartdomain->options->type == 'SSL') {
                    $billingcycle[$j] = $cartdomain->options->bicycle;
                    $name[$j] = $cartdomain->name;
                    $pid[$j] = $cartdomain->options->gid;
                    $package[$j] = $cartdomain->options->package;
	                $j++;
                }

				elseif ($cartdomain->options->type == 'server') {
                    $billingcycle[$j] = $cartdomain->options->bicycle;
                    $name[$j] = $cartdomain->name;
                    $pid[$j] = $cartdomain->options->gid;
                    $package[$j] = $cartdomain->options->package;
					foreach($cartdomain->options->addons_id as $dedicated)
					  {
						  $dedicated_server=explode('_',$dedicated);
						
						  $data_server[$dedicated_server[0]]=$dedicated_server[1];
						  //echo "<pre>";print_r($data_server[$dedicated_server[0]]);exit;
						  $k++;
					  }
	                $addons[$j]=base64_encode(serialize($data_server)); 
					//echo "<pre>";print_r($addons[$j]);exit;
                    $j++;
                }
				elseif ($cartdomain->options->type == 'Ecommerce') {
                    $billingcycle[$j] = $cartdomain->options->bicycle;
                    $name[$j] = $cartdomain->name;
                    $pid[$j] = $cartdomain->options->gid;
                    $package[$j] = $cartdomain->options->package;
					foreach($cartdomain->options->addons_id as $ecommerce)
					  {
						  $ecommerceHosting=explode('_',$ecommerce);
						  $data_server[$ecommerceHosting[0]]=$ecommerceHosting[1];
						  $k++;
					  }
	                $addons[$j]=base64_encode(serialize($data_server)); 
                    $j++;
                }
            }
			
			//print_r($addons);exit;

                if (count($domain) != 0) {
                $domain = $domain;
                $domain_reg = $domain_reg;
                $domain_regperiod = $domain_regperiod;
            } else {
                $domain = array();
                $domain_reg = array();
                $domain_regperiod = array();
            }
             
//print_r($addons);exit;
	/* Add Order to WHMCS */

        $order_details = Whmcs::AddOrder([
                'clientid' => $value,
                'paymentmethod' => 'razorpay',
                'pid' => $pid,
                'domain' => $domain,
                'billingcycle' => $billingcycle,
                'domaintype' => $domain_reg,
                'regperiod' => $domain_regperiod,
				//'configoptions'=>$addons
                'configoptions'=>$addons
            ]);
 
            $order_id = $order_details['orderid'];
            $invoiceid = $order_details['invoiceid'];
            return view('main.payWithRazorpay', compact('results', 'cartdata', 'order_id', 'invoiceid', 'order_details'));
        } else {
            session()->put('checkout', 'true');
            return redirect('signin');
        }
    }

    public function payment_success(Request $request)
    {

        $data = json_encode(Cart::content());
        $cartdata = json_decode($data);
        $value = session()->get('login_id');
        $payment_id = Input::get('payment_id');
        $order_id = Input::get('order_id');
        $invoiceid = Input::get('invoiceid');

	/* AddInvoice Payment */

        $dataobject=new AddInvoicePayment();
        $results=$dataobject->AddInvoicePayment();

	/* Accept Order */

        $dataobject=new AcceptOrder();
        $results1=$dataobject->AcceptOrder();
	
	/* Getclients */

        $dataobject=new Clients();
        $clientdetails=$dataobject->GetClients();

		$result2 = Whmcs::GetOrders([]);

       //echo "<pre>";print_r($cartdata);exit;
      Cart::destroy();
	  return redirect('payment_invoice/'.Crypt::encrypt($order_id).'/'.Crypt::encrypt($invoiceid));
       
    }
	
	public function invoice_details($id,$iid)
	{
	$order_id=Crypt::decrypt($id);
	$invoice_id=Crypt::decrypt($iid);
	$data = json_encode(Cart::content());
    $cartdata = json_decode($data);

	/* Get Invoice */

        $dataobject=new Invoice();
        $paymentinvoice=$dataobject->GetInvoice($invoice_id);
		//echo "<pre>";print_r($paymentinvoice);exit;

	/* Get Clients*/

        $dataobject=new Clients();
        $clientdetails=$dataobject->GetClients();

 	$result2 = Whmcs::GetOrders([]);
	
	return view('main.payment_success', compact('paymentinvoice', 'clientdetails','order_id','cartdata','result2'));
	
	}
}