<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DarthSoup\Whmcs\Facades\Whmcs;
use Illuminate\Support\Facades\View;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;
use App\Repositories\ARecord;
use App\Repositories\AAAARecord;
use Illuminate\Support\Facades\Input;
use App\Repositories\MXRecord;
use App\Repositories\CNAMERecord;
use App\Repositories\NSRecord;
use App\Repositories\TXTRecord;
  

class DNSController extends Controller
{
	public function DNS(Request $request){
	$clientid = session()->get('login_id');
        $domain=Whmcs::GetClientsDomains(['clientid'=>$clientid]);
        $results=Whmcs::GetClientsDetails(['clientid'=>$clientid]);
        
       
/* A Records */
        $domainname = $request->input('domainname'); 
        $value = $request->input('value');
	$ttl = $request->input('ttl');
        $domainArray = array($domainname, $value,$ttl);
        $dataObject1 = new ARecord();
        $result1 = $dataObject1->DNSRecord($domainArray);

	$Object = new ARecord();
        $response = $Object->getArecord();

 	
/* End A Records */



/* MX Records */
 	$domainname = $request->input('domainname'); 
        $value = $request->input('value');
	$ttl = $request->input('ttl');
	$priority = $request->input('priority');
        $domainArray1 = array($domainname, $value,$ttl,$priority);
	$dataObject2 = new MXRecord();
        $result2 = $dataObject2->AddMXRecord($domainArray1);

        $Object = new MXRecord();
        $response2 = $Object->getMXRecord();
/* End MX Records */

/* CNAME Records */
 	$domainname = $request->input('domainname'); 
        $value = $request->input('value');
	$ttl = $request->input('ttl');
        $domainArray2 = array($domainname, $value,$ttl);
	$dataObject3= new CNAMERecord();
        $result3 = $dataObject3->AddCNAMERecord($domainArray2);

	$Object = new CNAMERecord();
        $response3 = $Object->getCNAMErecord();

	$currentvalue = $request->input('currentvalue'); 
        $newvalue = $request->input('newvalue');
        $domainArray_modify = array($currentvalue, $newvalue);
        $modifyObject2 = new CNAMERecord();
        $modify_CNAMErecord = $modifyObject2->modifyCNAMERecord($domainArray_modify);

/* End CNAME Records */

/* NS Records */
 	$domainname = $request->input('domainname'); 
        $value = $request->input('value');
	$ttl = $request->input('ttl');
        $domainArray3 = array($domainname, $value,$ttl);
	$dataObject4= new NSRecord();
        $result4 = $dataObject4->AddNSRecord($domainArray3);

	$Object = new NSRecord();
        $response4 = $Object->getNSRecord();

	

/* End NS Records */


/* TXT Records */
        $domainname = $request->input('domainname'); 
        $value = $request->input('value');
	$ttl = $request->input('ttl');
        $domainArray4 = array($domainname, $value,$ttl);
        $dataObject6 = new TXTRecord();
        $result6 = $dataObject6->AddTXTRecord($domainArray4);

	


/* End TXT Records */


/* AAAA Records */
        $domainname = $request->input('domainname'); 
        $value = $request->input('value');
		$ttl = $request->input('ttl');
        $domainArray5 = array($domainname, $value,$ttl);
        $dataObject5 = new AAAARecord();
        $result5 = $dataObject5->AddAAAARecord($domainArray5);

		$Object = new AAAARecord();
        $response1 = $Object->getAAAArecord();
		$currentvalue = $request->input('currentvalue'); 
        $newvalue = $request->input('newvalue');
        $domainArray_modify = array($currentvalue, $newvalue);
        $modifyObject1 = new AAAARecord();
        $modify_AAAArecord = $modifyObject1->modifyAAAArecord($domainArray_modify);

/* End AAAA Records */ 

/* TXT Records */
        $domainname = $request->input('domainname'); 
        $value = $request->input('value');
		$ttl = $request->input('ttl');
        $domainArray4 = array($domainname, $value,$ttl);
        $dataObject6 = new TXTRecord();
        $result6 = $dataObject6->AddTXTRecord($domainArray4);
		$Object = new TXTRecord();
        $response5 = $Object->getTXTRecord();

/* End TXT Records */

return view('clientlayout.main.mydomains',compact('domain','results','result1','result2','result3','result4','result5','result6','response','response1',
'response2','response3','response4','response5','modify_CNAMErecord','modify_AAAArecord','modify_TXTrecord'));

}

public function updatedns(Request $request)
{
	//print_r(Input::get());exit;
	$domainname = Input::get('domainname'); 
	$currentvalue = Input::get('currentvalue'); 
        $newvalue = Input::get('newvalue');
        $domainArray_modify = array($currentvalue, $newvalue);
		//print_r($domainArray_modify);exit;
        $modifyObject = new ARecord();
        $modify_Arecord = $modifyObject->modifyArecord($domainArray_modify);
		\Session::flash('message', 'Successfully updated!');
		return redirect('/mydomains');
}


public function deletedns(Request $request)
{
	
		$domainname = base64_decode($_GET['domainname']); 
		$currentvalue = base64_decode($_GET['ip']); 
        $domainArray_modify = array($domainname, $currentvalue);
	    $modifyObject = new ARecord();
        $modify_Arecord = $modifyObject->deleteArecord($domainArray_modify);
		\Session::flash('message_delete', 'Successfully Deleted!');
		return redirect('/mydomains');
}


public function updateAAAARecord(Request $request)
{
	//print_r(Input::get());exit;
		$domainname = Input::get('domainname'); 
		$currentvalue = Input::get('currentvalue'); 
        $newvalue = Input::get('newvalue');
        $domainArray_modify1 = array($domainname,$currentvalue, $newvalue);
		print_r($domainArray_modify1);exit;
        $modifyObject = new AAAARecord();
        $modify_AAAArecord = $modifyObject->modifyAAAArecord($domainArray_modify1);
		//print_r($modify_AAAArecord);exit;
		\Session::flash('message', 'Successfully updated!');
		return redirect('/mydomains');
}

public function deleteAAAArecord(Request $request)
{
	
	$domainname = base64_decode($_GET['domainname']); 
	$currentvalue = base64_decode($_GET['ip']); 
        $host=base64_decode($_GET['host']);
        $domainArray_modify = array($domainname, $currentvalue,$host);
        $modifyObject = new AAAARecord();
        $modify_AAAArecord = $modifyObject->deleteAAAArecord($domainArray_modify);
		\Session::flash('message_delete', 'Successfully Deleted!');
		return redirect('/mydomains');
}


public function updateTXTRecord(Request $request)
{
		$domainname = Input::get('domainname'); 
		$currentvalue = Input::get('currentvalue'); 
        $newvalue = Input::get('newvalue');
		//print_r($newvalue);exit;
        $domainArray_modify2 = array($domainname,$currentvalue, $newvalue);
        $modifyObject = new TXTRecord();
        $modify_TXTrecord = $modifyObject->modifyTXTrecord($domainArray_modify2);
		//print_r($domainArray_modify2);exit;
		\Session::flash('message', 'Successfully updated!');
		return redirect('/mydomains');
}

public function deleteTXTrecord(Request $request)
{
		$domainname = base64_decode($_GET['domainname']); 
		$currentvalue = base64_decode($_GET['ip']); 
		$host=base64_decode($_GET['host']);
        $domainArray_modify = array($domainname, $currentvalue,$host);
        $modifyObject = new TXTRecord();
        $modify_TXTrecord = $modifyObject->deleteTXTrecord($domainArray_modify);
		\Session::flash('message_delete', 'Successfully Deleted!');
		return redirect('/mydomains');
}

public function updateNSRecord(Request $request)
{  		//print_r(Input::get());exit;
		$domainname = Input::get('domainname'); 
		$currentvalue = Input::get('currentvalue'); 
        $newvalue = Input::get('newvaluefqdn');
		//print_r($currentvalue);exit;
        $domainArray_modify = array($domainname, $currentvalue, $newvalue);
        $modifyObject = new NSRecord();
        $modify_NSrecord = $modifyObject->modifyNSrecord($domainArray_modify);
		//print_r($domainArray_modify);exit;
		\Session::flash('message', 'Successfully updated!');
		return redirect('/mydomains');
}

public function deleteNSrecord(Request $request)
{
	
		$domainname = base64_decode($_GET['domainname']); 
		$value = base64_decode($_GET['value']);
	    $domainArray_delete = array($domainname, $host, $value);
		echo "<pre>";print_r(domainArray_delete);exit;
	    $modifyObject = new NSRecord();
        $modify_NSrecord = $modifyObject->deleteNSrecord($domainArray_delete);
		\Session::flash('message_delete', 'Successfully Deleted!');
		return redirect('/mydomains');
}

public function updateMXRecord(Request $request)
{  		//print_r(Input::get());exit;
		$domainname = Input::get('domainname'); 
		$currentvalue = Input::get('currentvalue'); 
        $newvalue = Input::get('newvaluefqdn');
		$priority = Input::get('priority');
	//print_r($currentvalue);exit;
        $domainArray_modify = array($domainname, $currentvalue, $newvalue ,$priority);
        $modifyObject = new MXRecord();
        $modify_MXrecord = $modifyObject->modifyMXrecord($domainArray_modify);
		//print_r($domainArray_modify);exit;
		\Session::flash('message', 'Successfully updated!');
		return redirect('/mydomains');
}

public function deleteMXrecord(Request $request)
{
		$domainname = base64_decode($_GET['domainname']); 
		$value = base64_decode($_GET['ip']);
		
		$domainArray_modify = array($domainname, $value);
		//print_r($domainArray_modify);exit;
        $modifyObject = new MXRecord();
        $modify_MXrecord = $modifyObject->deleteMXrecord($domainArray_modify);
		\Session::flash('message_delete', 'Successfully Deleted!');
		return redirect('/mydomains');
}
   
}
