<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\View;

use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;

class WordpressHostingController extends Controller
{
    public function show(){
    $products=Whmcs::GetProducts([]);
    return view('main.wordpressHosting',compact('products'));
    }
}