<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DarthSoup\Whmcs\Facades\Whmcs;

use Illuminate\Support\Facades\View;

use Darthsoup\Whmcs\WhmcsServiceProvider;

class TestController extends Controller
{
    public function order(){
        $orders=Whmcs::GetOrders([]);
    //    dd($orders);
        return view('clientlayout.main.test2', compact('orders'));
    }
}
