<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\DomainSuggestion;
use App\Repositories\Domain;
use App\Repositories\DomainPricing;
use App\Repositories\TLDPricing;

class FileController extends Controller
{
    public function check(Request $request)
    {
        // ini_set('max_execution_time', 300);
       
        // $sld = $request->input('sld'); 
        // $tld = $request->input('tld');
        // // dd($sld, $tld); 
        $domainname = $request -> input('domain-name');
        $tld = $request->input('tlds');
        

        $domainArray = array($domainname, $tld);
        $dataObject = new Domain();
        $result = $dataObject->getdomain($domainArray);
       //dd($result);


        // $dataObject1 = new DomainSuggestion();
        // $result1 = $dataObject1->getSuggestdomain($domainArray);

   
        // $dataObject2 = new DomainPricing();
        // $result2 = $dataObject2->domainprice($domainArray);
        // //dd($result2);
        
        // // $dataObject3 = new TLDPricing();
        // // $result3 = $dataObject3->tldprice($domainArray);
        // // dd($result3);
        
        return view('clientlayout.main.tld',compact('result'));
    }
}
