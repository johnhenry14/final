<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DarthSoup\Whmcs\Facades\Whmcs;

use Illuminate\Support\Facades\View;

use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;

class TicketController extends Controller
{
    public function set(Request $request){
        $clientid = session()->get('login_id');
        $results=Whmcs::GetTickets([
            'clientid'=>$clientid
        ]);     
return view('clientlayout.main.Tickets',compact('results'));

}

  
   
}

