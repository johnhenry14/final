<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DarthSoup\Whmcs\Facades\Whmcs;

use Illuminate\Support\Facades\View;

use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;

class MyServicesController extends Controller
{
   
    public function show(){
        $clientid = session()->get('login_id');
        $value = Whmcs::GetClientsProducts(['clientid'=>$clientid]);
       
        //dd($value);
        return view('clientlayout.main.myservices',compact('value'));
    }
 
}