<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DomainAvailableController extends Controller
{
    public function isAvailable($url) 
	{
		$this->api->parseUrl($url);
		return $this->api->call(['command' => 'check'])->response()['RRPCode'] == 210 ? true : false;
	}
}