<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DarthSoup\Whmcs\Facades\Whmcs;
use Illuminate\Support\Facades\View;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;
class InvoiceTicketController extends Controller
{   
    public function show(Request $request){
    $value = session()->get('login_session');
        if($value =='true' && $value !='')
        {
            $clientid = session()->get('login_id');
            $tickets = Whmcs::GetTickets([ 'clientid'=>$clientid]);
            $invoices = Whmcs::GetInvoices([ 'userid'=>$clientid]);      
            $products = Whmcs::GetClientsProducts([
               'clientid'=>$clientid,
               'stats' =>true

            ]);
            $domain=Whmcs::GetClientsDomains(['clientid'=>$clientid]);
                     
            return view('clientlayout.main.index',compact('tickets', 'invoices','products','services','domain'));        
        }
            else
            {
                return redirect('signin');
            }
        
    }

    public function set(){

        $tld=Whmcs::GetTLDPricing([]);
        return view('clientlayout.main.registerdomain',compact('tld'));
    }

    public function order(){
        $clientid = session()->get('login_id');
        $orders=Whmcs::GetOrders(['userid'=>$clientid]);
//echo "<pre>";print_r($orders);exit;
        return view('clientlayout.main.Getorders',compact('orders'));
    }
	
    public function get(){
        $clientid = session()->get('login_id');
        $domain=Whmcs::GetClientsDomains(['clientid'=>$clientid]);
        $results=Whmcs::GetClientsDetails(['clientid'=>$clientid]);
        // dd($domain);
        return view('clientlayout.main.mydomains',compact('domain','results'));
    }
    public function invoice(){
        $clientid = session()->get('login_id');
        $invoice=Whmcs::GetInvoices(['userid'=>$clientid]);

        return view('clientlayout.main.invoice',compact('invoice'));
    }
   
}
