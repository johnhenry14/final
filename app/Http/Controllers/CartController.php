<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use Cart;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;

class CartController extends Controller
{
    public function addToCart(){
    return Cart::content();
    }

    public function addCart(Request $request)
    {
        $value = session()->get('flag_v');
        $name = $request->input();
		//echo "<pre>";print_r($name);exit;
        $hosting=$name['hosting'];
        $hosting_id=$name['hosting_id'];
        $package=$name['package'];
        $price=$name['price'];

        if($package!='' && $price!='')
        {
            $price1=explode('-',$price);

            $month=(int)$price1[0];
            $month_s=$price1[1];
            $price2=(int)$price1[2];

            if($month_s=='annually' || $month_s=='biennially' || $month_s=='triennially' )
            {
                $price_t=($month*$price2)*12;

            }
            else
            {
                $price_t=$month*$price2;
            }

            $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' => (int)$price_t, 'options' => ['package' => $package,'bicycle'=>$month_s,'gid'=>$hosting_id,'type'=>'hosting','os'=>'','fields_id'=>0,'addons'=>[],'addons_id'=>[]]]);

            $data=json_encode($response);
            $cartdata=json_decode($data);

            if($value=='true')
            {
                $request->session()->forget('flag_v');
                return redirect('cart');
            }
            else
            {
                $request->session()->forget('flag_v');
                return redirect('Domain');
            }
        }
        else
        {
            return redirect('SME_Hosting');
        }

    }
	
	public function addCartWordpress(Request $request)
    {
        $value = session()->get('flag_v');
        $name = $request->input();
		//echo "<pre>";print_r($name);exit;
        $hosting=$name['hosting'];
        $hosting_id=$name['hosting_id'];
        $package=$name['package'];
        $price=$name['price'];

        if($package!='' && $price!='')
        {
            $price1=explode('-',$price);

            $month=(int)$price1[0];
            $month_s=$price1[1];
            $price2=(int)$price1[2];

            if($month_s=='annually' || $month_s=='biennially' || $month_s=='triennially' )
            {
                $price_t=($month*$price2)*12;

            }
            else
            {
                $price_t=$month*$price2;
            }

            $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' => (int)$price_t, 'options' => ['package' => $package,'bicycle'=>$month_s,'gid'=>$hosting_id,'type'=>'hosting','os'=>'','fields_id'=>0,'addons'=>[],'addons_id'=>[]]]);

            $data=json_encode($response);
            $cartdata=json_decode($data);
return redirect('cart');
            
        }
        else
        {
            return redirect('wordpressHosting');
        }

    }
	
	public function addCartEcommerce(Request $request)
    {
         //print_r(Input::get());exit;
        $name = $request->input();
        //echo "<pre>";print_r($name);exit;
		$hosting=explode('-',$name['ecommerce_hostings']); 
		$package=$name['package'];
	    $hosting1=$name['name1'];
        $price=$name['price'];
        $gid=$name['gid'];
		$msetupfee=0;
		//$add_ons[0]=array('name'=>"ecommerce",'id'=>$gid,'price'=>$name['price']);
		$add_ons[0]=array('name'=>$hosting[1],'id'=>$hosting[0],'price'=>$hosting[2]);
		$add_ons_id=array($hosting[0]);
		//echo "<pre>";print_r($hosting);exit;
		if($package!='' && $price!='')
			{
            $price1=explode('-',$price);

            $month=(int)$price1[0];
            $month_s=$price1[1];
            $price2=(int)$price1[2];

            if($month_s=='annually' || $month_s=='biennially' || $month_s=='triennially' )
            {
                $price_t=($month*$price2)*12;

            }
            else
            {
                $price_t=$month*$price2;
            }

        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting1, 'quantity' => 1, 'price' => (int)$price_t, 'options' => ['package' => 'Ecommerce Hosting','bicycle'=>'','gid'=>$gid,'msetupfee'=>"",'type'=>'Ecommerce','os'=>$hosting[1],'addons'=>$add_ons,'addons_id'=>$add_ons_id,'fields_id'=>$hosting[0],'db'=>"",'fields_id1'=>"",'cp'=>"",'fields_id2'=>""]]);
		//echo "<pre>";print_r($response);exit;

        $data=json_encode($response);
        $cartdata=json_decode($data);
		//echo "<pre>";print_r($cartdata);exit;
		return redirect('Domain');
        //return redirect('cart');
		 }
        else
        {
            return redirect('ecommerceHosting');
        }
    }
	

    public function addCartdomain(Request $request)
    {

        $name = $request->input();
        $hosting=$name['domain'];
        $price=$name['price'];
        $flag=$name['flag'];

        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' => $price, 'options' => ['package' => 'Domain','bicycle'=>'Annualy','type'=>'domain','os'=>'','fields_id'=>0,'addons'=>[],'addons_id'=>[]]]);

        $data=json_encode($response);
        $cartdata=json_decode($data);
		
		
		
        if($flag==0)
        {
            session()->put('flag_v', 'false');
            return redirect('cart');
        }
        else
        {
            session()->put('flag_v', 'true');
            return redirect('Product');
        }

    }
	public function domaintransfer(Request $request)
    {
		$name=$request->input();
		$hosting=$name['domain'];
		$domaintype=$name['domaintype'];
		$price=$name['price'];
    	//dd($name);exit;
        $transfer=Cart::add(['id'=>str_random(50) ,'name' => $hosting,'quantity' => 1, 'price' => $price,'domaintype' => $domaintype, 'options' => ['package' => 'Domain Transfer','bicycle'=>'Annualy','type'=>'Domain Transfer','os'=>'','fields_id'=>0,'addons'=>[],'addons_id'=>[]]]);
//dd($transfer);exit;
        $data=json_encode($transfer);
        $cartdata=json_decode($data);
		return redirect('cart');
  //dd($cartdata);exit;      

    }

    public function addCart_deticated(Request $request)
    {
         //print_r(Input::get());exit;
        $name = $request->input();
       // echo "<pre>";print_r($name);exit;
	$hosting=explode('-',$name['os']);
	$database=explode('-',$name['db']);
	$controlpanel=explode('-',$name['cp']);
        $hosting1=$name['name1'];
        $price=$name['price']+$hosting[2]+$database[2]+$controlpanel[2];
        $gid=$name['gid'];
	$msetupfee=0;
	$add_ons[0]=array('name'=>"server",'id'=>0,'price'=>$name['price']);
	$add_ons[1]=array('name'=>$hosting[1],'id'=>$hosting[0],'price'=>$hosting[2]);
	$add_ons[2]=array('name'=>$database[1],'id'=>$database[0],'price'=>$database[2]);
	$add_ons[3]=array('name'=>$controlpanel[1],'id'=>$controlpanel[0],'price'=>$controlpanel[2]);
	$add_ons_id=array($hosting[0],$database[0],$controlpanel[0]);
	
	

//echo "<pre>";print_r($hosting);exit;

        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting1, 'quantity' => 1, 'price' => $price, 'options' => ['package' => 'Dedicated Server','bicycle'=>'','gid'=>$gid,'msetupfee'=>$msetupfee,'type'=>'server','os'=>$hosting[1],'addons'=>$add_ons,'addons_id'=>$add_ons_id,'fields_id'=>$hosting[0],'db'=>$database[1],'fields_id1'=>$database[0],'cp'=>$controlpanel[1],'fields_id2'=>$controlpanel[0]]]);
//echo "<pre>";print_r($response);exit;

        $data=json_encode($response);
        $cartdata=json_decode($data);
        return redirect('cart');


    }
	
	
	

    public function addCart_cloud(Request $request)
    {
        $value = session()->get('flag_v');
        $name = $request->input();

        $hosting=$name['name'];
        $price=$name['price'];
        $gid=$name['gid'];


        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' => $price, 'options' => ['package' => 'Cloud Hosting','bicycle'=>'','gid'=>$gid,'type'=>'hosting','os'=>'','fields_id'=>0,'addons'=>[],'addons_id'=>[]]]);

        $data=json_encode($response);
        $cartdata=json_decode($data);
        if($value=='true')
        {
            $request->session()->forget('flag_v');
            return redirect('cart');
        }
        else
        {
            $request->session()->forget('flag_v');
            return redirect('Domain');
        }
        //return redirect('Domain');


    }
	
	public function addCartSSL(Request $request)
    {
        $value = session()->get('flag_v');
        $name = $request->input();
//echo "<pre>";print_r($name);exit;
		$package=$name['package'];
	    $price=$name['price'];
        $hosting=$name['name'];
        $price=$name['price'];
        $gid=$name['gid'];

if($package!='' && $price!='')
			{
            $price1=explode('-',$price);

            $month=(int)$price1[0];
            $month_s=$price1[1];
            $price2=(int)$price1[2];

            if($month_s=='annually' || $month_s=='biennially' || $month_s=='triennially' )
            {
                $price_t=($month*$price2)*1;

            }
            else
            {
                $price_t=$month*$price2;
            }

        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' =>(int) $price_t, 'options' => ['package' => 'SSL Certificate','bicycle'=>'','gid'=>$gid,'type'=>'SSL','os'=>'','fields_id'=>0,'addons'=>[],'addons_id'=>[]]]);

        $data=json_encode($response);
        $cartdata=json_decode($data);
        if($value=='true')
        {
            $request->session()->forget('flag_v');
            return redirect('cart');
        }
        else
        {
            $request->session()->forget('flag_v');
            return redirect('Domain');
        }
        //return redirect('Domain');
 }
        else
        {
            return redirect('ssl');
        }

    }
	
	public function addCartSSL_certificate(Request $request)
    { 
        $value = session()->get('flag_v');
        $name = $request->input();
//echo "<pre>";print_r($name);exit;
		$package=$name['package'];
	    $price=$name['price'];
        $hosting=$name['name'];
        $price=$name['price'];
        $gid=$name['gid'];

if($package!='' && $price!='')
			{
            //$price1=explode('-',$price);

            
        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' =>(int) $price, 'options' => ['package' => 'SSL Certificate','bicycle'=>'','gid'=>$gid,'type'=>'SSL','os'=>'','fields_id'=>0,'addons'=>[],'addons_id'=>[]]]);

        $data=json_encode($response);
        $cartdata=json_decode($data);
        if($value=='true')
        {
            $request->session()->forget('flag_v');
            return redirect('cart');
        }
        else
        {
            $request->session()->forget('flag_v');
            return redirect('Domain');
        }
        //return redirect('Domain');
 }
        else
        {
            return redirect('ssl');
        }

    }
 
 
    public function cart_view()
    {
        $data=json_encode(Cart::content());
        $cartdata=json_decode($data);
        //echo "<pre>";print_r($cartdata);exit;

        return view('main.cart',compact('cartdata'));
    }

    public function cart_remove(Request $request)
    {
        Cart::remove($request->rowid);
        return redirect('cart');

    }
}
