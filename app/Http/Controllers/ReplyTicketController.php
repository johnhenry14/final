<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DarthSoup\Whmcs\Facades\Whmcs;

use Illuminate\Support\Facades\View;

use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;

class ReplyTicketController extends Controller
{
    
    public function reply(){
        $clientid = session()->get('login_id');
        $value = Whmcs::GetTicket(['clientid'=>$clientid,
'tid' => 854649

]);
               return view('clientlayout.main.ticketreply',compact('value'));
    }
 
}
