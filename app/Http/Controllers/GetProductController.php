<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\View;

use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;

class GetProductController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {

        $products = Whmcs::GetProducts([


        ]);
        return view('main.smeHosting', compact('products'));
    }

}
