<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DarthSoup\Whmcs\Facades\Whmcs;
use Illuminate\Support\Facades\View;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;
use Illuminate\Support\Facades\Input;

class DomainCheckerController extends Controller
{
    public function available(Request $request)
    {
       

     function apiCall($method, $url, $data = false) {
$curl = curl_init();

switch ($method) {
   case "POST":
       curl_setopt($curl, CURLOPT_POST, 1);

       if ($data)
           curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
       break;
   case "PUT":
       curl_setopt($curl, CURLOPT_PUT, 1);
       break;
   default:
       if ($data)
           $url = sprintf("%s?%s", $url, http_build_query($data));
}

// Optional Authentication: - Need not touch this
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_USERPWD, "username:password");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
return curl_exec($curl);
}
$tldmostarray = array('org', 'info', 'in', 'co.uk', 'net', 'biz', 'co.in');
$tldmorearray = array('asia');
$comarray = array("com");
$resellersupporttld = array_merge($comarray, $tldmostarray, $tldmorearray);
$domarray = array("org", "biz", "info");
$standardcomarray = array();
$premiumcomarray = array("uk");
$url = 'https://httpapi.com/api/products/customer-price.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi';
$data = "";
$data = apiCall('GET', $url, $data);
$datajson = json_decode($data, TRUE);




//echo "<pre>";print_r($nameservers);exit;
return view('main.Domain',compact('sugdomain','domain','datajson','resellersupporttld','domarray','standardcomarray','premiumcomarray','result1','result2','nameservers'));

}

public function updatenameservers(){
 $clientid = session()->get('login_id');
            $nameservers = Whmcs::DomainUpdateNameservers([ 
			'clientid'=>$clientid,
			'domain' => Input::get('domain'),
			'ns1' => Input::get('ns1'),
			'ns2' => Input::get('ns2'),
			]);
			\Session::flash('message', 'Successfully updated!');
			return redirect('Domain');
}

}
