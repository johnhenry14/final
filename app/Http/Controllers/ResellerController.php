<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Input;
use App\Repositories\DomainSuggestion;
use App\Repositories\DomainChecker;
use App\Repositories\DomainPricing;
use App\Repositories\TLDPricing;

class ResellerController extends Controller
{
    public function domaincheck(Request $request)
    {
        // ini_set('max_execution_time', 300);
       
        $sld = $request->input('sld'); 
        $tld = $request->input('tld');
        // dd($sld, $tld); 
        $domainArray = array($sld, $tld);
        $dataObject = new DomainChecker();
        $result = $dataObject->getdomain($domainArray);
       //dd($result);


        $dataObject1 = new DomainSuggestion();
        $result1 = $dataObject1->getSuggestdomain($domainArray);

   
        $dataObject2 = new DomainPricing();
        $result2 = $dataObject2->domainprice($domainArray);
        //dd($result2);
        
        // $dataObject3 = new TLDPricing();
        // $result3 = $dataObject3->tldprice($domainArray);
        // dd($result3);
        
        return view('clientlayout.main.test',compact('result','result1','result2'));
    }
}
