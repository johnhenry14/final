<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Cart;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;

class CartController extends Controller
{
    public function addToCart(){


        return Cart::content();

    }

    public function addCart(Request $request)
    {
        $value = session()->get('flag_v');
        $name = $request->input();
        $hosting=$name['hosting'];
        $hosting_id=$name['hosting_id'];
        $package=$name['package'];
        $price=$name['price'];

        if($package!='' && $price!='')
        {
            $price1=explode('-',$price);

            $month=(int)$price1[0];
            $month_s=$price1[1];
            $price2=(int)$price1[2];

            if($month_s=='annually' || $month_s=='biennially' || $month_s=='triennially' )
            {
                $price_t=($month*$price2)*12;

            }
            else
            {
                $price_t=$month*$price2;
            }

            $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' => (int)$price_t, 'options' => ['package' => $package,'bicycle'=>$month_s,'gid'=>$hosting_id,'type'=>'hosting','os'=>'','fields_id'=>0]]);

            $data=json_encode($response);
            $cartdata=json_decode($data);

            if($value=='true')
            {
                $request->session()->forget('flag_v');
                return redirect('cart');
            }
            else
            {
                $request->session()->forget('flag_v');
                return redirect('Domain');
            }

        }
        else
        {
            return redirect('SME_Hosting');
        }

    }

    public function addCartdomain(Request $request)
    {

        $name = $request->input();
        $hosting=$name['domain'];
        $price=$name['price'];
        $flag=$name['flag'];



        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' => $price, 'options' => ['package' => 'Domain','bicycle'=>'Annualy','type'=>'domain','os'=>'','fields_id'=>0]]);

        $data=json_encode($response);
        $cartdata=json_decode($data);
        if($flag==0)
        {
            session()->put('flag_v', 'false');
            return redirect('cart');
        }
        else
        {
            session()->put('flag_v', 'true');
            return redirect('Product');
        }

    }

   public function addCart_deticated(Request $request)
    {
         //print_r(Input::get());exit;
        $name = $request->input();
       //echo "<pre>";print_r($name);exit;
	   $add_ons_id=array();
	$hosting=explode('-',$name['os']);
	$database=explode('-',$name['db']);
	$controlpanel=explode('-',$name['cp']);
        $hosting1=$name['name1'];
        $price=$name['price']+$hosting[2]+$database[2]+$controlpanel[2];
        $gid=$name['gid'];
	$msetupfee=0;
	$add_ons[0]=array('name'=>"server",'id'=>0,'price'=>$name['price']);
	$add_ons[1]=array('name'=>$hosting[1],'id'=>$hosting[0],'price'=>$hosting[2]);
	$add_ons[2]=array('name'=>$database[1],'id'=>$database[0],'price'=>$database[2]);
	$add_ons[3]=array('name'=>$controlpanel[1],'id'=>$controlpanel[0],'price'=>$controlpanel[2]);
	$hosting_exp=explode('_',$hosting[0]);

	$db_exp=explode('_',$database[0]);
	$cpanel_exp=explode('_',$controlpanel[0]);
	$add_ons_id[$hosting_exp[0]]=$hosting_exp[1];
	$add_ons_id[$db_exp[0]]=$db_exp[1];
	$add_ons_id[$cpanel_exp[0]]=$cpanel_exp[1];
	
	

//echo "<pre>";print_r($add_ons_id);exit;

        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting1, 'quantity' => 1, 'price' => $price, 'options' => ['package' => 'Dedicated Server','bicycle'=>'','gid'=>$gid,'msetupfee'=>$msetupfee,'type'=>'server','os'=>$hosting[1],'addons'=>$add_ons,'addons_id'=>$add_ons_id,'fields_id'=>$hosting[0],'db'=>$database[1],'fields_id1'=>$database[0],'cp'=>$controlpanel[1],'fields_id2'=>$controlpanel[0]]]);
echo "<pre>";print_r($response);exit;

        $data=json_encode($response);
        $cartdata=json_decode($data);
        return redirect('cart');


    }
    public function addCart_cloud(Request $request)
    {
        $value = session()->get('flag_v');
        $name = $request->input();
        $hosting=$name['name'];
        $price=$name['price'];
        $gid=$name['gid'];


        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting, 'quantity' => 1, 'price' => $price, 'options' => ['package' => 'Cloud Hosting','bicycle'=>'','gid'=>$gid,'type'=>'hosting','os'=>'','fields_id'=>0]]);

        $data=json_encode($response);
        $cartdata=json_decode($data);
        if($value=='true')
        {
            $request->session()->forget('flag_v');
            return redirect('cart');
        }
        else
        {
            $request->session()->forget('flag_v');
            return redirect('Domain');
        }
        //return redirect('Domain');


    }

    public function cart_view()
    {
        $data=json_encode(Cart::content());
        $cartdata=json_decode($data);
        //echo "<pre>";print_r($cartdata);exit;

        return view('main.cart',compact('cartdata'));
    }

    public function cart_remove(Request $request)
    {
        Cart::remove($request->rowid);
        return redirect('cart');

    }
}
