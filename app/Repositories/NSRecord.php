<?php

        namespace App\Repositories;

class NSRecord
{
	function apiCall($method, $url, $data = false) {
$curl = curl_init();

switch ($method) {
   case "POST":
       curl_setopt($curl, CURLOPT_POST, 1);

       if ($data)
           curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
       break;
   case "PUT":
       curl_setopt($curl, CURLOPT_PUT, 1);
       break;
   default:
       if ($data)
           $url = sprintf("%s?%s", $url, http_build_query($data));
}
// Optional Authentication: - Need not touch this
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_USERPWD, "username:password");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
return curl_exec($curl);
}

    function AddNSRecord($domainArray3)
    {
            $url = 'https://httpapi.com/api/dns/manage/add-ns-record.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi&domain-name='.$domainArray3[0].'&value='.$domainArray3[1].'&ttl='.$domainArray3[2].'';
	    $data = "";
	    $data = $this->apiCall('POST', $url, $data);
	    $datajson = json_decode($data, TRUE);
	    return $datajson ;
    }
function getNSRecord(){

$url1 = 'https://httpapi.com/api/dns/manage/search-records.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi&domain-name=novulusindia.com&type=NS&no-of-records=10&page-no=1';
	    $data1 = "";
	    $data1 = $this->apiCall('GET', $url1, $data1);
	    $datajson1 = json_decode($data1, TRUE);
	    return $datajson1;
}


function modifyNSrecord($domainArray_modify){

$url1 = 'https://httpapi.com/api/dns/manage/update-ns-record.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi&domain-name=novulusindia.com&current-value='.$domainArray_modify[0].'&new-value='.$domainArray_modify[1].'';
	    $data1 = "";
	    $data1 = $this->apiCall('GET', $url1, $data1);
	    $datajson1 = json_decode($data1, TRUE);
	    return $datajson1;
}

function deleteNSrecord($domainArray_delete){
$url3 = '
https://httpapi.com/api/dns/manage/delete-ns-record.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi&domain-name=novulusindia.com&host='.$domainArray_delete[0].'&value='.$domainArray_delete[1].''; 

	    $data3 = "";
	    $data3 = $this->apiCall('POST', $url3, $data3);
	    $datajson3 = json_decode($data3, TRUE);
	    return $datajson3;
}

}




