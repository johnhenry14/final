<?php

namespace App\Repositories;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AcceptOrder
{
    function AcceptOrder()
    {
        $value = session()->get('login_id');
        $payment_id = Input::get('payment_id');
        $order_id = Input::get('order_id');
        $invoiceid = Input::get('invoiceid');

        $results1 = Whmcs::AcceptOrder([
            'orderid' => $order_id,
            'autosetup' => true,
            'sendemail' => true
        ]);

        return $results1;
    }
}