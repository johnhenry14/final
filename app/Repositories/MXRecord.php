<?php

        namespace App\Repositories;

class MXRecord
{
	function apiCall($method, $url, $data = false) {
$curl = curl_init();

switch ($method) {
   case "POST":
       curl_setopt($curl, CURLOPT_POST, 1);
   
       if ($data)
           curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
       break;
   case "PUT":
       curl_setopt($curl, CURLOPT_PUT, 1);
       break;
   default:
       if ($data)
           $url = sprintf("%s?%s", $url, http_build_query($data));
}
// Optional Authentication: - Need not touch this
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_USERPWD, "username:password");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
return curl_exec($curl);
}

    function AddMXRecord($domainArray1)
    {
            $url = 'https://httpapi.com/api/dns/manage/add-mx-record.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi&domain-name='.$domainArray1[0].'&value='.$domainArray1[1].'&ttl='.$domainArray1[2].'&priority='.$domainArray1[3].'';
	    $data = "";
	    $data = $this->apiCall('POST', $url, $data);
	    $datajson = json_decode($data, TRUE);
	    return $datajson ;
    }
	
function getMXRecord(){

$url1 = 'https://httpapi.com/api/dns/manage/search-records.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi&domain-name=novulusindia.com&type=MX&no-of-records=10&page-no=1';
	    $data1 = "";
	    $data1 = $this->apiCall('GET', $url1, $data1);
	    $datajson1 = json_decode($data1, TRUE);
	    return $datajson1;
}


  function apiCall_m($method, $url, $data = false) {
$curl = curl_init();

switch ($method) {
   case "POST":
       curl_setopt($curl, CURLOPT_POST, 1);

       if ($data)
           curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
       break;
   case "PUT":
       curl_setopt($curl, CURLOPT_PUT, 1);
       break;
   default:
       if ($data)
           $url = sprintf("%s?%s", $url, http_build_query($data));
}
// Optional Authentication: - Need not touch this
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_USERPWD, "username:password");
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
return curl_exec($curl);
}

function modifyMXrecord($domainArray_modify){

$url2 = 'https://httpapi.com/api/dns/manage/update-mx-record.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi&domain-name=novulusindia.com&current-value='.$domainArray_modify[1].'&new-value='.$domainArray_modify[2].'';

	    $data2 = "";
	    $data2 = $this->apiCall_m('POST', $url2, $data2);
	    $datajson2 = json_decode($data2, TRUE);
	    return $datajson2;
}

function deleteMXrecord($domainArray_modify){

$url2 = 'https://httpapi.com/api/dns/manage/delete-mx-record.json?auth-userid=711757&api-key=74miSZPgYkkc108Ptmeo5Q8iDTtKk3xi&domain-name=novulusindia.com&value='.$domainArray_modify[1].'';
	    $data2 = "";
	    $data2 = $this->apiCall_m('POST', $url2, $data2);
	    $datajson2 = json_decode($data2, TRUE);
	    return $datajson2;
}


}




