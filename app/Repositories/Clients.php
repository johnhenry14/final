<?php

namespace App\Repositories;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Clients
{
    function GetClients()
    {
        $value = session()->get('login_id');
        $payment_id = Input::get('payment_id');
        $order_id = Input::get('order_id');
        $invoiceid = Input::get('invoiceid');

        $clientdetails = Whmcs::GetClientsDetails([
            'clientid' => $value,
        ]);

        return $clientdetails;
    }
}