<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* ------------- Ticketing system ------------------*/
Route::post('create','CreateTicketController@create')->name('ticket.create');
Route::get('Tickets','CreateTicketController@show_tickets')->name('results.show_tickets');
Route::get('ReplyTicket','CreateTicketController@reply_ticket')->name('reply.reply_ticket');
Route::get('DeleteTicket','CreateTicketController@DeleteTicket')->name('reply.DeleteTicket');
Route::post('add_ticket_reply','CreateTicketController@add_reply')->name('ticket.add_reply');


/* ------------- Login System ------------------*/

Route::post('insert','UserController@insert')->name('user.insert');
Route::any('signin', 'SigninController@signin');
Route::post('signin_action','SigninController@signin_action')->name('login.signin_action');
Route::post('forgotpassword_action','SigninController@forgotpassword_action')->name('login.forgotpassword_action');
Route::get('signout', 'SigninController@signout')->name('logout.signout');
Route::get('/reset_password','SigninController@reset_password');
Route::post('reset_password_action','SigninController@reset_password_action')->name('login.reset_password_action');
Route::get('/profile','ProfileController@profile_update_action')->name('profile.profile_update_action');
Route::post('profile_update_action_update','ProfileController@profile_update_action_update')->name('profile.profile_update_action_update');

Route::get('/Domain','DomainCheckerController@available');
Route::post('/updatenameservers','DomainCheckerController@updatenameservers')->name('domain.updatenameservers');

Route::get('/index','InvoiceTicketController@show');
Route::get('/mydomains','InvoiceTicketController@get');
Route::get('/Getorders','InvoiceTicketController@order');
Route::get('/invoice','InvoiceTicketController@invoice');
Route::get('/registerdomain','TldController@available');
Route::get('/getdomains','SigninController@test');

/* ------------- DNS Management System ------------------*/

Route::get('mydomains','DNSController@DNS');
Route::get('deleteArecord', 'DNSController@deletedns')->name('record.remove');
Route::get('deleteAAAArecord', 'DNSController@deleteAAAArecord')->name('record.remove');
Route::get('deleteTXTrecord', 'DNSController@deleteTXTrecord')->name('record.remove');
Route::get('deleteNSrecord', 'DNSController@deleteNSrecord')->name('record.remove');
Route::get('deleteMXrecord', 'DNSController@deleteMXrecord')->name('record.remove');

Route::post('modifyArecord','DNSController@updatedns')->name('dns.modify');
Route::post('modifyAAAArecord','DNSController@updateAAAARecord')->name('AAAA.modify');
Route::post('modifyTXTrecord','DNSController@updateTXTRecord')->name('TXT.modify');
Route::post('modifyMXrecord','DNSController@updateMXRecord')->name('MX.modify');
Route::post('modifyNSrecord','DNSController@updateNSRecord')->name('NS.modify');
Route::post('modifyCNAMErecord','DNSController@updateCNAMERecord')->name('CNAME.modify');

/* -------------End DNS Management System ------------------*/

Route::any('/Funds', 'FundsController@Funds')->name('Funds.success');

Route::get('/myservices','MyServicesController@show');

Route::get('/RaiseTicket', function () {
    return view('clientlayout.main.RaiseTicket');
});
Route::get('/login', function () {
    return view('clientlayout.main.login');
});


Route::get('/signup', function () {
    return view('clientlayout.main.signup');
});

Route::get('/forgotpassword', function () {
    return view('clientlayout.main.forgotpassword');
});





Route::get('/Freelancers', function () {
    return view('main.Freelancers');
});

Route::get('/EnterpriseServer_Hosting', function () {
    return view('main.EnterpriseServer_Hosting');
});
Route::get('/Resellers', function () {
    return view('main.Resellers');
});
Route::get('/About',function(){
    return view('main.About');
});
Route::get('/Aboutus',function(){
    return view('main.Aboutus');
});
Route::get('/',function(){
    return view('main.index');
});
Route::get('/Product',function(){
    return view('main.Product');
});
Route::get('/Products',function(){
    return view('main.Products');
});

Route::get('/Solutions',function(){
    return view('main.Solutions');
});

Route::get('/Partners',function(){
    return view('main.Partners');
});
Route::get('/Contact',function(){
    return view('main.Contact');
});
Route::get('/Privacy_Policy',function(){
    return view('main.Privacy_Policy');
});
Route::get('/Terms_Service',function(){
    return view('main.Terms_Service');
});
Route::get('/Refund_Policy',function(){
    return view('main.Refund_policy');
});




// Get Route For Show Payment Form
Route::get('payWithRazorpay', 'RazorpayController@payWithRazorpay')->name('payWithRazorpay.payWithRazorpay');

Route::post('payment', 'RazorpayController@payment');

Route::get('PayWithRazorpay', 'ClientController@set');


Route::get('domaintransfer', 'TransferController@show');




//Cart Controller
// add to cart 
Route::any('/addtocart', 'CartController@addCart')->name('cart.add');
Route::any('/add_to_cart_dedicated_server', 'CartController@addCart_deticated')->name('cart.add_dedicated_server');
Route::any('/addCartEcommerce', 'CartController@addCartEcommerce')->name('cart.addCartEcommerce');
Route::any('/add_to_cart_cloud_hosting', 'CartController@addCart_cloud')->name('cart.add_cloud_hosting');
Route::any('/domaintransfer', 'CartController@domaintransfer')->name('cart.domaintransfer');
Route::any('/addCartSSL', 'CartController@addCartSSL')->name('cart.addCartSSL');
Route::any('/addCartSSL_certificate', 'CartController@addCartSSL_certificate')->name('cart.addCartSSL_certificate');
Route::any('/addCartWordpress', 'CartController@addCartWordpress')->name('cart.addCartWordpress');
Route::any('/addtocartDomain', 'CartController@addCartdomain')->name('cart.domain');
Route::any('/cart', 'CartController@cart_view')->name('cart');
Route::any('/cart_remove', 'CartController@cart_remove')->name('cart.remove');


Route::any('/payment_success', 'RazorpayController@payment_success')->name('payment.success');
Route::get('/payment_invoice/{id}/{id1}', 'RazorpayController@invoice_details')->name('payment.invoice');
Route::any('/pdf_invoice', 'RazorpayController@pdf_invoice')->name('pdf_invoice.download');

Route::get('smeHosting','GetProductController@show');
Route::get('cloudHosting','CloudHostingController@show');
Route::get('dedicatedServer','DedicatedServerController@show');
Route::get('ecommerceHosting','EcommerceHostingController@show');
Route::get('ssl','SslController@show');
Route::get('wordpressHosting','WordpressHostingController@show');


Route::get('clientinvoice','ClientInvoiceController@pay');


Route::get('/domains',function(){
    $products = Whmcs::GetProducts([
		
    ]);
	echo "<pre>";print_r($products);exit;
    return Whmcs::GetProducts();
});


