/**
 * Decksys Script Documentation
 *
 * This Decksys Version Uses jquery-3.2.1
 *
 * @project   Decksys
 * @version   0.0.1
 * @package   Javascript And Jqurey
 * @authors   Decksys Team
 * @Date      Oct 2 2017 - 
 */


/**
 * Hosting type Hover And Click Effect Using Animate.css
 * @Framework Animate.css
 * @Link  https://github.com/daneden/animate.css
 * @Demo  https://daneden.github.io/animate.css/
 */

$(document).ready(function() {
    var animationType = 'animated pulse';
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $('div.hostingBox').on('click', function() {
        $(this).addClass(animationType).one(animationEnd, function() {
            $(this).removeClass(animationType);
        });
    });
});


/**
 * SME Hosting Tabs Script To Show the relevant Packages
 * @Script       Jquery
 * @Reference    https://codepen.io/anon/pen/RjVvww
 */
$(document).ready(function() {

    $('ul.smeHostingTabsContainer li').click(function() {
        var tab_id = $(this).attr('data-tab');

        $('ul.smeHostingTabsContainer li').removeClass('active');
        $('.tabContent').removeClass('active');
        $('.tabHostingFeatureContainer section').removeClass('active');

        $(this).addClass('active');
        $("#" + tab_id).addClass('active');
        $(".tabHostingFeatureContainer section#" + tab_id).addClass('active');

    })

});

/**
 * SME Hosting Slide Type Animation Using animation.css
 */